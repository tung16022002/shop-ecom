import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AdminLayoutComponent} from "./layout/admin-layout/admin-layout.component";
import {LoginComponent} from "./component/login/login.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path:'dashboard',
        loadChildren: () => import('./module/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path:'category',
        loadChildren: () => import('./module/category/category.module').then(m => m.CategoryModule)
      },
      {
        path:'order',
        loadChildren: () => import('./module/order/order.module').then(m => m.OrderModule)
      },
      {
        path:'user',
        loadChildren: () => import('./module/user/user.module').then(m => m.UserModule)
      },
      {
        path: 'product',
        loadChildren: () => import('./module/product/product.module').then(m => m.ProductModule)
      },
      {
        path: 'coupon',
        loadChildren: () => import('./module/coupon/coupon.module').then(m => m.CouponModule)
      }

    ],
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
