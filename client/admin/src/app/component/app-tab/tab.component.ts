import { Component, Input } from '@angular/core';
import {NgIf, NgTemplateOutlet} from "@angular/common";

@Component({
  selector: 'my-tab',
  styles: [
    `
      .pane {
        padding: 1em;
      }
    `,
  ],
  template: `
    <div [hidden]="!active" class="pane">
      <ng-content></ng-content>
      <ng-container *ngIf="template"
                    [ngTemplateOutlet]="template"
                    [ngTemplateOutletContext]="{ person: dataContext }"
      >
      </ng-container>
    </div>
  `,
  imports: [
    NgTemplateOutlet,
    NgIf
  ],
  standalone: true
})
export class TabComponent {
  @Input('tabTitle') title: string;
  @Input('icon') icon: string;
  @Input() active = false;
  @Input() isCloseable = false;
  @Input() template;
  @Input() dataContext;
}
