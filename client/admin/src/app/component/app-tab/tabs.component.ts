import {AfterContentInit, Component, ContentChildren, QueryList, ViewChild, ViewContainerRef} from '@angular/core';
import {TabComponent} from './tab.component';
import {DynamicTabsDirective} from './dynamic-tabs.directive';
import {NgForOf, NgIf} from "@angular/common";

@Component({
  selector: 'my-tabs',
  template: `
    <ul class="nav nav-underline">
      <li class="nav-item" *ngFor="let tab of tabs" (click)="selectTab(tab)" [class.active]="tab.active">
        <a class="nav-link d-flex align-items-center" [class.active]="tab.active">
          <i class="icon" [class.text-primary]="tab.active"  [class]="tab.icon"></i>
          {{ tab.title }}
        </a>
      </li>
      <!-- dynamic tabs -->
      <li class="nav-item" *ngFor="let tab of dynamicTabs" (click)="selectTab(tab)" [class.active]="tab.active">
        <span class="nav-link d-flex align-items-center" [class.active]="tab.active">
          <i class="icon" [class.text-primary]="tab.active" [class]="tab.icon"></i>
          {{ tab.title }}
          <a class="tab-close " *ngIf="tab.isCloseable" (click)="closeTab(tab)"> <i class="fa-solid fa-xmark"></i></a>
        </span>
      </li>
    </ul>

    <ng-content></ng-content>
    <ng-template dynamic-tabs></ng-template>
  `,
  styles: [
    `
      .tab-close {
        color: gray;
        text-align: right;
        cursor: pointer;
        margin-left: 8px;
      }

      .nav-item .nav-link.active {
        color: #000000 !important; /* Màu xanh của Bootstrap */
        font-weight: 400;
        border-bottom: 2px solid #007bff; /* Gạch chân màu xanh */
        transition: border-bottom 0.3s ease;
      }

      .nav-item .nav-link {
        color: #000; /* Màu chữ bình thường (đen) cho tab không active */
        border-bottom: 2px solid transparent; /* Không có gạch chân cho tab không active */
      }

      .nav-item .nav-link:hover {
        color: #000000 !important; /* Màu xanh của Bootstrap */
        font-weight: 400;
        border-bottom: 2px solid #007bff; /* Gạch chân màu xanh */
        transition: border-bottom 0.3s ease;
        cursor: pointer;
      }

      .nav-item:hover  {

      }

      .icon {
        margin-right: 4px;
      }
    `,
  ],
  imports: [
    NgForOf,
    NgIf,
    DynamicTabsDirective
  ],
  standalone: true
})
export class TabsComponent implements AfterContentInit {
  dynamicTabs: TabComponent[] = [];

  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
  @ViewChild(DynamicTabsDirective, {static: true}) dynamicTabPlaceholder: DynamicTabsDirective;

  constructor(private viewContainerRef: ViewContainerRef) {
  }

  ngAfterContentInit() {
    const activeTabs = this.tabs.filter(tab => tab.active);
    if (activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }

  openTab(title: string,icon: string ,template: any, data: any, isCloseable = false) {
    const componentRef = this.dynamicTabPlaceholder.viewContainer.createComponent(TabComponent);

    const instance: TabComponent = componentRef.instance;
    instance.title = title;
    instance.icon = icon;
    instance.template = template;
    instance.dataContext = data;
    instance.isCloseable = isCloseable;

    this.dynamicTabs.push(instance);
    this.selectTab(this.dynamicTabs[this.dynamicTabs.length - 1]);
  }

  selectTab(tab: TabComponent) {
    this.tabs.toArray().forEach(tab => tab.active = false);
    this.dynamicTabs.forEach(tab => tab.active = false);
    tab.active = true;
  }

  closeTab(tab: TabComponent) {
    for (let i = 0; i < this.dynamicTabs.length; i++) {
      if (this.dynamicTabs[i] === tab) {
        this.dynamicTabs.splice(i, 1);
        this.dynamicTabPlaceholder.viewContainer.remove(i);
        this.selectTab(this.tabs.first);
        break;
      }
    }
  }

  closeActiveTab() {
    const activeTabs = this.dynamicTabs.filter(tab => tab.active);
    if (activeTabs.length > 0) {
      this.closeTab(activeTabs[0]);
    }
  }
}
