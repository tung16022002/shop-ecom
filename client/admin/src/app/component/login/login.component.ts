import {Component, OnInit} from '@angular/core';
import {MessageService} from "primeng/api";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../service/auth.service";
import {UserService} from "../../service/user.service";
import {StartupService} from "../../service/startup.service";
import {load} from "@angular-devkit/build-angular/src/utils/server-rendering/esm-in-memory-file-loader";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit{
  signInForm: FormGroup;
  errorMessage: string | null;
  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private messageService: MessageService,
    private userService: UserService,
    private startupService: StartupService
  ) {
  }

  ngOnInit(): void {
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
    }, 2000);
    this.initData();
  }

  initData() {
    this.signInForm = this.fb.group({
      phoneNumber: ['', Validators.compose([
        Validators.required,
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])]
    })
  }

  login() {
    if (this.signInForm.invalid) {
      this.signInForm.markAllAsTouched();
      return;
    }

    const data = this.signInForm.value;

    this.userService.login(data).subscribe(
      res => {
        // Giả sử res chứa thông tin người dùng bao gồm role
        const userRole = this.startupService.getCurrentUser(res).roleName; // Dữ liệu role trả về từ server
        console.log(userRole);

        if (userRole === 'admin') {
          // Lưu token và điều hướng nếu là admin
          this.authService.saveToken(res);
          console.log('Login successfully as admin');
          this.router.navigate(['/dashboard']);
        } else {
          // Nếu không phải admin, hiện thông báo lỗi
          this.errorMessage = 'Chỉ có quản trị viên (admin) mới được đăng nhập';
          this.messageService.add({
            severity: 'error',
            summary: 'Đăng nhập thất bại',
            detail: 'Bạn không có quyền truy cập'
          });
        }
      },
      error => {
        this.errorMessage = 'Số điện thoại hoặc mật khẩu không chính xác';
        this.messageService.add({
          severity: 'error',
          summary: 'Đăng nhập thất bại',
          detail: 'Số điện thoại hoặc mật khẩu không đúng!'
        });
      }
    );
  }


}
