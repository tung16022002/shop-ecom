import { Component, Input, Output, EventEmitter } from '@angular/core';
import {NgForOf} from "@angular/common";

@Component({
  selector: 'people-list',
  template: `
    <button class="btn btn-primary btn-default" (click)="addPerson.emit()">Add new person</button>

    <table class="table table-striped">
      <thead>
      <th>Name</th>
      <th>Surname</th>
      <th>Twitter</th>
      <th></th>
      </thead>
      <tbody>
      <tr *ngFor="let p of people">
        <td>{{ p.name }}</td>
        <td>{{ p.surname }}</td>
        <td><a href="https://twitter.com/{{ p.twitter }}" target="_blank">{{ p.twitter }}</a></td>
        <td>
          <button class="btn btn-sm btn-default" (click)="editPerson.emit(p)">Edit</button>
        </td>
      </tr>
      </tbody>
    </table>
  `,
  imports: [
    NgForOf
  ],
  standalone: true
})
export class PeopleListComponent {
  @Input() people;
  @Output() addPerson = new EventEmitter<any>();
  @Output() editPerson = new EventEmitter<any>();
}
