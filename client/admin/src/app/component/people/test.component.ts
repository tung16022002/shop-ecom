//our root app component
import { Component, ViewChild } from '@angular/core';
import {TabsComponent} from "../app-tab/tabs.component";
import {TabComponent} from "../app-tab/tab.component";
import {PeopleListComponent} from "./people-list.component";
import {PersonEditComponent} from "./people-edit.component";


@Component({
  selector: 'my-app',
  template: `
    <my-tabs>
      <my-tab
        [icon]="'pi pi-list'"
        [tabTitle]="''">
        <people-list
          [people]="people"
          (addPerson)="onAddPerson()"
          (editPerson)="onEditPerson($event)">
        </people-list>
        <hr/>
      </my-tab>
    </my-tabs>

    <ng-template let-person="person" #personEdit>
      <person-edit [person]="person" (savePerson)="onPersonFormSubmit($event)"></person-edit>
    </ng-template>
  `,
  imports: [
    TabsComponent,
    TabComponent,
    PeopleListComponent,
    PersonEditComponent
  ],
  standalone: true
})
export class TestComponent {
  @ViewChild('personEdit') editPersonTemplate: PersonEditComponent;
  @ViewChild(TabsComponent) tabsComponent: TabsComponent;

  people = [
    {
      id: 1,
      name: 'Juri',
      surname: 'Strumpflohner',
      twitter: '@juristr',
    },
  ];

  onEditPerson(person) {
    this.tabsComponent.openTab(
      `Editing ${person.name}`,
      'fa-regular fa-pen-to-square',
      this.editPersonTemplate,
      person,
      true
    );
  }

  onAddPerson() {
    if (this.editPersonTemplate) {
      this.tabsComponent.openTab('Thêm sản phẩm mới','fa-solid fa-plus', this.editPersonTemplate, {}, true);
    } else {
      console.error('editPersonTemplate is not defined');
    }
  }


  onPersonFormSubmit(dataModel) {
    if (dataModel.id > 0) {
      this.people = this.people.map((person) => {
        if (person.id === dataModel.id) {
          return dataModel;
        } else {
          return person;
        }
      });
    } else {
      // create a new one
      dataModel.id = Math.round(Math.random() * 100);
      this.people.push(dataModel);
    }

    // close the tab
    this.tabsComponent.closeActiveTab();
  }

}
