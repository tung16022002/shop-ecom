export interface OrderDetailDTO {
  productId: string;
  quantity: number;
  totalPrice: number;
  totalProduct: number;
}

export interface OrderDTO {
  id?: string;
  userId?: string;
  fullName?: string;
  email?: string;
  phoneNumber?: string;
  address?: string;
  note?: string;
  status?: string;
  orderDate?: Date;
  totalMoney?: number;
  orderDetails?: OrderDetailDTO[];
  active?: boolean;
}
