export interface ProductDTO {
  id?: string;
  name?: string;
  price?: number;
  thumbnail?: string;
  categoryId?: string;
  subCategoryId?: string;
  totalProduct?: number;
}

