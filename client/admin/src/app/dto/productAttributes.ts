export interface ProductAttributes {
  id?: string,
  code?: string,
  key: string,
  value?: string[]
}
