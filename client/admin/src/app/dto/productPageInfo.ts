export interface ProductPageInfo {
  currentPage?: number;
  recordPerPage?: number;
  categoryId?: string;
}
