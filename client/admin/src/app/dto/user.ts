export interface UserAdmin {
  id?: string;
  fullName?: string;
  password?: string;
  phoneNumber?: string;
  retypePassword?: string;
  facebookAccountId?: 0;
  googleAccountId?: 0;
  roleName?: "admin";
  avt?: string;
}

export interface UserDTO {
  id?: string;
  fullName?: string;
  password?: string;
  phoneNumber?: string;
  retypePassword?: string;
  facebookAccountId?: 0;
  googleAccountId?: 0;
  roleName?: "";
  avt?: string;
}

export interface LoginData {
  phoneNumber?: string;
  password?: string;
}

export interface ChangePasswordDTO {
  currentPassword?: string,
  newPassword?: string,
  confirmPassword?: string
}

