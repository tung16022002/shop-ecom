import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category/category.component';
import {CardModule} from "primeng/card";
import {ButtonModule} from "primeng/button";
import {TabsComponent} from "../../component/app-tab/tabs.component";
import {TabComponent} from "../../component/app-tab/tab.component";
import { ListCategoryParentComponent } from './list-category-parent/list-category-parent.component';
import { ListCategoryChildrenComponent } from './list-category-children/list-category-children.component';
import {DialogModule} from "primeng/dialog";
import {InputTextModule} from "primeng/inputtext";
import {SharedModule} from "primeng/api";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import { FormUpdateCategoryComponent } from './form-update-category/form-update-category.component';
import {ControlMessageComponent} from "../../shared/control-message/control-message.component";
import {ImageModule} from "primeng/image";


@NgModule({
  declarations: [
    CategoryComponent,
    ListCategoryParentComponent,
    ListCategoryChildrenComponent,
    FormUpdateCategoryComponent
  ],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    CardModule,
    ButtonModule,
    TabsComponent,
    TabComponent,
    DialogModule,
    InputTextModule,
    SharedModule,
    FormsModule,
    ConfirmDialogModule,
    ReactiveFormsModule,
    ControlMessageComponent,
    ImageModule
  ]
})
export class CategoryModule { }
