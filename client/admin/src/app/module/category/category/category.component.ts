import {Component, ViewChild} from '@angular/core';
import {TabsComponent} from "../../../component/app-tab/tabs.component";
import {ListCategoryChildrenComponent} from "../list-category-children/list-category-children.component";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent {
  @ViewChild('detailCate') detailCate: ListCategoryChildrenComponent;
  @ViewChild(TabsComponent) tabsComponent: TabsComponent;
  dataCategory: any;

  onDetailCate(category: any) {
    this.dataCategory = category;
    this.tabsComponent.openTab(`Danh sách danh mục con `, 'pi pi-list', this.detailCate, this.dataCategory, true);
  }
}
