import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-form-update-category',
  templateUrl: './form-update-category.component.html',
  styleUrls: ['./form-update-category.component.scss']
})
export class FormUpdateCategoryComponent implements OnInit{
  @Input() category: any;
  formUpdate: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {

  }

  initDataUpdate() {
    this.formUpdate = this.fb.group({
      id: [this.category?.id],
      name: [this.category?.name, Validators.required],
      thumbnail: [this.category?.thumbnail]
    })
  }

}
