import {Component, Input, OnInit} from '@angular/core';
import {CategoryService} from "../../../service/category.serivce";
import {ConfirmationService, ConfirmEventType, MessageService} from "primeng/api";

@Component({
  selector: 'app-list-category-children',
  templateUrl: './list-category-children.component.html',
  styleUrls: ['./list-category-children.component.scss'],
  providers: [ConfirmationService]

})
export class ListCategoryChildrenComponent implements OnInit{
  @Input() category: any;
  visible: boolean;
  listCate: any;
  categoryName: any;
  showUpdate: boolean;
  cateName: any;
  categoryUpdate = {
    id: '',
    name: '',
    parent: '',
  };

  constructor(
    private categoryService: CategoryService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,


  ) {
  }

  ngOnInit(): void {
   this.initData();
  }

  initData() {
    this.categoryService.getCategoryChildren(this.category.code).subscribe(data => {
      this.listCate = data;
    })
  }

  showModal() {
    this.visible = true;
  }

  openUpdate(category: any) {
    this.showUpdate = true;
    this.categoryUpdate.id = category.id;
    this.categoryUpdate.name = category.name;
    this.categoryUpdate.parent = category.parent;
  }

  updateCategory() {
    if(this.categoryUpdate.name == "") {
      this.messageService.add({severity: 'error', summary: 'Tên danh mục là bắt buộc'})
      return ;
    }
    this.categoryService.updateCategory(this.categoryUpdate).subscribe(data => {
      this.messageService.add({severity: "success", summary: "Thành công"})
      this.showUpdate = false;
      this.initData();
    })
  }

  addCategory() {
    if (!this.categoryName || this.categoryName.trim() === '') {
      this.messageService.add({ severity: 'error', summary: 'Lỗi', detail: 'Tên danh mục không được để trống' });
      return; // Dừng hàm nếu giá trị không hợp lệ
    }

    const data = {
      name: this.categoryName,
      parent: this.category.code
    }

    this.categoryService.createCategory(data).subscribe(data => {
      this.messageService.add({severity: 'success', summary: 'Thành công', })
      this.visible = false;
      this.initData();
    }, error => {
      this.messageService.add({severity: 'error', summary: 'Thất bại', detail: 'Danh mục đã có, vui lòng tạo tên khác'})
    })
    this.categoryName = "";
  }

  deleteCategory(category: any) {
    this.confirmationService.confirm({
      message: `Bạn có muốn xóa danh mục  "${category.name}"?`,
      header: 'Xóa danh mục',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.categoryService.deleteCategory(category.id).subscribe(data => {
          this.messageService.add({ severity: 'info', summary: 'Xóa danh mục thành công', detail: '' });
          this.initData();
        })
      },
      reject: (type: ConfirmEventType) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            this.messageService.add({ severity: 'error', summary: 'Hủy', detail: '' });
            break;
          case ConfirmEventType.CANCEL:
            this.messageService.add({ severity: 'warn', summary: 'Hủy', detail: '' });
            break;
        }
      }
    });
  }

}
