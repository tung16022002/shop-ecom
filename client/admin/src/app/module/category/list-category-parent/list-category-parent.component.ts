import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CategoryService} from "../../../service/category.serivce";
import {ConfirmationService, ConfirmEventType, MessageService} from "primeng/api";

@Component({
  selector: 'app-list-category-parent',
  templateUrl: './list-category-parent.component.html',
  styleUrls: ['./list-category-parent.component.scss'],
  providers: [ConfirmationService]
})
export class ListCategoryParentComponent implements OnInit{

  @Output() detailCate = new EventEmitter<any>();

  listDataParent: any;
  visible: boolean;
  categoryName: any;
  showUpdate: boolean;
  categoryUpdate = {
    id: '',
    name: '',
    thumbnail: '',
  };


  constructor(
    private categoryService: CategoryService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,

  ) {
  }

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.categoryService.getCategoryParent().subscribe(data => {
      this.listDataParent = data;
    })
  }

  showModal() {
    this.visible = true;
  }

  openUpdate(category: any) {
    this.showUpdate = true;
    this.categoryUpdate.id = category.id;
    this.categoryUpdate.name = category.name;
    this.categoryUpdate.thumbnail = category.thumbnail;
  }

  updateCategory() {
    if(this.categoryUpdate.name == "") {
      this.messageService.add({severity: 'error', summary: 'Tên danh mục là bắt buộc'})
      return ;
    }
    this.categoryService.updateCategory(this.categoryUpdate).subscribe(data => {
      this.messageService.add({severity: "success", summary: "Thành công"})
      this.showUpdate = false;
      this.initData();
    })
  }



  addCategory() {
    if (!this.categoryName || this.categoryName.trim() === '') {
      this.messageService.add({ severity: 'error', summary: 'Lỗi', detail: 'Tên danh mục không được để trống' });
      return; // Dừng hàm nếu giá trị không hợp lệ
    }

    const data = {
      name: this.categoryName
    }

    this.categoryService.createCategory(data).subscribe(data => {
      this.messageService.add({severity: 'success', summary: 'Thành công', })
      this.visible = false;
      this.initData();
    }, error => {
      this.messageService.add({severity: 'error', summary: 'Thất bại', detail: 'Danh mục đã có, vui lòng tạo tên khác'})
    })
    this.categoryName = ""

  }

  deleteCategory(category: any) {
    this.confirmationService.confirm({
      message: `Bạn có muốn xóa danh mục  "${category.name}"?`,
      header: 'Xóa danh mục',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.categoryService.deleteCategory(category.id).subscribe(data => {
          this.messageService.add({ severity: 'info', summary: 'Xóa danh mục thành công', detail: '' });
          this.initData();
        })
      },
      reject: (type: ConfirmEventType) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            this.messageService.add({ severity: 'error', summary: 'Hủy', detail: '' });
            break;
          case ConfirmEventType.CANCEL:
            this.messageService.add({ severity: 'warn', summary: 'Hủy', detail: '' });
            break;
        }
      }
    });
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      this.categoryService.uploadAvatar("te", file).subscribe({
        next: (response) => {
          this.messageService.add({
            severity: 'info', summary: 'Thành công', detail: 'Ảnh đại diện đã được cập nhật'
          });
        },
        error: (err) => {
          this.messageService.add({
            severity: 'error', summary: 'Thất bại', detail: 'Không thể tải lên ảnh đại diện'
          });
        }
      });
    }
  }

}
