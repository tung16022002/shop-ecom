import {Component, OnDestroy, OnInit} from '@angular/core';
import {CouponService} from "../../../service/coupon.service";
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {MessageService} from "primeng/api";
import {StateService} from "../../../service/state.service";
import {AppEvent} from "../../../enum/app.state";

@Component({
  selector: 'app-add-coupon',
  templateUrl: './add-coupon.component.html',
  styleUrls: ['./add-coupon.component.scss']
})
export class AddCouponComponent implements OnInit{

  formData: FormGroup;
  private appStateEvent: {};

  constructor(
    private couponService: CouponService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private appState: StateService
  ) {
  }

  ngOnInit(): void {
    this.initData();
  }
  initData() {


    this.formData = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      quantity: [1, [Validators.required, Validators.min(1)]],
      discountPercent: ['', [Validators.required, Validators.min(1), Validators.max(100)]],
      maxDiscountAmount: ['', [Validators.required, Validators.min(1)]],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
    }, { validators: this.dateRangeValidator() }); // Áp dụng validator vào FormGroup
  }


   dateRangeValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const startDate = control.get('startDate')?.value;
      const endDate = control.get('endDate')?.value;

      if (startDate && endDate && new Date(startDate) > new Date(endDate)) {
        return { dateRangeInvalid: true }; // Trả về lỗi nếu startDate > endDate
      }
      return null; // Không có lỗi
    };
  }



  toLocalISOString(date: Date): string {
    const offset = date.getTimezoneOffset();
    const localDate = new Date(date.getTime() - (offset * 60 * 1000));
    return localDate.toISOString().slice(0, -1); // Bỏ 'Z' để giữ nguyên múi giờ
  }

  onSubmit() {
    if (this.formData.invalid) {
      this.formData.markAllAsTouched();
      return;
    }

    const startDate = new Date(this.formData.get('startDate').value);
    const endDate = new Date(this.formData.get('endDate').value);

    startDate.setSeconds(0, 0);
    endDate.setSeconds(0, 0);

    const formData = {
      ...this.formData.value,
      startDate: this.toLocalISOString(startDate), // Chuyển đổi thành chuỗi ISO giữ nguyên múi giờ địa phương
      endDate: this.toLocalISOString(endDate)
    };


    this.couponService.addCoupon(formData).subscribe(data => {
      this.messageService.add({ severity: 'success', summary: 'Thành công', detail: 'Thêm mã giảm giá thành công' });
      this.appState.notify(AppEvent.RELOAD_LIST_COUPON, true);
    });
  }




}
