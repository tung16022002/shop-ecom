import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CouponRoutingModule } from './coupon-routing.module';
import { CouponComponent } from './coupon/coupon.component';
import { ListCouponComponent } from './list-coupon/list-coupon.component';
import {TabsComponent} from "../../component/app-tab/tabs.component";
import {TabComponent} from "../../component/app-tab/tab.component";
import { AddCouponComponent } from './add-coupon/add-coupon.component';
import { UpdateCouponComponent } from './update-coupon/update-coupon.component';
import {CalendarModule} from "primeng/calendar";
import {ReactiveFormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {CardModule} from "primeng/card";
import {ControlMessageComponent} from "../../shared/control-message/control-message.component";
import {InputNumberModule} from "primeng/inputnumber";


@NgModule({
  declarations: [
    CouponComponent,
    ListCouponComponent,
    AddCouponComponent,
    UpdateCouponComponent
  ],
  imports: [
    CommonModule,
    CouponRoutingModule,
    TabsComponent,
    TabComponent,
    CalendarModule,
    ReactiveFormsModule,
    InputTextModule,
    CardModule,
    ControlMessageComponent,
    InputNumberModule
  ]
})
export class CouponModule { }
