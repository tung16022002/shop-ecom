import {Component, ViewChild} from '@angular/core';
import {AddCouponComponent} from "../add-coupon/add-coupon.component";
import {UpdateCouponComponent} from "../update-coupon/update-coupon.component";
import {TabsComponent} from "../../../component/app-tab/tabs.component";

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.scss']
})
export class CouponComponent {
  @ViewChild('addCoupon') addCoupon: AddCouponComponent;
  @ViewChild('updateCoupon') updateCoupon: UpdateCouponComponent;
  @ViewChild(TabsComponent) tabsComponent: TabsComponent;

  dataUpdate: any;

  onAddCoupon() {
    this.tabsComponent.openTab('Thêm mã giảm giá', 'fa-solid fa-plus', this.addCoupon, {}, true);
  }

  onUpdateCoupon(coupon: any) {
    this.dataUpdate = coupon;
    this.tabsComponent.openTab(`Chi tiết mã giảm giá ${coupon.name}`, 'fa-regular fa-eye', this.updateCoupon, this.dataUpdate, true);
  }


}
