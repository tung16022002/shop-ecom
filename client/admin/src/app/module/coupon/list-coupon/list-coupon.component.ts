import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import { CouponService } from "../../../service/coupon.service";
import { MessageService } from "primeng/api";
import { interval } from 'rxjs';
import {StateService} from "../../../service/state.service";
import {AppEvent} from "../../../enum/app.state";

@Component({
  selector: 'app-list-coupon',
  templateUrl: './list-coupon.component.html',
  styleUrls: ['./list-coupon.component.scss']
})
export class ListCouponComponent implements OnInit, OnDestroy {
  @Output() addCoupon = new EventEmitter<any>();
  @Output() updateCoupon = new EventEmitter<any>();

  listCoupon: any;
  remainingTime: { [key: string]: string } = {};
  private appStateEvent = {};

  constructor(
    private couponsService: CouponService,
    private messageService: MessageService,
    private appState: StateService
  ) {}

  ngOnInit(): void {
    this.initData();

    // Update the remaining time every second
    interval(1000).subscribe(() => {
      this.updateRemainingTime();
    });

   this.appStateEvent[AppEvent.RELOAD_LIST_COUPON] = this.appState.subscribe(AppEvent.RELOAD_LIST_COUPON, () => {
      this.initData();
    })
  }

  initData() {
    this.couponsService.getAllCoupons().subscribe(data => {
      this.listCoupon = data;
      this.updateRemainingTime();
    });
  }

  ngOnDestroy(): void {
    Object.keys(this.appStateEvent).forEach(k => {
      this.appState.unSubscribe(k, this.appStateEvent[k]);
    });
  }


  updateRemainingTime() {
    const currentTime = new Date().getTime();
    this.listCoupon.forEach((coupon: any) => {
      const endDate = new Date(coupon.endDate).getTime();
      const timeDiff = endDate - currentTime;

      if (timeDiff > 0) {
        const hours = Math.floor((timeDiff / (1000 * 60 * 60)) % 24);
        const minutes = Math.floor((timeDiff / (1000 * 60)) % 60);
        const seconds = Math.floor((timeDiff / 1000) % 60);
        this.remainingTime[coupon.code] = `${hours}h ${minutes}m ${seconds}s`;
      } else {
        this.remainingTime[coupon.code] = 'Expired';
      }
    });
  }

  deleteCoupon(code: any) {
    this.couponsService.deleteCoupon(code).subscribe(data => {
      this.messageService.add({ severity: 'info', summary: 'Xóa thành công' });
      this.initData();
    });
  }
}
