import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {CouponService} from "../../../service/coupon.service";
import {MessageService} from "primeng/api";
import {AppEvent} from "../../../enum/app.state";
import {StateService} from "../../../service/state.service";

@Component({
  selector: 'app-update-coupon',
  templateUrl: './update-coupon.component.html',
  styleUrls: ['./update-coupon.component.scss']
})
export class UpdateCouponComponent implements OnInit, OnDestroy{

  @Input() coupon;

  formData: FormGroup;
  private appStateEvent = {};

  constructor(
    private couponService: CouponService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private appState: StateService
  ) {
  }

  ngOnInit(): void {
    this.initData();
  }
  initData() {
    const starDate = new Date(this.coupon.startDate);
    const endDate = new Date(this.coupon.endDate);
    console.log(this.coupon)

    this.formData = this.fb.group({
      name: [this.coupon.name,[Validators.required, Validators.minLength(3), Validators.maxLength(15)] ],
      quantity: [this.coupon.quantity, [Validators.required, Validators.min(1)] ],
      discountPercent: [this.coupon.discountPercent, [Validators.required, Validators.min(1) , Validators.max(100)]],
      maxDiscountAmount: [this.coupon.maxDiscountAmount, [Validators.required, Validators.min(1)]],
      startDate: [starDate, Validators.required],
      endDate: [endDate, Validators.required],
    } , { validators: this.dateRangeValidator() })
  }

  ngOnDestroy(): void {
    Object.keys(this.appStateEvent).forEach(k => {
      this.appState.unSubscribe(k, this.appStateEvent[k]);
    });
  }

  dateRangeValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const startDate = control.get('startDate')?.value;
      const endDate = control.get('endDate')?.value;

      if (startDate && endDate && new Date(startDate) > new Date(endDate)) {
        return { dateRangeInvalid: true }; // Trả về lỗi nếu startDate > endDate
      }
      return null; // Không có lỗi
    };
  }

  toLocalISOString(date: Date): string {
    const offset = date.getTimezoneOffset();
    const localDate = new Date(date.getTime() - (offset * 60 * 1000));
    return localDate.toISOString().slice(0, -1); // Bỏ 'Z' để giữ nguyên múi giờ
  }

  onSubmit() {
    if (this.formData.invalid) {
      this.formData.markAllAsTouched();
      return;
    }

    // Đặt giờ vào giữa ngày để tránh bị lệch
    const startDate = new Date(this.formData.get('startDate').value);
    const endDate = new Date(this.formData.get('endDate').value);

    startDate.setSeconds(0, 0);
    endDate.setSeconds(0, 0);

    const formData = {
      ...this.formData.value,
      startDate: this.toLocalISOString(startDate),  // Đảm bảo format đúng
      endDate: this.toLocalISOString(endDate),      // Đảm bảo format đúng
      id: this.coupon.id,
      code: this.coupon.code
    };

    this.couponService.updateCoupon(formData).subscribe(data => {
      this.messageService.add({severity: 'info', summary: 'Thành công', detail: 'Cập nhật mã giảm giá thành công'});
      this.appState.notify(AppEvent.RELOAD_LIST_COUPON, true);

    });
  }

}
