import { Component, OnInit } from '@angular/core';
import { OrderService } from "../../service/order.service";
import {ProductService} from "../../service/product.service";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  data: any;
  options: any;
  totalOrder: any;
  totalProduct: any;
  totalUser: any;
  totalMoney: any;
  pageInfo = {
    currentPage: 1,
    recordPerPage: 20,
  }
  monthlyOrderCount: number[] = [];

  constructor(
    private orderService: OrderService,
    private productService: ProductService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.getCountOrder();
  }

  getCountOrder() {
    this.orderService.getAllOrders().subscribe(data => {
      // Khởi tạo mảng monthlyOrderCount và monthlyMoneyCount
      let monthlyOrderCount = Array(12).fill(0);
      let monthlyMoneyCount = Array(12).fill(0); // Mảng lưu tổng tiền cho mỗi tháng

      data.forEach(order => {
        const month = new Date(order.orderDate).getMonth(); // Lấy tháng từ ngày đặt hàng
        monthlyOrderCount[month] += 1; // Cộng số lượng đơn hàng vào tháng tương ứng
        if (order.status == 'done' || order.paymentMethod == "BANK") {
          monthlyMoneyCount[month] += order.totalMoney; // Cộng tổng tiền vào tháng tương ứng
        }
      });

      // Cập nhật dữ liệu biểu đồ sau khi đã có monthlyMoneyCount
      this.updateChart(monthlyOrderCount, monthlyMoneyCount);
    });

    // Các API khác để lấy tổng số đơn hàng, sản phẩm, người dùng, và tổng tiền
    this.orderService.count(this.pageInfo).subscribe(data => this.totalOrder = data);
    this.productService.countProducts(this.pageInfo).subscribe(data => this.totalProduct = data);
    this.userService.getAll().subscribe(data => this.totalUser = data.length);
    this.orderService.getAllOrders().subscribe(data => {
      this.totalMoney = data
        .filter(order => order.status == 'done' || order.paymentMethod == "BANK")
        .reduce((acc, order) => acc + order.totalMoney, 0);
    });
  }

  updateChart(monthlyOrderCount: number[], monthlyMoneyCount: number[]) {
    const documentStyle = getComputedStyle(document.documentElement);
    const textColor = documentStyle.getPropertyValue('--text-color');
    const textColorSecondary = documentStyle.getPropertyValue('--text-color-secondary');
    const surfaceBorder = documentStyle.getPropertyValue('--surface-border');

    this.data = {
      labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
      datasets: [
        {
          label: 'Số lượng đơn hàng',
          backgroundColor: documentStyle.getPropertyValue('--blue-500'),
          borderColor: documentStyle.getPropertyValue('--blue-500'),
          data: monthlyOrderCount // Sử dụng dữ liệu monthlyOrderCount cho biểu đồ
        },
        {
          label: 'Tổng tiền đơn hàng',
          backgroundColor: documentStyle.getPropertyValue('--green-500'),
          borderColor: documentStyle.getPropertyValue('--green-500'),
          data: monthlyMoneyCount // Sử dụng dữ liệu monthlyMoneyCount cho biểu đồ tổng tiền
        }
      ]
    };

    this.options = {
      maintainAspectRatio: false,
      aspectRatio: 0.8,
      plugins: {
        legend: {
          labels: {
            color: textColor
          }
        }
      },
      scales: {
        x: {
          ticks: {
            color: textColorSecondary,
            font: {
              weight: 500
            }
          },
          grid: {
            color: surfaceBorder,
            drawBorder: false
          }
        },
        y: {
          ticks: {
            color: textColorSecondary
          },
          grid: {
            color: surfaceBorder,
            drawBorder: false
          }
        }
      }
    };
  }

}
