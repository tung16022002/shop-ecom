import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {ChartModule} from "primeng/chart";
import {TabsComponent} from "../../component/app-tab/tabs.component";
import {TabComponent} from "../../component/app-tab/tab.component";


@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ButtonModule,
    CardModule,
    ChartModule,
    TabsComponent,
    TabComponent
  ]
})
export class DashboardModule { }
