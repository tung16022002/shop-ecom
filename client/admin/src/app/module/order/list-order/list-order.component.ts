import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {OrderService} from "../../../service/order.service";
import {ProductService} from "../../../service/product.service";
import {StateService} from "../../../service/state.service";
import {AppEvent} from "../../../enum/app.state";

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.scss']
})
export class ListOrderComponent implements OnInit, OnDestroy{
  @Output() updateOrder = new EventEmitter<any>();

  orders: any;
  pageInfo = {
    currentPage: 1,
    recordPerPage: 20,
    status: '',
  }
  selectedStatus: any;
  listStatus = [
    {name: 'Chờ xác nhận', value: 'pending'},
    {name: 'Chuẩn bị hàng', value: 'prepare'},
    {name: 'Đang giao hàng', value: 'delivery'},
    {name: 'Đã nhận được hàng', value: 'done'},
    {name: 'Hủy đơn hàng', value: 'cancel'}
  ]

  totalRecord: number;
  appStateEvent = {};


  constructor(
    private orderService: OrderService,
    private productService: ProductService,
    private appState: StateService
  ) {
  }

  ngOnInit() {
    this.searchOrders();
    this.appStateEvent[AppEvent.RELOAD_LIST_ORDER] = this.appState.subscribe(AppEvent.RELOAD_LIST_ORDER, () => {
      this.searchOrders();
    })
  }

  ngOnDestroy(): void {
    Object.keys(this.appStateEvent).forEach(k => {
      this.appState.unSubscribe(k, this.appStateEvent[k]);
    });
  }

  searchOrders() {
    this.orderService.search(this.pageInfo).subscribe(data => {
      this.orders = data;
      this.getProductDetail(this.orders);
      // Duyệt qua từng đơn hàng để lấy chi tiết sản phẩm


    });
    this.orderService.count(this.pageInfo).subscribe(data => this.totalRecord = data);
  }

  searchByStatus() {
    this.pageInfo.status = this.selectedStatus;
    this.searchOrders()
  }

  clearSearch() {
    this.pageInfo.status = "";
    this.selectedStatus = "";
    this.searchOrders();
  }

  onPageChange(event: any) {
    this.pageInfo.currentPage = event.page + 1;
    this.pageInfo.recordPerPage = event.rows;
    this.searchOrders();
  }

  getProductDetail(orders: any) {
    orders.forEach(order => {
      const productIds = order.orderDetails.map(detail => detail.productId); // Lấy danh sách productId từ orderDetails

      // Gọi API để lấy thông tin chi tiết sản phẩm
      this.productService.getProductsByIds(productIds).subscribe(products => {
        // Cập nhật thông tin sản phẩm vào orderDetails
        order.orderDetails.forEach(detail => {
          const product = products.find(p => p.id === detail.productId);
          if (product) {
            detail.name = product.name;
            detail.image = product.thumbnail;
            detail.totalProduct = product.totalProduct
          }
        });
      });
    });
  }

  getStatusName(status: string): string {
    switch (status) {
      case 'pending':
        return 'Chờ xác nhận';
      case 'prepare':
        return 'Chuẩn bị hàng';
      case 'delivery':
        return 'Đang giao hàng';
      case 'done':
        return 'Đã nhận được hàng';
      case 'cancel':
        return 'Hủy đơn hàng'
      default:
        return 'Không xác định';
    }
  }
}
