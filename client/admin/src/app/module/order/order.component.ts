import {Component, ViewChild} from '@angular/core';
import {UpdateOrderComponent} from "./update-order/update-order.component";
import {TabsComponent} from "../../component/app-tab/tabs.component";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent {
  @ViewChild('updateOrder') updateOrder: UpdateOrderComponent;
  @ViewChild(TabsComponent) tabsComponent: TabsComponent;
  dataUpdate: any;

  onUpdateOrder(order: any) {
    this.dataUpdate = order;
    this.tabsComponent.openTab(
      `Cập nhật đơn hàng của ${order.fullName}`,
      'fa-regular fa-pen-to-square',
      this.updateOrder,
      this.dataUpdate,
      true
    )
  }
}
