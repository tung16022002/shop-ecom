import { NgModule } from '@angular/core';
import {CommonModule, CurrencyPipe, DatePipe, NgForOf, NgIf} from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order.component';
import { ListOrderComponent } from './list-order/list-order.component';
import { UpdateOrderComponent } from './update-order/update-order.component';
import {TabComponent} from "../../component/app-tab/tab.component";
import {TabsComponent} from "../../component/app-tab/tabs.component";
import {ButtonModule} from "primeng/button";
import {RouterLink} from "@angular/router";
import {TagModule} from "primeng/tag";
import {CardModule} from "primeng/card";
import {ImageModule} from "primeng/image";
import {DividerModule} from "primeng/divider";
import {SharedModule} from "primeng/api";
import {TimelineModule} from "primeng/timeline";
import {DropdownModule} from "primeng/dropdown";
import {FormsModule} from "@angular/forms";
import {ToastModule} from "primeng/toast";
import {PaginatorModule} from "primeng/paginator";


@NgModule({
  declarations: [
    OrderComponent,
    ListOrderComponent,
    UpdateOrderComponent
  ],
    imports: [
        CommonModule,
        OrderRoutingModule,
        TabComponent,
        TabsComponent,
        ButtonModule,
        CurrencyPipe,
        DatePipe,
        NgForOf,
        NgIf,
        RouterLink,
        TagModule,
        CardModule,
        ImageModule,
        DividerModule,
        SharedModule,
        TimelineModule,
        DropdownModule,
        FormsModule,
        ToastModule,
        DatePipe,
        NgForOf,
        SharedModule,
        TimelineModule,
        PaginatorModule
    ]
})
export class OrderModule { }
