import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {OrderService} from "../../../service/order.service";
import {MessageService} from "primeng/api";
import {DatePipe} from "@angular/common";
import {StateService} from "../../../service/state.service";
import {AppEvent} from "../../../enum/app.state";

@Component({
  selector: 'app-update-order',
  templateUrl: './update-order.component.html',
  styleUrls: ['./update-order.component.scss'],
  providers: [MessageService, DatePipe]
})
export class UpdateOrderComponent implements OnInit, OnDestroy{
  @Input() order;
  events: any;
  statusHistory: any;

  status = [
    { name: 'Chờ xác nhận', code: 'pending' },
    { name: 'Chuẩn bị hàng', code: 'prepare' },
    { name: 'Đang giao hàng ', code: 'delivery' },
    { name: 'Đã nhận được hàng', code: 'done' },
  ];

  statusSelect: string;
  appStateEvent = {} ;

  constructor(
    private orderService: OrderService,
    private messageService: MessageService,
    private datePipe: DatePipe,
    private appState: StateService
  ) {
  }

  ngOnInit() {
   this.updateStatusHistory(this.order);
    this.statusSelect = this.order.status
  }

  ngOnDestroy(): void {
    Object.keys(this.appStateEvent).forEach(k => {
      this.appState.unSubscribe(k, this.appStateEvent[k]);
    });
  }

  hasOutOfStockItems(): boolean {
    return this.order.orderDetails.some(data => data.quantity > data.totalProduct);
  }


  updateStatusHistory(order:any) {
    const allStatuses = [
      { status: 'pending', displayName: 'Chờ xác nhận', icon: 'pi pi-th-large', color: '#9C27B0' },
      { status: 'prepare', displayName: 'Đang chuẩn bị hàng', icon: 'pi pi-cog', color: '#673AB7' },
      { status: 'delivery', displayName: 'Đang giao hàng', icon: 'pi pi-shopping-cart', color: '#FF9800' },
      { status: 'done', displayName: 'Đã nhận được hàng', icon: 'pi pi-check', color: 'green' },
    ];
    this.statusHistory = order.statusHistory;
    this.events = allStatuses.map((statusInfo) => {
      const matchedHistory = this.statusHistory.find((history) => history.status === statusInfo.status);
      return {
        status: statusInfo.displayName,
        date: matchedHistory ? this.datePipe.transform(matchedHistory.time, 'dd/MM/yyyy HH:mm:ss') : 'Chưa hoàn thành',
        icon: statusInfo.icon,
        color: matchedHistory ? statusInfo.color : 'lightgray',  // Nếu chưa có trong history, màu sẽ là 'lightgray'
      };
    });

  }

  updateOrder() {

    const data = {
      ...this.order,
      status: this.statusSelect
    }
    this.orderService.updateOrder(data).subscribe(data => {
      this.messageService.add({severity: 'info', summary: 'Đơn hàng đã được cập nhật', life: 1500});
      this.appState.notify(AppEvent.RELOAD_LIST_ORDER, true);
      this.orderService.getOrderById(data.id).subscribe(data => {
        this.updateStatusHistory(data);
      })
    })



  }

  cancelOrder(){
    if(this.order.status === 'pending' || this.order.status === 'prepare') {
      const data = {
        ...this.order,
        status: 'cancel'
      }
      this.orderService.updateOrder(data).subscribe(data => {
        this.messageService.add({severity: 'warn', summary: 'Đã hủy đơn hàng ', life: 1500})
      })
    } else {
      this.messageService.add({severity: 'error', summary: 'Hủy đơn hàng thất bại ', detail: 'Không thể hủy đơn hàng đã giao ', life: 2000 })
    }
  }

}
