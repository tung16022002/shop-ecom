import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CategoryService} from "../../../service/category.serivce";
import {ProductAttributesService} from "../../../service/productAttributes.service";
import {ProductService} from "../../../service/product.service";
import {MessageService} from "primeng/api";
import {StateService} from "../../../service/state.service";
import {AppEvent} from "../../../enum/app.state";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss'],
  providers: [MessageService]
})
export class AddProductComponent implements OnInit, OnDestroy {

  productForm: FormGroup;
  categories: any;
  attr: any;
  selectedAttr: any;
  uploadedFiles: any[] = [];
  childCategories: any[] = [];
  productData: any;
  pageInfo = {
    currentPage: 1,
    recordPerPage: 10,
  };
  totalRecord: number;
  appStateEvent = {};

  constructor(private fb: FormBuilder,
              private categoryService: CategoryService,
              private attributeService: ProductAttributesService,
              private productService: ProductService,
              private messageService: MessageService,
              private appState: StateService
  ) { }

  ngOnInit(): void {
    this.productForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]],
      price: [1000, [Validators.required, Validators.min(1000)]],
      thumbnail: [''],
      description: [''],
      favourite: [false],
      categoryId: ['', Validators.required],  // Danh mục cha
      subCategoryId: ['', Validators.required],  // Danh mục con (nếu có)
      quantity: [0, [Validators.required, Validators.min(0), Validators.max(10000)]],
      totalProduct: [1, [Validators.required, Validators.min(1)]],
      listThumbnail: [''],  // Sử dụng FormArray nếu có nhiều hình ảnh
      attributes: ['']  // Danh sách các thuộc tính sản phẩm
    });

    this.loadCategory();

    this.productForm.get('categoryId')?.valueChanges.subscribe(parentCategory => {
      this.updateChildCategories(parentCategory);
    });

    this.productForm.get('subCategoryId').valueChanges.subscribe(subCate => {
      this.updateAttributeProduct(subCate);
    })
  }

  ngOnDestroy(): void {
    Object.keys(this.appStateEvent).forEach(k => {
      this.appState.unSubscribe(k, this.appStateEvent[k]);
    });
  }

  onUpload(event: any) {
    const files = event.files;
    this.productService.uploadProductImages(files).subscribe(data => {
      this.uploadedFiles = data.map(data => data.imageUrl);
      this.messageService.add({severity: 'info', summary: 'Tải ảnh thành công', detail: ''});
    });
  }

  updateAttributeProduct(code: string) {
   this.attributeService.getByCode(code).subscribe(data => {
     this.attr = data.map(attribute => {
       return {
         ...attribute,
         value: attribute.value.map(val => ({ label: val, value: val } || []))
       };
     });

   })
  }

  updateChildCategories(parentCategoryCode: string) {
    const selectedCategory = this.categories.find(cat => cat.value === parentCategoryCode);
    this.childCategories = selectedCategory ? selectedCategory.children : [];
  }

  loadCategory() {
    this.categoryService.getAllCategoriesWithChildren().subscribe(data => {
      this.categories = data;
    });
  }

  onSubmit() {
    if (this.productForm.invalid) {
      this.productForm.markAllAsTouched();
      return;
    }

    const formData = this.productForm.value;

    // Tạo mảng attributes với định dạng {key: "Tên thuộc tính", value: "Giá trị"}
    const attributes = this.attr.map(attr => ({
      key: attr.key,
      value: formData.attributes
    }));

    console.log(attributes)

    this.productData = {
      ...formData,
      attributes: attributes,
      listThumbnail: this.uploadedFiles
    };

    console.log('Product data:', this.productData);

    this.productService.createProduct(this.productData).subscribe(data => {
      this.messageService.add({severity: 'success', summary: 'Thêm sản phẩm thành công', detail: ''});
      this.appState.notify(AppEvent.RELOAD_LIST_PRODUCT, true);
      this.productForm.value.clear;
    });
  }

  searchProducts() {
    this.productService.searchProducts(this.pageInfo).subscribe(data => {
      console.log('Searched products:', data);
    });
    this.productService.countProducts(this.pageInfo).subscribe(data => this.totalRecord = data);
  }
}
