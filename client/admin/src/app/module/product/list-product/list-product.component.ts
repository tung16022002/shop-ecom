import {Component, EventEmitter, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {ProductService} from "../../../service/product.service";
import { ConfirmationService, MessageService, ConfirmEventType } from 'primeng/api';
import {CategoryService} from "../../../service/category.serivce";
import {StateService} from "../../../service/state.service";
import {AppEvent} from "../../../enum/app.state";

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss'],
  providers: [ConfirmationService, MessageService]

})
export class ListProductComponent implements OnInit, OnChanges, OnDestroy{
  @Output() addProduct = new EventEmitter<any>();
  @Output() updateProduct = new EventEmitter<any>();

  searchText: any;
  categories: any[] = [];
  categorySearch: any;
  subCategory: any[] =[];
  subCategorySearch: any;

  listStock= [
    {name: 'Còn hàng', value: 'instock'},
    {name: 'Còn ít hàng', value: 'lowstock'},
    {name: 'Hết hàng ', value: 'outstock'}
  ]

  products: any;
  pageInfo = {
    currentPage: 1,
    recordPerPage: 10,
    categoryId: '',
    subCategoryId: '',
    keyword: '',
    stockStatus: '',
    typeSorted: ''
  };
  totalRecord: number;
  stockStatus: string = '';
  appStateEvent = {};

  constructor(
    private productService: ProductService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private categoryService: CategoryService,
    private appState: StateService
  ) {
  }
  ngOnInit(): void {
    this.searchProducts();
    this.loadCategory();

    this.appStateEvent[AppEvent.RELOAD_LIST_PRODUCT] = this.appState.subscribe(AppEvent.RELOAD_LIST_PRODUCT, () => {
      this.searchProducts();
    });
  }

  ngOnDestroy(): void {
    Object.keys(this.appStateEvent).forEach(k => {
      this.appState.unSubscribe(k, this.appStateEvent[k]);
    });
  }

  loadCategory() {
    this.categoryService.getAllCategoriesWithChildren().subscribe(data =>
      this.categories = data
    )
  ;}

  onCategoryChange(event: any) {
    const parentCategoryCode = event.value;
    this.updateChildCategories(parentCategoryCode);
    this.categorySearch = parentCategoryCode;
    this.subCategorySearch = '';
  }

  updateChildCategories(parentCode: string) {
    const selectedCategory = this.categories.find(cat => cat.value == parentCode);
    this.subCategory = selectedCategory ? selectedCategory.children: [];

  }

  clearSearch() {
    this.pageInfo.keyword = '';
    this.pageInfo.stockStatus = '';
    this.pageInfo.categoryId = '';
    this.pageInfo.subCategoryId = '';
    this.searchText = '';
    this.subCategorySearch = ''
    this.categorySearch = '';
    this.stockStatus = '';

    this.productService.searchProducts(this.pageInfo).subscribe(data => {
      this.products = data;
    })
    this.productService.countProducts(this.pageInfo).subscribe(data => this.totalRecord = data);
  }

  filterProduct() {
    if(this.subCategorySearch) {
      this.pageInfo = {
        currentPage: 1,
        recordPerPage: 10,
        categoryId: '',
        subCategoryId: this.subCategorySearch,
        keyword: this.searchText,
        stockStatus: this.stockStatus,
        typeSorted: ""
      };

    } else {
      this.pageInfo = {
        currentPage: 1,
        recordPerPage: 10,
        categoryId: this.categorySearch,
        subCategoryId: '',
        keyword: this.searchText,
        stockStatus: this.stockStatus,
        typeSorted: ""
      }

    }

    this.productService.searchProducts(this.pageInfo).subscribe(data => {
      this.products = data;
    })
    this.productService.countProducts(this.pageInfo).subscribe(data => this.totalRecord = data);

  }

  searchProducts() {
    this.productService.searchProducts(this.pageInfo).subscribe(data => {
      this.products = data;
    })
    this.productService.countProducts(this.pageInfo).subscribe(data => this.totalRecord = data);
  }

  onPageChange(event: any) {
    this.pageInfo.currentPage = event.page + 1;
    this.pageInfo.recordPerPage = event.rows;
    this.searchProducts();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.searchProducts();
  }

  deleteProduct(product: any) {
    this.confirmationService.confirm({
      message: `Bạn có muốn xóa sản phẩm "${product.name}"?`,
      header: 'Xóa sản phẩm',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.productService.deleteProduct(product.id).subscribe(data => {
          this.searchProducts();
          this.messageService.add({ severity: 'info', summary: 'Xóa sản phẩm thành công', detail: '' });
        })
      },
      reject: (type: ConfirmEventType) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            this.messageService.add({ severity: 'error', summary: 'Hủy', detail: '' });
            break;
          case ConfirmEventType.CANCEL:
            this.messageService.add({ severity: 'warn', summary: 'Hủy', detail: '' });
            break;
        }
      }
    });
  }

}
