import {Component, ViewChild} from '@angular/core';
import {TabsComponent} from "../../component/app-tab/tabs.component";
import {AddProductComponent} from "./add-product/add-product.component";
import {UpdateProductComponent} from "./update-product/update-product.component";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {
  @ViewChild('addProduct') addProduct: AddProductComponent;
  @ViewChild('updateProduct') updateProduct: UpdateProductComponent;
  @ViewChild(TabsComponent) tabsComponent: TabsComponent;
  dataUpdate: any;

  onAddProduct() {
    this.tabsComponent.openTab('Thêm sản phẩm mới', 'fa-solid fa-plus', this.addProduct, {}, true);
  }

  onUpdateProduct(product: any) {
    this.dataUpdate = product;
    console.log(this.dataUpdate);
    this.tabsComponent.openTab(`Cập nhật sản phẩm ${product.name}`, 'fa-regular fa-pen-to-square', this.updateProduct, this.dataUpdate, true)
  }
}
