import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import {TestComponent} from "../../component/people/test.component";
import {TabsComponent} from "../../component/app-tab/tabs.component";
import {TabComponent} from "../../component/app-tab/tab.component";
import { ListProductComponent } from './list-product/list-product.component';
import {ButtonModule} from "primeng/button";
import {TagModule} from "primeng/tag";
import {PaginatorModule} from "primeng/paginator";
import { AddProductComponent } from './add-product/add-product.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import {ReactiveFormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {InputTextareaModule} from "primeng/inputtextarea";
import {MultiSelectModule} from "primeng/multiselect";
import {FileUploadModule} from "primeng/fileupload";
import {ToastModule} from "primeng/toast";
import {ControlMessageComponent} from "../../shared/control-message/control-message.component";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {FilterPipe} from "../../shared/pipe/filter.pipe";


@NgModule({
    declarations: [
        ProductComponent,
        ListProductComponent,
        AddProductComponent,
        UpdateProductComponent
    ],
    imports: [
        CommonModule,
        ProductRoutingModule,
        TestComponent,
        TabsComponent,
        TabComponent,
        ButtonModule,
        TagModule,
        PaginatorModule,
        ReactiveFormsModule,
        InputTextModule,
        InputTextareaModule,
        MultiSelectModule,
        FileUploadModule,
        ToastModule,
        ControlMessageComponent,
        ConfirmDialogModule,
        FilterPipe,
    ],

    exports: [
        UpdateProductComponent
    ]
})
export class ProductModule { }
