import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CategoryService} from "../../../service/category.serivce";
import {ProductAttributesService} from "../../../service/productAttributes.service";
import {ProductService} from "../../../service/product.service";
import {MessageService} from "primeng/api";
import {StateService} from "../../../service/state.service";
import {AppEvent} from "../../../enum/app.state";

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.scss'],
  providers: [MessageService]
})
export class UpdateProductComponent implements OnInit, OnDestroy {
  productForm: FormGroup;
  productData: any;
  @Input() product;
  @Output() saveProduct = new EventEmitter<any>();
  categories: any;
  attr: any;
  uploadedFiles: any[] = [];
  childCategories: any[] = [];

  pageInfo = {
    currentPage: 1,
    recordPerPage: 10,
  };
  totalRecord: number;
  appStateEvent =  {};

  constructor(
    private fb: FormBuilder,
    private categoryService: CategoryService,
    private attributeService: ProductAttributesService,
    private productService: ProductService,
    private messageService: MessageService,
    private appState: StateService
  ) {}

  ngOnInit() {
    this.productForm = this.fb.group({
      name: [this.product.name, [Validators.required, Validators.minLength(3), Validators.maxLength(200)]],
      price: [this.product.price, [Validators.required, Validators.min(1000)]],
      thumbnail: [this.product.thumbnail],
      description: [this.product.description],
      favourite: [false],
      categoryId: [this.product.categoryId, Validators.required],
      subCategoryId: [this.product.subCategoryId, Validators.required],
      quantity: [0, [Validators.required, Validators.min(0), Validators.max(10000)]],
      totalProduct: [this.product.totalProduct, [Validators.required, Validators.min(1)]],
      listThumbnail: [this.product.listThumbnail],
      textSearch: [this.product.textSearch],
      attributes: [this.attr]  // Khởi tạo với giá trị của attr
    });


    this.loadCategory();

    this.productForm.get('categoryId')?.valueChanges.subscribe(parentCategory => {
      this.updateChildCategories(parentCategory);
      this.updateAttributeProduct(parentCategory);
    });
  }

  ngOnDestroy(): void {
    Object.keys(this.appStateEvent).forEach(k => {
      this.appState.unSubscribe(k, this.appStateEvent[k]);
    });
  }

  onUpload(event: any) {
    const files = event.files;
    this.productService.uploadProductImages(files).subscribe(data => {
      this.uploadedFiles = data.map(data => data.imageUrl);
      this.messageService.add({ severity: 'info', summary: 'Tải ảnh thành công', detail: '' });
    });
  }

  updateAttributeProduct(code: string) {
    this.attributeService.getByCode(code).subscribe(data => {
      this.attr = data.map(attribute => {
        return {
          ...attribute,
          value: attribute.value.map(val => ({ label: val, value: val }))
        };
      });
    });
  }

  updateChildCategories(parentCategoryCode: string) {
    const selectedCategory = this.categories.find(cat => cat.value === parentCategoryCode);
    this.childCategories = selectedCategory ? selectedCategory.children : [];
  }

  loadCategory() {
    this.categoryService.getAllCategoriesWithChildren().subscribe(data => {
      this.categories = data;
      this.updateChildCategories(this.product.categoryId);
      this.updateAttributeProduct(this.product.categoryId);
    });
  }

  onSubmit() {
    if (this.productForm.invalid) {
      this.productForm.markAllAsTouched();
      return;
    }

    const formData = this.productForm.value;

    const attributes = this.attr.map(a => ({
      key: a.key,
      value: formData.attributes
    }));

    const listThumbnail = [
      ...(this.product.listThumbnail || []),
      ...this.uploadedFiles
    ];

    this.productData = {
      id: this.product.id,
      ...formData,
      attributes: attributes,
      listThumbnail: listThumbnail
    };

    this.productService.updateProduct(this.productData).subscribe(data => {
      this.messageService.add({ severity: 'success', summary: 'Cập nhật sản phẩm thành công', detail: '' });
      this.appState.notify(AppEvent.RELOAD_LIST_PRODUCT, true);
    });
  }


}
