import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../service/user.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit{
  users: any = [];
  searchText: any;

  constructor(
    private userService: UserService,
    private messageService: MessageService
  ) {
  }

  ngOnInit() {
   this.initData();
  }

  initData() {
    this.userService.getAll().subscribe(data => {
      this.users = data;
    });
  }

  updateStatus(data) {
   const user = {
     ...data,
     active: !data.active,
   }
    this.userService.update(user).subscribe(data => {
      this.messageService.add({severity: 'success', summary: 'Cập nhật thành công'})
      this.initData();

    })
  }
}
