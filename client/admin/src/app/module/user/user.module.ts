import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { ListUserComponent } from './list-user/list-user.component';
import {TabsComponent} from "../../component/app-tab/tabs.component";
import {TabComponent} from "../../component/app-tab/tab.component";
import {ToggleButtonModule} from "primeng/togglebutton";
import {FormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {ButtonModule} from "primeng/button";
import {FilterPipe} from "../../shared/pipe/filter.pipe";


@NgModule({
  declarations: [
    UserComponent,
    ListUserComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    TabsComponent,
    TabComponent,
    ToggleButtonModule,
    FormsModule,
    InputTextModule,
    ButtonModule,
    FilterPipe
  ]
})
export class UserModule { }
