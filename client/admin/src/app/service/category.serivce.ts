import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from "../enviroment/enviroment";
import {CategoriesTree} from "../dto/categoriesTree";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private apiUrl = `${environment.apiUrl}/categories`;// Replace with your Spring Boot backend API base URL

  constructor(private http: HttpClient) {
  }

  getCategoryByCode(code: string): Observable<any> {
    const params = new HttpParams().set('code', code);
    return this.http.get<any>(`${this.apiUrl}/getCategoriesByCode`, { params });
  }

  getCategoryParent(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/parent`);
  }

  createCategory(category: any):Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/create`, category)
  }

  deleteCategory(id: string): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }

  updateCategory(category: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/updateNameCategories`, category)
  }

  getCategoryChildren(parent: any): Observable<any> {
    return this.http.get<void>(`${this.apiUrl}/children/${parent}`);
  }

  // //Lay danh sach Tree Category bang code
  getCategoriesTreeByParent(parent: string): Observable<CategoriesTree[]> {
    const params = new HttpParams().set('parent', parent);
    return this.http.get<CategoriesTree[]>(`${this.apiUrl}/getCategoriesTreeByParent`, {params});
  }

  // getCategoriesByCode(code: string): Observable<Categories> {
  //   const params = new HttpParams().set('code', code);
  //   return this.http.get<Categories>(`${this.apiUrl}/getCategoriesByCode`, {params});
  // }

  getAllCategoriesWithChildren(): Observable<CategoriesTree[]> {
    return this.http.get<CategoriesTree[]>(`${this.apiUrl}/getAll`);
  }

  uploadAvatar(id: string, file: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    const uploadUrl = `${this.apiUrl}/upload/${id}`;
    return this.http.post<any>(uploadUrl, formData).pipe(
      catchError(err => {
        return throwError(() => new Error(err.message || 'Failed to upload avatar'));
      })
    );
  }


}
