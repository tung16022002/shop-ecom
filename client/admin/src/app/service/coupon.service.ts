import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from "../enviroment/enviroment";

@Injectable({
  providedIn: 'root'
})
export class CouponService {
  private baseUrl = `${environment.apiUrl}/coupons`;

  constructor(private http: HttpClient) {}

  // Lấy tất cả các mã giảm giá
  getAllCoupons(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/getAll`);
  }

  // Lấy mã giảm giá đang active
  getActiveCoupons(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/getByActive`);
  }

  // Lấy mã giảm giá theo code
  getCouponByCode(code: string): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/${code}`);
  }

  // Thêm mới một mã giảm giá
  addCoupon(coupon: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/add`, coupon);
  }

  // Cập nhật một mã giảm giá
  updateCoupon(coupon: any): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/update`, coupon);
  }

  // Xóa mã giảm giá theo code
  deleteCoupon(code: string): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${code}`);
  }

  // Lấy danh sách mã giảm giá theo trạng thái
  getCouponsByStatus(request: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/status`, request);
  }
}
