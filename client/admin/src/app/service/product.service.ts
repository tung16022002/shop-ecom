import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../enviroment/enviroment";
import {ProductDTO} from "../dto/product";
import {ProductPageInfo} from "../dto/productPageInfo";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private apiUrl = `${environment.apiUrl}/products`;

  constructor(private http: HttpClient) {
  }

  getAllProducts(): Observable<ProductDTO[]> {
    return this.http.get<ProductDTO[]>(this.apiUrl);
  }

  getProductsByIds(productIds: string[]): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/getList`, productIds);
  }

  searchProducts(model: ProductPageInfo): Observable<ProductDTO[]> {
    return this.http.post<ProductDTO[]>(`${this.apiUrl}/search`, model);
  }



  countProducts(model: ProductPageInfo): Observable<number> {
    return this.http.post<number>(`${this.apiUrl}/count`, model);
  }

  getProductById(id: string): Observable<ProductDTO> {
    return this.http.get<ProductDTO>(`${this.apiUrl}/${id}`);
  }



  createProduct(product: any): Observable<any> {
    return this.http.post<any>(this.apiUrl, product);
  }

  updateProduct(product: any): Observable<any> {
    return this.http.put<any>(this.apiUrl, product);
  }

  deleteProduct(id: string): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/${id}`);
  }

  uploadProductImages( files: File[]): Observable<any> {
    const formData: FormData = new FormData();
    files.forEach(file => formData.append('files', file, file.name));
    return this.http.post<any>(`${this.apiUrl}/upload-list`, formData);
  }
}
