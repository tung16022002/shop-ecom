import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from "../enviroment/enviroment";
import {ProductAttributes} from "../dto/productAttributes"; // Đảm bảo đường dẫn này đúng

@Injectable({
  providedIn: 'root'
})
export class ProductAttributesService {
  private apiUrl = `${environment.apiUrl}/attributes`;

  constructor(private http: HttpClient) {}

  // Lấy product attribute bằng code
  getByCode(code: string): Observable<ProductAttributes[]> {
    return this.http.get<ProductAttributes[]>(`${this.apiUrl}/${code}`);
  }

  // Thêm product attribute mới
  // add(productAttributes: ProductAttributes): Observable<ProductAttributes> {
  //   return this.http.post<ProductAttributes>(`${this.apiUrl}/add`, productAttributes);
  // }
}
