import {Injectable} from '@angular/core';
import {jwtDecode} from "jwt-decode";


@Injectable({
  providedIn: 'root'
})
export class StartupService {

  constructor() {
  }

  loggedIn() {
    const token = localStorage.getItem('token');

    return token != null;
  }

  getCurrentUser(token): any {
    if (token)
      return jwtDecode(token);
    return null;
  }

}
