import {Injectable} from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private data = new Subject<Object>();
  private dataStream$ = this.data.asObservable();

  private subscriptions = new Map<string, Array<Function>>();

  constructor() {
    this.dataStream$.subscribe((data) => {
      this.onEvent(data);
    });
  }

  /**
   * Thông báo sự kiện
   * @param event
   * @param value
   * @param isChange
   */
  notify(event: any, value: any = null, isChange: boolean = false) {
    const current = this.data[event];

    if (isChange == false) {
      this.data.next({
        event: event,
        data: value
      });
      return;
    }

    if (current != value) {
      this.data[event] = value;

      this.data.next({
        event: event,
        data: this.data[event]
      });
    }
  }

  /**
   * Theo dõi sự kiện
   * @returns {number}: Chỉ số để xóa subscribe sự kiện
   * @param event
   * @param callback
   */
  subscribe(event: any, callback: Function): number {
    const subscribers = this.subscriptions.get(event) || [];
    subscribers.push(callback);

    this.subscriptions.set(event, subscribers);

    return subscribers.indexOf(callback);
  }

  /**
   * Xóa sự kiện sử dụng Index trả về khi subscribe
   * @param event
   * @param index
   */
  unSubscribe(event: any, index: number) {
    const subscribers = this.subscriptions.get(event) || [];

    subscribers.splice(index, 1);

    this.subscriptions.set(event, subscribers);
  }

  /**
   * Xóa sự kiện
   * @param event
   */
  unSubscribeAll(event: any) {
    delete this.data[event];
    this.subscriptions.delete(event);
  }

  private onEvent(data: any) {
    const subscribers = this.subscriptions.get(data.event) || [];

    subscribers.forEach((callback) => {
      callback(data.data);
    });
  }
}
