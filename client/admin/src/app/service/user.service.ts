import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, delay, map, Observable, throwError} from "rxjs";
import {ChangePasswordDTO, LoginData, UserDTO} from "../dto/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = 'http://localhost:8088/api/v1/users';

  constructor(private http: HttpClient) {
  }


  update(data: any): Observable<any> {
    return this.http.put<any>(this.apiUrl + "/update", data);
  }

  login(data: LoginData): Observable<any> {
    return this.http.post(this.apiUrl + "/login", data, {responseType: 'text'}).pipe(
      catchError(err => {
        return throwError(() => new Error(err.message || 'Server Error'));
      })
    );
  }

  getAll(): Observable<any> {
    return this.http.get(this.apiUrl);
  }

  validatePhoneNumber(phoneNumber: string): Observable<boolean> {
    return this.getAll().pipe(
      map(users => {
        return users.every((user: any) => user.phoneNumber !== phoneNumber);
      }),
      delay(1000)
    );
  }

  uploadAvatar(userId: string, file: File): Observable<UserDTO> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    const uploadUrl = `${this.apiUrl}/upload-avt/${userId}`;
    return this.http.post<UserDTO>(uploadUrl, formData).pipe(
      catchError(err => {
        return throwError(() => new Error(err.message || 'Failed to upload avatar'));
      })
    );
  }

  changePassword(phoneNumber: string, model: ChangePasswordDTO) {
    return this.http.put(`${this.apiUrl}/change-password/${phoneNumber}`, model);
  }

}
