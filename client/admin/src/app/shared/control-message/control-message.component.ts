import {Component, Input} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-control-message',
  template: `
    <div class="has-error" *ngIf="errorMessage !== null">{{ errorMessage }}</div>`,
  standalone: true,
  imports: [CommonModule],
  styles: ['.has-error {color: red;}']
})
export class ControlMessageComponent {
  @Input() control: AbstractControl;
  @Input() label: string;

  get errorMessage() {
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && !this.control.valid && (this.control.dirty || this.control.touched)) {
        return this.getValidatorErrorMessage(propertyName, this.label, this.control.errors[propertyName]);
      }
    }
    return null;
  }

  private getValidatorErrorMessage(validatorName: string, label: string, validatorValue?: any) {
    const config = {
      'required': `${label} là bắt buộc`,
      'minlength': `${label} phải có ít nhất ${validatorValue.requiredLength} ký tự`,
      'maxlength': `${label} không được vượt quá ${validatorValue.requiredLength} ký tự`,
      'max': `${label} phải nhỏ hơn 100%`,
      'pattern': this.getPatternMessage(label),
      'phoneNumber': `${label} phải là số điện thoại hợp lệ (bắt đầu bằng 0 hoặc +84 và có 10 chữ số)`,
      'duplicated': `${label} đã tồn tại, vui lòng nhập lại`,
      'phoneNumberDuplicated': `${label} đã được đăng ký`,
      'email': `${label} phải là email hợp lệ !`
    };
    return config[validatorName] || 'Giá trị không hợp lệ';
  }

  private getPatternMessage(label: string): string {
    if (label === 'Số điện thoại') {
      return `${label} phải là số điện thoại hợp lệ (bắt đầu bằng 0 hoặc +84 và có 10 chữ số)`;
    } else if (label === 'Mật khẩu') {
      return `${label} phải bao gồm chữ số (0-9), ký tự đặc biệt (@,!,#,...), ít nhất 1 chữ cái in hoa, và có từ 6 đến 30 ký tự`;
    }
    return 'Giá trị không hợp lệ';
  }
}
