export interface CartAddInfo {
  productId: string;
  quantity: number;
}
