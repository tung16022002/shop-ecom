export interface Cart {
  id: string;
  userId: string;
  items: CartItem[];
}

export interface CartItem {
  productId: string;
  totalPrice: number;
  quantity: number;
  selected: boolean;
}
