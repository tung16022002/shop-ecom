export interface CartUpdateInfo {
  productId: string;
  quantity: number;
  totalPrice: number;
}
