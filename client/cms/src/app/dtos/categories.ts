export interface Categories {
  id?: string;
  name: string;
  parent?: string;
  path?: string;
  code?: string;
}
