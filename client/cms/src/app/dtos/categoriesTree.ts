export interface CategoriesTree {
  text: string;
  value: string;
  children: CategoriesTree[];
}
