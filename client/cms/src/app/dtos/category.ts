export interface CategoryDTO {
  id?: String;
  name?: string;
  // subcategories?: SubCategoryDTO[];
}

// export interface SubCategoryDTO {
//   id?: string;
//   name?: string;
//   categoryId?: string;
// }
