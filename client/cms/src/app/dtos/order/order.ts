import {OrderDetailDTO} from "./orderDetail";

export interface OrderDTO {
  id?: string;
  userId?: string;
  fullName?: string;
  email?: string;
  phoneNumber?: string;
  address?: string;
  note?: string;
  feeShip?: number,
  paymentMethod?: string;
  status?: string;
  orderDate?: Date;
  totalMoney?: number;
  orderDetails?: OrderDetailDTO[];
  statusHistory?: OrderHistoryStatus[];
  active?: boolean;
}

export interface OrderHistoryStatus {
  status?: string;
  time?: Date;
}
