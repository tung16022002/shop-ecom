export interface OrderDetailDTO {
  productId: string;
  quantity: number;
  totalPrice: number;
}
