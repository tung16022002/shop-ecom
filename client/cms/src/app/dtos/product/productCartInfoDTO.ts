export interface ProductCartInfoDTO {
  selected: boolean;
  id?: string;
  name?: string;
  price?: number;
  thumbnail?: string;
  quantity?: number;
  totalPrice?: number;
  totalProduct?: number;
}
