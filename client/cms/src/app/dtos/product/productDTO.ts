export interface ProductDTO {
  id?: string;
  name?: string;
  price?: number;
  thumbnail?: string;
  description?: string;
  favourite?: boolean;
  categoryId?: string;
  subCategoryId?: string;
  quantity?: number; // Adding quantity field if needed
  listThumbnail?: string[];
}

