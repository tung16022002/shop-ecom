export interface ProductImageDTO {
  id?: string;
  productId?: string;
  imageUrl?: string;
}
