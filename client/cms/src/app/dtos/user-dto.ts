export interface UserDTO {
  id?: string;
  fullName?: string;
  password?: string;
  phoneNumber?: string;
  retypePassword?: string;
  facebookAccountId?: 0;
  googleAccountId?: 0;
  roleName?: "user";
  avt?: string;
}
