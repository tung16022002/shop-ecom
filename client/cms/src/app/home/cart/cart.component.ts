import {Component, OnDestroy, OnInit} from '@angular/core';
import {CartService} from '../../service/cart.service';
import {StartupService} from '../../service/startup.service';
import {ProductService} from '../../service/product.service';
import {ProductCartInfoDTO} from '../../dtos/product/productCartInfoDTO';
import {CartUpdateInfo} from "../../dtos/cart/cartUpdate";
import {MessageService} from "primeng/api";
import {OrderService} from "../../service/order.service";
import {OrderDetailDTO} from "../../dtos/order/orderDetail";
import {OrderDTO} from "../../dtos/order/order";
import {PaymentService} from "../../service/payment.service";
import {ActivatedRoute} from "@angular/router";
import {CouponService} from "../../service/coupon.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  // providers: [MessageService]
})
export class CartComponent implements OnInit, OnDestroy {
  userId: string;
  isLogin: boolean;
  listProduct: ProductCartInfoDTO[];
  totalOrder: number = 0;
  selectAll: boolean = false;
  selectedProductCount: number = 0;
  address: string;
  loading: boolean = false;
  feeShip: number;
  paymentMethod: any;

  paymentSelect: string;
  code:any;
  feeVoucher = 0;

  constructor(
    private cartService: CartService,
    private startupService: StartupService,
    private productService: ProductService,
    private messageService: MessageService,
    private orderService: OrderService,
    private paymentService: PaymentService,
    private route: ActivatedRoute,
    private couponService: CouponService,
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const responseCode = params['vnp_ResponseCode'];
      if (responseCode) { // Kiểm tra nếu có `vnp_ResponseCode` trong URL
        switch (responseCode) {
          case '00':
            this.messageService.add({severity: 'success', summary: 'Thanh toán thành công', detail:'Đơn hàng đã được tạo',  life: 3000});
            break;
          case '01':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Giao dịch chưa hoàn tất', life: 3000});
            break;
          case '02':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Giao dịch chưa hoàn tất', life: 3000});
            break;
          case '04':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Giao dịch chưa hoàn tất', life: 3000});
            break;
          case '05':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Giao dịch chưa hoàn tất', life: 3000});
            break;
          case '06':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'VNPAY đã gửi yêu cầu hoàn tiền sang Ngân hàng (GD hoàn tiền)', life: 3000});
            break;
          case '07':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường).', life: 3000});
            break;
          case '09':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.', life: 3000});
            break;
          case '10':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần', life: 3000});
            break;
          case '11':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.', life: 3000});
            break;
          case '12':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công', detail: 'Thẻ/Tài khoản của khách hàng bị khóa.', life: 3000});
            break;
          case '24':
            this.messageService.add({severity: 'error', summary: 'Giao dịch không thành công do',detail:' Khách hàng hủy giao dịch', life: 3000});
            break;
          default:
            this.messageService.add({severity: 'error', summary: 'Có lỗi xảy ra trong quá trình thanh toán', life: 3000});
        }
      }
    });

    this.paymentMethod = [
      {name: 'Thanh toán khi nhận hàng', code: 'COD'},
      {name: 'Ngân hàng ', code: 'BANK'}
    ]
    this.paymentSelect = "COD"
    this.isLogin = this.startupService.loggedIn();
    if (this.isLogin) {
      this.userId = this.startupService.getCurrentUser().id;
      this.loadCart();
    }
    this.address = this.startupService.getCurrentUser().address
  }

  private loadCart(): void {
    this.selectAll = false;
    this.cartService.getCart(this.userId).subscribe(cart => {
      const productIds = this.extractProductIds(cart);
      this.loadProductsByIds(productIds, cart);
    });
  }

  findVoucher(code: string) {
    if (this.totalOrder === 0) {
      this.messageService.add({severity: 'warn', summary: 'Bạn cần chọn mặt hàng thanh toán trước'});
      return;
    }

    code = this.code;
    this.couponService.getCouponByCode(code).subscribe(
      data => {
        if (data.status === "ACTIVE" && data.quantity >= 1) {
          console.log(data);
          const percentPrice = (this.totalOrder * data.discountPercent) / 100;
          const discountMax = data.maxDiscountAmount;

          this.feeVoucher = percentPrice > discountMax ? discountMax : percentPrice;
          this.messageService.add({severity: 'success', summary: 'Mã giảm giá hợp lệ'});
        } else {
          this.messageService.add({severity: 'warn', summary: 'Mã giảm giá đã hết hoặc chưa thể sử dụng'});
        }
      },
      error => {
        if (error.status === 404) {
          this.messageService.add({severity: 'error', summary: 'Không tìm thấy mã giảm giá'});
        } else {
          this.messageService.add({severity: 'error', summary: 'Không tìm thấy mã giảm giá'});
        }
      }
    );
  }


  private extractProductIds(cart: any): string[] {
    return cart.items.map(item => item.productId);
  }

  private loadProductsByIds(productIds: string[], cart: any): void {
    this.productService.getProductsByIds(productIds).subscribe(products => {
      this.listProduct = this.mapCartItemsToProducts(products, cart);
      this.calculateTotalOrder(); // Calculate total order after loading products

      if (this.listProduct.some(product => product.selected)) {
        this.feeShip = 20000;
      } else {
        this.feeShip = 0; // Nếu không có sản phẩm nào được chọn, đặt phí ship về 0
      }
    });
  }

  private mapCartItemsToProducts(products: ProductCartInfoDTO[], cart: any): ProductCartInfoDTO[] {
    return products.map(product => {
      const cartItem = cart.items.find(item => item.productId === product.id);
      if (cartItem) {
        return {
          ...product,
          quantity: cartItem.quantity,
          totalPrice: cartItem.totalPrice,
          selected: cartItem.selected // Initialize selected as false
        };
      }
      return product;
    });
  }

  incrementQuantity(product: ProductCartInfoDTO) {
    product.quantity++;
    this.updateTotalPrice(product);
  }

  onQuantityChange(product: ProductCartInfoDTO, event: any) {
    const newQuantity = +event.target.value;
    if (newQuantity > 0) {
      product.quantity = newQuantity;
      this.updateTotalPrice(product);
    }
  }

  preventNegative(event: KeyboardEvent) {
    if (event.key === '-' || event.key === '0' && (event.target as HTMLInputElement).value === '') {
      event.preventDefault();
    }
  }

  decrementQuantity(product: ProductCartInfoDTO) {
    if (product.quantity > 1) {
      product.quantity--;
      this.updateTotalPrice(product);
    }
  }

  updateTotalPrice(product: ProductCartInfoDTO) {
    product.totalPrice = product.price * product.quantity;

    const updateInfo: CartUpdateInfo = {
      productId: product.id,
      quantity: product.quantity,
      totalPrice: product.totalPrice
    };

    this.cartService.updateCart(this.userId, updateInfo).subscribe(response => {
      this.calculateTotalOrder(); // Recalculate total order after update
    });
  }

  deleteProduct(product: any) {
    const userID = this.startupService.getCurrentUser().id;
    const item = {
      productId: product.id,
      quantity: product.quantity
    };

    this.cartService.removeProductFromCart(userID, item).subscribe(data => {
      console.log(data);
      this.loadCart();
      this.messageService.add({severity: 'info', summary: 'Xóa thành công', life: 1500});
    });
  }

  onProductSelectChange() {
    this.feeShip = 20000;
    this.calculateTotalOrder();
  }

  onSelectAllChange() {
    this.feeShip = 20000;
    this.listProduct.forEach(product => {
      product.selected = this.selectAll
    });
    this.calculateTotalOrder();

  }

  calculateTotalOrder() {
    const selectedProducts = this.listProduct.filter(product => product.selected);
    this.selectedProductCount = selectedProducts.length; // Set the count of selected products
    if (this.selectedProductCount === 0) {
      this.feeShip = 0;
    }
    this.totalOrder = selectedProducts.reduce((sum, product) => sum + product.totalPrice, 0);
  }

  confirmOrder(): void {
    if (!this.listProduct.some(product => product.selected)) {
      this.messageService.add({severity: 'warn', summary: 'Chọn ít nhất một sản phẩm', life: 1500});
      return;
    }

    const orderDetails: OrderDetailDTO[] = this.listProduct
      .filter(product => product.selected)
      .map(product => ({
        productId: product.id,
        quantity: product.quantity,
        totalPrice: product.totalPrice
      }));
    orderDetails.forEach(data => {
      this.productService.getProductById(data.productId).subscribe(product => {
        if(data.quantity <= product.totalProduct) {
          const order: OrderDTO = {
            userId: this.userId,
            fullName: this.startupService.getCurrentUser().fullName,
            email: this.startupService.getCurrentUser().email,
            phoneNumber: this.startupService.getCurrentUser().phoneNumber,
            address: this.address,
            feeShip: this.feeShip,
            paymentMethod: this.paymentSelect,
            orderDetails: [data],
            totalMoney: this.totalOrder + this.feeShip - this.feeVoucher
          };

          if (order.paymentMethod === "BANK") {
            const data = {
              amount: order.totalMoney,
              orderInfo: `Đơn hàng của ${order.fullName}`,
              returnUrl: "http://localhost:4200/cart"
            };
            this.paymentService.createVnPayPayment(data).subscribe(data => {
              if (data) {
                window.location.href = data.paymentUrl;
                this.loading = true;
                this.messageService.add({severity: 'info', summary: 'Đang chuyển hướng đến trang thanh toán', life: 3000});
              }
            });
            this.createOrder(order);
          } else if (order.paymentMethod === "COD") {
            this.createOrder(order);
          }
        }
        else {
          this.messageService.add({severity: 'error', summary: 'Thất bại', detail: 'Số lượng sản phẩm trong kho không đủ'})
          return;
        }
      })
    })

  }

  private createOrder(order: OrderDTO): void {

    this.couponService.applyCoupon(this.code).subscribe()

    this.orderService.createOrder(order).subscribe(
      response => {
        this.loading = false;
        this.feeVoucher = 0;
        this.code = "";
        if(order.paymentMethod == "COD") {
          this.messageService.add({severity: 'success', summary: 'Đơn hàng đã được tạo', life: 1500});
        }
        this.removeSelectedProducts(order.orderDetails);
      },
      error => {
        this.messageService.add({severity: 'error', summary: 'Tạo đơn hàng thất bại', life: 1500});
        console.error('Error creating order:', error);
      }
    );
  }

  removeSelectedProducts(orderDetails: OrderDetailDTO[]): void {
    const removeProductRecursively = (index: number) => {
      if (index >= orderDetails.length) {
        // Khi tất cả sản phẩm đã được xóa, tải lại giỏ hàng
        this.loadCart();
        return;
      }

      const item = {
        productId: orderDetails[index].productId,
        quantity: orderDetails[index].quantity
      };

      this.cartService.removeProductFromCart(this.userId, item).subscribe({
        next: () => {
          console.log(`Product removed: ${orderDetails[index].productId}`);
          // Xóa sản phẩm tiếp theo
          removeProductRecursively(index + 1);
        },
        error: (err) => {
          console.error(`Error removing product: ${orderDetails[index].productId}`, err);
          // Tiếp tục với sản phẩm tiếp theo ngay cả khi gặp lỗi
          removeProductRecursively(index + 1);
        }
      });
    };

    // Bắt đầu xóa từ sản phẩm đầu tiên
    removeProductRecursively(0);
  }

  removeAllSelectedProducts(): void {
    const selectedProducts = this.listProduct.filter(product => product.selected);

    if (selectedProducts.length === 0) {
      this.messageService.add({severity: 'warn', summary: 'Chọn ít nhất một sản phẩm để xóa', life: 1500});
      return;
    }

    const removeProductRecursively = (index: number) => {
      if (index >= selectedProducts.length) {
        // Khi tất cả sản phẩm đã được xóa, tải lại giỏ hàng
        this.loadCart();
        this.messageService.add({severity: 'success', summary: 'Đã xóa tất cả sản phẩm đã chọn', life: 1500});
        return;
      }

      const item = {
        productId: selectedProducts[index].id,
        quantity: selectedProducts[index].quantity
      };

      this.cartService.removeProductFromCart(this.userId, item).subscribe({
        next: () => {
          console.log(`Product removed: ${selectedProducts[index].id}`);
          // Xóa sản phẩm tiếp theo
          removeProductRecursively(index + 1);
        },
        error: (err) => {
          console.error(`Error removing product: ${selectedProducts[index].id}`, err);
          // Tiếp tục với sản phẩm tiếp theo ngay cả khi gặp lỗi
          removeProductRecursively(index + 1);
        }
      });
    };

    // Bắt đầu xóa từ sản phẩm đầu tiên
    removeProductRecursively(0);
  }


  ngOnDestroy(): void {
    this.cartService.unselectProducts(this.userId).subscribe();
  }
}
