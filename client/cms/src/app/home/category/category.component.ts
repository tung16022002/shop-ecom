import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent {
  @Input() categories: any;

  @Output() subcategoryClick = new EventEmitter<string>();

  onSubCategoryClick(subCategoryId: string) {
    this.subcategoryClick.emit(subCategoryId);
  }


}
