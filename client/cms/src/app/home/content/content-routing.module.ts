import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MainContentComponent} from "../main-content/main-content.component";
import {SearchProductComponent} from "../search-product/search-product.component";
import {ListVoucherComponent} from "../list-voucher/list-voucher.component";

const routes: Routes = [
  {
    path: '',
    component: MainContentComponent
  },
  {
    path: 'searchProduct',
    component: SearchProductComponent,
    data: {
      title: 'Tìm kiếm sản phẩm'
    }
  },
  {
    path: 'voucher',
    component: ListVoucherComponent,
    data: {
      title: 'Kho voucher'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
