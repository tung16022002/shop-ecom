import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../service/product.service";
import {ActivatedRoute, Router} from "@angular/router";
import {StartupService} from "../../service/startup.service";
import {CartService} from "../../service/cart.service";
import {MessageService} from "primeng/api";
import {CommentService} from "../../service/comment.service";
import {delay} from "rxjs";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [MessageService]
})
export class DetailComponent implements OnInit {
  product$: any;
  quantity = 1;
  address: any;
  loading: boolean = false
  listAttribute = []
  listComment: any = [];
  averageRating: number;
  countRating: number;

  constructor(
    private productService: ProductService,
    private router: ActivatedRoute,
    private startupService: StartupService,
    private cartService: CartService,
    private messageService: MessageService,
    private _router: Router,
    private commentService: CommentService
  ) {
  }

  responsiveOptions: any[] = [
    {
      breakpoint: '1024px',
      numVisible: 5
    },
    {
      breakpoint: '768px',
      numVisible: 3
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];

  ngOnInit() {
    this.router.paramMap.subscribe(params => {
      const id = params.get('productId');
      this.productService.getProductById(id).pipe(delay(1500)).subscribe(data => {

        this.product$ = data;
        console.log(data)
        this.listAttribute = data.attributes[0].value.map(size => ({name: size, active: false}))
        console.log(this.listAttribute)

      })
      this.commentService.getCommentsByProductId(id).subscribe(data => {
        this.listComment = data;
        this.calculatorRating();
      })

    })

    // this.address = this.startupService.getCurrentUser().address;
  }

  calculatorRating() {
    const totalRating = this.listComment.reduce((sum, comment) => sum + comment.rating, 0)
    this.countRating = this.listComment.length;

    this.averageRating = totalRating / this.countRating
  }

  select(value) {
    this.listAttribute.forEach(att => att.active = false);
    value.active = true;

  }

  up() {
    this.quantity = this.quantity + 1;
  }

  down() {
    if (this.quantity > 1)
      this.quantity--;
  }

  onQuantityChange(event: any) {
    const newQuantity = +event.target.value;

    // Kiểm tra nếu giá trị là 0 hoặc không hợp lệ, đặt lại là 1
    if (newQuantity <= 0 || isNaN(newQuantity)) {
      this.quantity = 1;
    } else {
      this.quantity = newQuantity;
    }

    // Nếu người dùng cố ý nhập "0" ở đầu, loại bỏ nó khỏi input
    event.target.value = this.quantity.toString();
  }

  preventNegative(event: KeyboardEvent) {
    const inputValue = (event.target as HTMLInputElement).value;

    // Chặn nhập dấu trừ và không cho nhập 0 ở đầu khi input trống
    if (event.key === '-' || (event.key === '0' && inputValue === '')) {
      event.preventDefault();
    }
  }



  addToCart() {
    if (this.startupService.loggedIn()) {
      const userId = this.startupService.getCurrentUser().id;
      const cartItem = {
        productId: this.product$.id,
        quantity: this.quantity,
        selected: false
      };
      this.loading = true;

      if (this.product$.totalProduct == 0) {
        this.messageService.add({severity: 'error', summary: 'Thất bại', detail: 'Hết hàng'});
        this.loading = false;
        return;
      }
      if (cartItem.quantity > this.product$.totalProduct) {
        this.messageService.add({severity: 'error', summary: 'Thất bại', detail: 'Số lượng mua vượt quá số lượng trong kho'})
        this.loading = false;
        return;
      }

      else {
        this.cartService.addProductToCart(userId, cartItem).subscribe(response => {
            setTimeout(() => {
              this.loading = false
              this.messageService.add({
                severity: 'success',
                key: 'success',
                summary: 'Thành công ',
                detail: 'Đã thêm vào giỏ hàng',
                life: 1500
              });
            }, 1500);
          }
          , error => {
            console.error('Error adding product to cart:', error);
          });
      }

    } else {
      this.messageService.add({
        severity: 'error',
        key: 'danger',
        summary: 'Lỗi !',
        detail: 'Bạn cần phải đăng nhập để thêm vào giỏ hàng ',
        life: 2000
      })
    }
  }

  buyProduct() {
    if (this.startupService.loggedIn()) {
      const userId = this.startupService.getCurrentUser().id;
      const cartItem = {
        productId: this.product$.id,
        quantity: this.quantity,
        selected: true
      };
      if (this.product$.totalProduct == 0) {
        this.messageService.add({severity: 'error', summary: 'Thất bại', detail: 'Hết hàng'});
        this.loading = false;
        return;
      }
      if (cartItem.quantity > this.product$.totalProduct) {
        this.messageService.add({severity: 'error', summary: 'Thất bại', detail: 'Số lượng sản phẩm trong kho không đủ '})
        return;
      } else {
        this.cartService.addProductToCart(userId, cartItem).subscribe(response => {
          this._router.navigate(["/cart"]);
        }, error => {
          console.error('Error adding product to cart:', error);
        });
      }

    } else {
      console.error('User is not logged in');
      this.messageService.add({
        severity: 'error',
        key: 'danger',
        summary: 'Lỗi !',
        detail: 'Bạn cần phải đăng nhập để mua ngay ',
        life: 2000
      })
    }
  }


}
