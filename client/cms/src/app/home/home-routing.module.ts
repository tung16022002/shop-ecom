import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {MainContentComponent} from './main-content/main-content.component';
import {SearchComponent} from './search/search.component';
import {DetailComponent} from './detail/detail.component';
import {CartComponent} from './cart/cart.component';
import {ListProuductFavouriteComponent} from './list-prouduct-favourite/list-prouduct-favourite.component';
import {SearchProductComponent} from "./search-product/search-product.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {title: "Trang chủ"},
    children: [
      {
        path: '',
        redirectTo: 'shopping',
        pathMatch: 'full'
      },
      {
        path: 'shopping',
        loadChildren: () => import('./content/content.module').then(m => m.ContentModule),
        data: {title: 'Mua sắm'}
      },
      {
        path: 'profile',
        loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
        data: {title: 'Thông tin tài khoản'}
      },
      {
        path: 'category/:id',
        component: SearchComponent,
        data: {isCategoryPage: true}
      },
      {
        path: 'cart',
        component: CartComponent,
        data: {title: 'Giỏ hàng'}
      },
      {
        path: 'favourite',
        component: ListProuductFavouriteComponent,
        data: {title: 'Sản phẩm yêu thích'}
      },
      {
        path: 'product/:productId',
        component: DetailComponent,
        data: {title: 'Chi tiết sản phẩm '}
      },
      {
        path: 'order',
        loadChildren: () => import('./order/order.module').then(m => m.OrderModule),
        data: {title: 'Đơn hàng'},
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
