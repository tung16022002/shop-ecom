import {Component, OnInit} from '@angular/core';
import {StartupService} from "../service/startup.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{
    isLogin: boolean;

    constructor(
      private startupService: StartupService,
    ) {
    }

    ngOnInit() {
      this.isLogin = this.startupService.loggedIn();
    }
}
