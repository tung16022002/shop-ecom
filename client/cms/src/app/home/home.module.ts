import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule, NgForOf, NgIf, NgOptimizedImage} from '@angular/common';
import {HomeRoutingModule} from './home-routing.module';
import {BreadcrumbModule} from 'xng-breadcrumb';
import {MainContentComponent} from './main-content/main-content.component';
import {ProductComponent} from './product/product.component';
import {CategoryComponent} from './category/category.component';
import {HomeComponent} from "./home.component";
import {RatingModule} from "primeng/rating";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TopBarComponent} from "../layout/top-bar/top-bar.component";
import {HeaderComponent} from "../layout/header/header.component";
import {FooterComponent} from "../layout/footer/footer.component";
import {ToastModule} from "primeng/toast";
import {SearchComponent} from './search/search.component';
import {BreadcrumbComponent} from "../layout/breadcrumb/breadcrumb.component";
import {PaginatorModule} from "primeng/paginator";
import {DetailComponent} from './detail/detail.component';
import {GalleriaModule} from "primeng/galleria";
import {CartComponent} from './cart/cart.component';
import {DividerModule} from "primeng/divider";
import {BadgeModule} from "primeng/badge";
import {ButtonModule} from "primeng/button";
import {TableModule} from "primeng/table";
import {TagModule} from "primeng/tag";
import {ListProuductFavouriteComponent} from './list-prouduct-favourite/list-prouduct-favourite.component';
import {ControlMessageComponent} from "../shared/control-message/control-message.component";
import {ImageModule} from "primeng/image";
import {TabViewModule} from "primeng/tabview";
import {InputTextareaModule} from "primeng/inputtextarea";
import {CardModule} from "primeng/card";
import { SearchProductComponent } from './search-product/search-product.component';
import {InputTextModule} from "primeng/inputtext";
import { ListVoucherComponent } from './list-voucher/list-voucher.component';
import {
  NgbDropdown,
  NgbDropdownItem,
  NgbDropdownMenu,
  NgbDropdownToggle,
  NgbInputDatepicker
} from "@ng-bootstrap/ng-bootstrap";
import {NavbarMobileBottomComponent} from "../layout/navbar-mobile-bottom/navbar-mobile-bottom.component";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    BreadcrumbModule,
    RatingModule,
    FormsModule,
    ToastModule,
    PaginatorModule,
    NgOptimizedImage,
    GalleriaModule,
    DividerModule,
    BadgeModule,
    ButtonModule,
    TableModule,
    TagModule,
    ControlMessageComponent,
    ImageModule,
    TabViewModule,
    InputTextareaModule,
    ReactiveFormsModule,
    InputTextareaModule,
    ReactiveFormsModule,
    CardModule,
    InputTextModule,
    NgForOf,
    NgIf,
    NgbInputDatepicker,
    NgbDropdown,
    NgbDropdownToggle,
    NgbDropdownMenu,
    NgbDropdownItem,
  ],
  declarations: [
    HomeComponent,
    MainContentComponent,
    ProductComponent,
    CategoryComponent,
    TopBarComponent,
    NavbarMobileBottomComponent,
    HeaderComponent,
    BreadcrumbComponent,
    FooterComponent,
    SearchComponent,
    DetailComponent,
    CartComponent,
    ListProuductFavouriteComponent,
    SearchProductComponent,
    ListVoucherComponent,
  ],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class HomeModule {
}
