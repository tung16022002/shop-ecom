import { Component, OnInit } from '@angular/core';
import { CouponService } from "../../service/coupon.service";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-list-voucher',
  templateUrl: './list-voucher.component.html',
  styleUrls: ['./list-voucher.component.scss']
})
export class ListVoucherComponent implements OnInit {
  listCoupon: any;
  timeIntervals: { [key: string]: string } = {};

  constructor(
    private couponsService: CouponService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.couponsService.getAllCoupons().subscribe(data => {
      this.listCoupon = data;
      this.startCountdown();
    });
  }

  startCountdown() {
    setInterval(() => {
      this.listCoupon.forEach((coupon: any) => {
        const currentTime = new Date().getTime();
        const endDate = new Date(coupon.endDate).getTime();

        const timeRemaining = endDate - currentTime;

        if (timeRemaining > 0) {
          this.timeIntervals[coupon.code] = this.formatTimeRemaining(timeRemaining);
        } else {
          this.timeIntervals[coupon.code] = 'Đã hết hạn';
        }
      });
    }, 1000);
  }

  formatTimeRemaining(milliseconds: number): string {
    const totalSeconds = Math.floor(milliseconds / 1000);
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds % 3600) / 60);
    const seconds = totalSeconds % 60;

    return `${hours}h ${minutes}m ${seconds}s`;
  }

  copyCode(code: string) {
    navigator.clipboard.writeText(code).then(() => {
      this.messageService.add({ severity: 'success', summary: 'Thành công', detail: 'Mã đã được sao chép' });
    });
  }
}
