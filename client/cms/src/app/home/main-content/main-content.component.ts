import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CategoryService} from "../../service/category.service";
import {ProductService} from "../../service/product.service";
import {ActivatedRoute, Router} from "@angular/router";
import {delay} from "rxjs";
import {UserService} from "../../service/user.service";
import {StartupService} from "../../service/startup.service";
import {AuthService} from "../../service/auth.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {

  active: boolean;
  pageInfo = {
    currentPage: 1,
    recordPerPage: 16,
    categoryId: "",
    typeSorted: "",
  }

  categoryName = "Gợi ý hôm nay"

  categories;
  first: number = 0;

  rows: number = 10;
  products;
  totalRecord;

  constructor(private categoryService: CategoryService,
              private productService: ProductService,
              private cdr: ChangeDetectorRef,
              private router: Router,
              private startupService: StartupService,
              private authService: AuthService,
              private messageService: MessageService

  ) {
  }

  ngOnInit(): void {
    if(localStorage.getItem('token'))
      this.checkActive();

    this.loadCategories();
    this.loadProduct();
  }

  checkActive() {
    this.active = this.startupService.getCurrentUser().active
    if (this.active == true) {
      return;
    } else {
      this.messageService.add({severity: 'error', summary: 'Bạn đã bị khóa tài khoản bởi admin', life: 3000})
      setTimeout(() => {
        this.authService.clearToken();
        this.router.navigate(['/login']);
      }, 3000)
    }


  }

  loadCategories(): void {
    this.categoryService.getAllCategoriesWithChildren().subscribe(data => {
      this.categories = data
    });
  }

  onPageChange(event: any) {
    this.pageInfo.currentPage = event.page + 1;
    this.pageInfo.recordPerPage = event.rows;
    this.loadProduct();
  }

  loadProduct(): void {
    this.products = null;
    this.productService.searchProducts(this.pageInfo).pipe(delay(1500)).subscribe(data => {
      this.products = data;
      console.log(data);
    });
    this.productService.countProducts(this.pageInfo).subscribe(data => this.totalRecord = data)
    this.cdr.detectChanges();
  }

}
