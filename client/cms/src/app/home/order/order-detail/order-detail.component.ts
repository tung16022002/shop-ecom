import {Component, OnInit} from '@angular/core';
import {DatePipe} from "@angular/common";
import {OrderService} from "../../../service/order.service";
import {ActivatedRoute} from "@angular/router";
import {StartupService} from "../../../service/startup.service";
import {MessageService} from "primeng/api";
import {CommentService} from "../../../service/comment.service";

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss'],
  providers: [DatePipe, MessageService]
})
export class OrderDetailComponent implements OnInit{
  events: any;
  order: any;
  statusHistory: any;
  name: any;
  address: any;
  phoneNumber: any;
  email: any;
  userId: any;
  visible: boolean = false;
  rating: number;
  uploadedFiles: any[] = [];
  content: any;

  constructor(
    private datePipe: DatePipe,
    private orderService: OrderService,
    private router: ActivatedRoute,
    private startupService: StartupService,
    private messageService: MessageService,
    private commentService: CommentService
  ) {}

  ngOnInit() {
    this.name = this.startupService.getCurrentUser().fullName;
    this.address = this.startupService.getCurrentUser().address;
    this.email = this.startupService.getCurrentUser().email;
    this.phoneNumber = this.startupService.getCurrentUser().phoneNumber;
    this.userId = this.startupService.getCurrentUser().id;



    this.router.paramMap.subscribe(params => {
      const id = params.get('id');
      this.orderService.getOrderById(id).subscribe(data => {
        this.order = data;
        this.loadEvent(data);

      })
    })
  }

  loadEvent(data: any) {
    const allStatuses = [
      { status: 'pending', displayName: 'Chờ xác nhận', icon: 'pi pi-th-large', color: '#9C27B0' },
      { status: 'prepare', displayName: 'Đang chuẩn bị hàng', icon: 'pi pi-cog', color: '#673AB7' },
      { status: 'delivery', displayName: 'Đang giao hàng', icon: 'pi pi-shopping-cart', color: '#FF9800' },
      { status: 'done', displayName: 'Đã nhận được hàng', icon: 'pi pi-check', color: 'green' },
    ];
    this.statusHistory = data.statusHistory
    this.events = allStatuses.map((statusInfo) => {
      const matchedHistory = this.statusHistory.find((history) => history.status === statusInfo.status);

      return {
        status: statusInfo.displayName,
        date: matchedHistory ? this.datePipe.transform(matchedHistory.time, 'dd/MM/yyyy HH:mm:ss') : 'Chưa hoàn thành',
        icon: statusInfo.icon,
        color: matchedHistory ? statusInfo.color : 'lightgray',  // Nếu chưa có trong history, màu sẽ là 'lightgray'
      };
    });
  }

  cancelOrder() {
      if(this.order.status === 'pending' || this.order.status === 'prepare') {
        const data = {
          ...this.order,
          status: 'cancel'
        }
        this.orderService.updateOrder(data).subscribe(data => {
          this.messageService.add({severity: 'warn', summary: 'Đã hủy đơn hàng ', life: 1500})
        })
      } else {
        this.messageService.add({severity: 'error', summary: 'Hủy đơn hàng thất bại ', detail: 'Bạn chỉ có thể hủy đơn hàng trước khi người bán giao hàng ', life: 2000 })
      }

  }

  updateOrder() {
    const data = {
      ...this.order,
      status: 'done'
    }
    this.orderService.updateOrder(data).subscribe(data => {
      this.orderService.getOrderById(data.id).subscribe(data => {
        this.order = data;
        this.loadEvent(data);
      });

      this.messageService.add({severity: 'info', summary: 'Đã nhận được hàng ', life: 1500});
    })
  }

  showComment() {
    this.visible = true;
  }

  onUpload(event: any) {
    const files = event.files;
    this.commentService.uploadProductImages(files).subscribe(data => {
      this.uploadedFiles = data.map(data => data.imageUrl);
      this.messageService.add({severity: 'info', summary: 'Tải ảnh thành công', detail: ''});
    })
  }

  addComment() {
    if (!this.rating || !this.content || this.content.trim() === '') {
      this.messageService.add({
        severity: 'error',
        summary: 'Lỗi',
        detail: 'Vui lòng điền đầy đủ thông tin đánh giá và xếp hạng sản phẩm.'
      });
      return; // Ngăn không thực hiện tiếp nếu điều kiện không thỏa
    }
    this.order.orderDetails.forEach(product => {
      console.log(product.productId);
      const data = {
        productId: product.productId,
        userId: this.userId,
        content: this.content,
        rating: this.rating,
        listThumbnail: this.uploadedFiles
      }
      this.commentService.createComment(data).subscribe(data => {
        this.messageService.add({severity: 'success', summary: 'Thêm bình luận thành công', detail: ''})
      })
      this.visible = false;

    })

  }




}

