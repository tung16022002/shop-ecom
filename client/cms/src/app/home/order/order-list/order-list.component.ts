import {Component, OnInit} from '@angular/core';
import {OrderService} from "../../../service/order.service";
import {StartupService} from "../../../service/startup.service";
import {ProductService} from "../../../service/product.service";
import {forkJoin} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss'],
  providers: [MessageService]
})
export class OrderListComponent implements OnInit {
  userId: string;
  orders;

  pageInfo = {
    currentPage: 1,
    recordPerPage: 20,
    status: '',
  }
  totalRecord: number;
  constructor(
    private orderService: OrderService,
    private startupService: StartupService,
    private productService: ProductService,

  ) {
  }

  ngOnInit(): void {
    this.userId = this.startupService.getCurrentUser().id;
    this.loadOrders();
    // Fetch orders for the current user

  }
  loadOrders() {
    this.orderService.getOrdersByUserId(this.userId).pipe(
      switchMap((orders: any[]) => {
        // Process each order to fetch related product details
        const orderRequests = orders.map(order => {
          const productIds = order.orderDetails.map((detail: any) => detail.productId);
          return this.productService.getProductsByIds(productIds).pipe(
            map(products => {
              // Add the product details to the order details
              order.orderDetails = order.orderDetails.map((detail: any) => {
                const product = products.find(p => p.id === detail.productId);
                return {
                  ...detail,
                  name: product?.name,
                  image: product?.thumbnail,
                };
              });
              return order;
            })
          );
        });
        return forkJoin(orderRequests);
      })
    ).subscribe((ordersWithProductDetails: any[]) => {
      this.orders = ordersWithProductDetails;
      console.log(this.orders, 'Orders with product details');
    });
  }
  getStatusName(status: string): string {
    switch (status) {
      case 'pending':
        return 'Chờ xác nhận';
      case 'prepare':
        return 'Chuẩn bị hàng';
      case 'delivery':
        return 'Đang giao hàng';
      case 'done':
        return 'Đã nhận được hàng';
      case 'cancel':
        return 'Đã hủy'
      default:
        return 'Không xác định';
    }
  }


}
