import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OrderListComponent} from "./order-list/order-list.component";
import {OrderDetailComponent} from "./order-detail/order-detail.component";

const routes: Routes = [
  {
    path: '',
    component: OrderListComponent,
    data: {
      title: 'Danh sách đơn hàng',
    }
  },
  {
    path: ':id',
    component: OrderDetailComponent,
    data: {
      title: 'Chi tiết đơn hàng '
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule {
}
