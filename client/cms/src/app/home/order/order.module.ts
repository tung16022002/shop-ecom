import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrderListComponent} from "./order-list/order-list.component";
import {OrderComponent} from "./order.component";
import {OrderDetailComponent} from "./order-detail/order-detail.component";
import {OrderRoutingModule} from "./order-routing.module";
import {TagModule} from "primeng/tag";
import {ButtonModule} from "primeng/button";
import {PanelModule} from "primeng/panel";
import {TimelineModule} from "primeng/timeline";
import {ToastModule} from "primeng/toast";
import {DialogModule} from "primeng/dialog";
import {RatingModule} from "primeng/rating";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InputTextareaModule} from "primeng/inputtextarea";
import {FileUploadModule} from "primeng/fileupload";
import {SharedModule} from "primeng/api";


@NgModule({
  declarations: [
    OrderListComponent,
    OrderComponent,
    OrderDetailComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    TagModule,
    ButtonModule,
    PanelModule,
    TimelineModule,
    ToastModule,
    DialogModule,
    RatingModule,
    FormsModule,
    InputTextareaModule,
    ReactiveFormsModule,
    FileUploadModule,
    SharedModule,
    InputTextareaModule,
    ReactiveFormsModule,
    FileUploadModule
  ]
})
export class OrderModule {
}
