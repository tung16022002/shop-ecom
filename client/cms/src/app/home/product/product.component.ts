import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductService} from "../../service/product.service";
import {StartupService} from "../../service/startup.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  value: number = 4.5;
  @Input() products: any;
  @Input() categoryName: string;
  @Input() filter: boolean;
  @Output() sortChange = new EventEmitter<string>();

  loading: boolean = false;



  constructor(
    private productService: ProductService,
    private startupService: StartupService,
  ) {
  }

  ngOnInit(): void {

  }

  toggleHeart(product) {
    product.favourite = !product.favourite;
    console.log(product.favourite);
    this.productService.updateProduct(product).subscribe(data => console.log(data));
  }

  sortAsc() {
    this.sortChange.emit('asc');
  }
  sortDesc() {
    this.sortChange.emit('desc');
  }

}
