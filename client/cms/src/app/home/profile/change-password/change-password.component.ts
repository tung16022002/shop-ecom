import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../service/user.service";
import {StartupService} from "../../../service/startup.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  providers: [MessageService]
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private startupService: StartupService,
    private messageService: MessageService
  ) {
  }

  ngOnInit(): void {
    this.initData()

  }

  initData() {
    this.form = this.fb.group({
        currentPassword: ['', Validators.required],
        newPassword: ['', Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(30),
          Validators.pattern(/^(?=.*[A-Z])(?=.*\d)(?=.*[@!#$%^&*.])[A-Za-z\d@!#$%^&*.]{6,30}$/),
        ])],
        confirmPassword: ['', Validators.required],

      },
      {
        validators: this.validateControlsValue('newPassword', 'confirmPassword')
      }
    )
  }

  validateControlsValue(firstControlName: string, secondControlName: string) {
    return function (formGroup: FormGroup) {
      const {value: firstControlValue} = formGroup.get(firstControlName);
      const {value: secondControlValue} = formGroup.get(secondControlName);
      return firstControlValue === secondControlValue ? null : {valueNotMatch: true};
    };
  }

  changePassword() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    const phoneNumber = this.startupService.getCurrentUser().phoneNumber
    const data = this.form.value
    console.log(data);
    this.userService.changePassword(phoneNumber, data).subscribe({
      next: (response) => {
        this.messageService.add({
          severity: 'info', summary: 'Thành công', detail: 'Cập nhật mật khẩu thành công'
        });
      },
      error: (err) => {
        this.messageService.add({
          severity: 'error', summary: 'Thất bại', detail: 'Mật khẩu hiện tại không đúng'
        });
      }
    });
  }

}
