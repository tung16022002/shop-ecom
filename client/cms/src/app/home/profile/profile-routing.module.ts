import {RouterModule, Routes} from "@angular/router";
import {ChangePasswordComponent} from "./change-password/change-password.component";
import {NgModule} from "@angular/core";
import {ProfileComponent} from "./profile.component";

const profileRouter: Routes = [
  {
    path: '',
    component: ProfileComponent,
    data: {
      title: 'Cập nhật tài khoản'
    }
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    data: {
      title: 'Thay đổi mật khẩu'
    }
  },
]

@NgModule({
  imports: [RouterModule.forChild(profileRouter)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {
}
