"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var api_1 = require("primeng/api");
var ProfileComponent = exports.ProfileComponent = function () {
    var _classDecorators = [(0, core_1.Component)({
            selector: 'app-profile',
            templateUrl: './profile.component.html',
            styleUrls: ['./profile.component.scss'],
            providers: [api_1.MessageService]
        })];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var ProfileComponent = _classThis = /** @class */ (function () {
        function ProfileComponent_1(startupService, fb, userService, authService, router, messageService) {
            this.startupService = startupService;
            this.fb = fb;
            this.userService = userService;
            this.authService = authService;
            this.router = router;
            this.messageService = messageService;
            this.user = {
                phoneNumber: '',
                email: '',
                fullName: '',
                password: '',
                avt: '',
                address: '',
                dateOfBirth: '',
                sex: '',
                role: 'user'
            };
        }
        ProfileComponent_1.prototype.ngOnInit = function () {
            this.initData();
            this.initForm();
        };
        ProfileComponent_1.prototype.initData = function () {
            this.currentUser = this.startupService.getCurrentUser();
            this.user.phoneNumber = this.currentUser.phoneNumber;
            this.user.email = this.currentUser.email;
            this.user.fullName = this.currentUser.fullName;
            this.user.address = this.currentUser.address;
            this.user.avt = this.currentUser.avt;
            this.user.sex = this.currentUser.sex;
            if (this.currentUser.dateOfBirth) {
                this.user.dateOfBirth = new Date(this.currentUser.dateOfBirth);
            }
            this.user.password = this.currentUser.password;
        };
        ProfileComponent_1.prototype.initForm = function () {
            this.profileForm = this.fb.group({
                phoneNumber: [
                    this.user.phoneNumber,
                    forms_1.Validators.compose([
                        forms_1.Validators.required,
                    ]),
                ],
                email: [
                    this.user.email,
                    forms_1.Validators.compose([
                        forms_1.Validators.required,
                        forms_1.Validators.email,
                    ])
                ],
                fullName: [
                    this.user.fullName,
                    forms_1.Validators.compose([
                        forms_1.Validators.required,
                    ])
                ],
                address: [
                    this.user.address,
                    forms_1.Validators.compose([
                        forms_1.Validators.required,
                    ])
                ],
                sex: [
                    this.user.sex
                ],
                dateOfBirth: [
                    this.user.dateOfBirth
                ]
            });
        };
        ProfileComponent_1.prototype.updateData = function () {
            var _this = this;
            var data = this.profileForm.value;
            console.log(data);
            this.userService.update(data).subscribe(function (data) {
                console.log(data);
                _this.messageService.add({
                    key: 'tc',
                    severity: 'success',
                    summary: 'Đăng nhập thất bại',
                    detail: 'Số điện thoại hoặc mật khẩu không đúng !'
                });
                _this.authService.clearToken();
                _this.router.navigate(['/login']);
            });
        };
        ProfileComponent_1.prototype.onFileSelected = function (event) {
            var _this = this;
            var file = event.target.files[0];
            if (file && file.size <= 5 * 1024 * 1024) {
                var userId = this.startupService.getCurrentUser().id;
                this.userService.uploadAvatar(userId, file).subscribe({
                    next: function (response) {
                        _this.user.avt = response.avt;
                        _this.currentUser.avt = response.avt;
                        _this.messageService.add({
                            severity: 'info', summary: 'Thành công', detail: 'Ảnh đại diện đã được cập nhật'
                        });
                    },
                    error: function (err) {
                        _this.messageService.add({
                            severity: 'error', summary: 'Thất bại', detail: 'Không thể tải lên ảnh đại diện'
                        });
                    }
                });
            }
            else {
                this.messageService.add({
                    severity: 'error', summary: 'Thất bại', detail: 'Vui lòng chọn ảnh dưới 5MB'
                });
            }
        };
        return ProfileComponent_1;
    }());
    __setFunctionName(_classThis, "ProfileComponent");
    (function () {
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        ProfileComponent = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return ProfileComponent = _classThis;
}();
