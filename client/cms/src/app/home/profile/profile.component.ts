import {Component, OnInit} from '@angular/core';
import {StartupService} from "../../service/startup.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../service/user.service";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {MessageService} from "primeng/api";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [MessageService]
})
export class ProfileComponent implements OnInit {
  user = {
    phoneNumber: '',
    email: '',
    fullName: '',
    password: '',
    avt: '',
    address: '',
    dateOfBirth: '' as string | Date,
    sex: '',
    role: 'user',
    active: true,
  };
  currentUser: any;
  profileForm: FormGroup;

  constructor(
    private startupService: StartupService,
    private fb: FormBuilder,
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private messageService: MessageService
  ) {
  }

  ngOnInit(): void {
    this.initData();
    this.initForm();
  }

  initData() {
    this.currentUser = this.startupService.getCurrentUser();

    this.user.phoneNumber = this.currentUser.phoneNumber;
    this.user.email = this.currentUser.email;
    this.user.fullName = this.currentUser.fullName;
    this.user.address = this.currentUser.address;
    this.user.avt = this.currentUser.avt;
    this.user.sex = this.currentUser.sex;

    if (this.currentUser.dateOfBirth) {
      this.user.dateOfBirth = new Date(this.currentUser.dateOfBirth);
    }

    this.user.password = this.currentUser.password;

  }

  initForm() {
    this.profileForm = this.fb.group({
      phoneNumber: [
        this.user.phoneNumber,
        Validators.compose([
          Validators.required,
        ]),
      ],
      email: [
        this.user.email,
        Validators.compose([
          Validators.required,
          Validators.email,
        ])
      ],
      fullName: [
        this.user.fullName,
        Validators.compose([
          Validators.required,
        ])
      ],
      address: [
        this.user.address,
        Validators.compose([
          Validators.required,
        ])
      ],
      sex: [
        this.user.sex
      ],
      dateOfBirth: [
        this.user.dateOfBirth
      ],
      active: [true]

    })
  }

  updateData() {
    const data = this.profileForm.value;
    console.log(data);
    this.userService.update(data).subscribe(data => {
      console.log(data);
      this.messageService.add({
        severity: 'success',
        summary: 'Cập nhật thành công',
        detail: 'Vui lòng đăng nhập lại!'
      });
      setTimeout(() => {
        this.authService.clearToken();
        this.router.navigate(['/login'])
      }, 3000)

    })
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file && file.size <= 5 * 1024 * 1024) {
      const userId = this.startupService.getCurrentUser().id;
      this.userService.uploadAvatar(userId, file).subscribe({
        next: (response) => {
          this.user.avt = response.avt;
          this.currentUser.avt = response.avt;
          this.messageService.add({
            severity: 'info', summary: 'Thành công', detail: 'Ảnh đại diện đã được cập nhật'
          });
        },
        error: (err) => {
          this.messageService.add({
            severity: 'error', summary: 'Thất bại', detail: 'Không thể tải lên ảnh đại diện'
          });
        }
      });
    } else {
      this.messageService.add({
        severity: 'error', summary: 'Thất bại', detail: 'Vui lòng chọn ảnh dưới 5MB'
      });
    }
  }
}
