import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {ProfileRoutingModule} from "./profile-routing.module";
import {ButtonModule} from "primeng/button";
import {ToastModule} from "primeng/toast";
import {DividerModule} from "primeng/divider";
import {PanelModule} from "primeng/panel";
import {CalendarModule} from "primeng/calendar";
import {ProfileComponent} from "./profile.component";
import {ControlMessageComponent} from "../../shared/control-message/control-message.component";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    ChangePasswordComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ButtonModule,
    ToastModule,
    DividerModule,
    PanelModule,
    CalendarModule,
    ControlMessageComponent,
    ReactiveFormsModule,
  ]
})
export class ProfileModule {
}
