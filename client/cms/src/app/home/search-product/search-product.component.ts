import {ChangeDetectorRef, Component} from '@angular/core';
import {CategoryService} from "../../service/category.service";
import {ProductService} from "../../service/product.service";
import {ActivatedRoute} from "@angular/router";
import {delay} from "rxjs";

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.scss']
})
export class SearchProductComponent {
  pageInfo = {
    currentPage: 1,
    recordPerPage: 16,
    categoryId: "",
    keyword: "",
    typeSorted: ""
  }


  categories;
  first: number = 0;

  rows: number = 10;
  products;
  totalRecord;

  constructor(private categoryService: CategoryService,
              private productService: ProductService,
              private cdr: ChangeDetectorRef,
              private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.loadCategories();
    this.route.queryParams.subscribe(params => {
      this.pageInfo.keyword = params['keyword'] || ''; // Lấy giá trị keyword từ URL
      this.loadProduct(); // Gọi hàm loadProduct để tìm kiếm sản phẩm
    });
  }

  loadCategories(): void {
    this.categoryService.getAllCategoriesWithChildren().subscribe(data => {
      this.categories = data
    });
  }

  onSortChange(sortType: string) {
    this.pageInfo.typeSorted = sortType;
    this.loadProduct();
  }

  onPageChange(event: any) {
    this.pageInfo.currentPage = event.page + 1;
    this.pageInfo.recordPerPage = event.rows;
    this.loadProduct();
    console.log('Page changed:', event);
  }

  loadProduct(): void {
    this.products = null;
    this.productService.searchProducts(this.pageInfo).pipe(delay(1500)).subscribe(data => {
      this.products = data;
      console.log(data);
    });
    this.productService.countProducts(this.pageInfo).subscribe(data => this.totalRecord = data)
    this.cdr.detectChanges();
  }
}
