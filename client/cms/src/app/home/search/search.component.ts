import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../../service/product.service";
import {CategoryService} from "../../service/category.service";
import {delay} from "rxjs";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  categories: any[] = [];
  products: any;
  categoryName: string;

  pageInfo = {
    currentPage: 1,
    recordPerPage: 8,
    categoryId: '',
    subCategoryId: '',
    typeSorted: ''
  };
  totalRecord: number;

  isCategoryPage = false;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private categoryService: CategoryService,
  ) {
  }

  ngOnInit() {
    // console.log('search component')
    this.isCategoryPage = this.route.routeConfig.data['isCategoryPage'];
    this.searchProducts();
  }

  onSortChange(sortType: string) {
    this.pageInfo.typeSorted = sortType;
    this.searchProducts();
  }

  onPageChange(event: any) {
    this.pageInfo.currentPage = event.page + 1;
    this.pageInfo.recordPerPage = event.rows;
    this.searchProducts();
  }

  searchProducts() {
    // Lấy categoryId từ param URL
    this.route.paramMap.subscribe(params => {
      this.products = null; // Reset sản phẩm trước khi tìm kiếm
      const categoryId = params.get('id');

      // Kiểm tra nếu có categoryId mới thực hiện gọi API
      if (categoryId) {
        this.pageInfo.categoryId = categoryId;

        // Gọi API để lấy danh mục con
        this.categoryService.getCategoriesTreeByParent(categoryId).subscribe(cats => {
          if (cats.length > 1) {
            this.categories = cats;
            console.log('Categories:', cats);
            // Lấy tên danh mục
            this.categoryService.getCategoriesByCode(categoryId).subscribe(cat => {
              this.categoryName = cat.name;
            });
          } else {
            // Trường hợp không có nhiều danh mục con, lấy danh mục cha
            this.categoryService.getCategoriesByCode(categoryId).subscribe(cat => {
              this.categoryName = cat.name;
              console.log('Parent Category:', cat.parent);
              this.categoryService.getCategoriesTreeByParent(cat.parent).subscribe(data => {
                this.categories = data;
              });
            });
          }
        });

        // Tìm sản phẩm theo danh mục
        this.productService.searchProducts(this.pageInfo).pipe(delay(2000)).subscribe(data => {
          this.products = data;
          console.log('Products found:', data);

          // Kiểm tra nếu không có sản phẩm
          if (this.products.length === 0) {
            // Cập nhật subCategoryId bằng categoryId từ params và tìm kiếm lại
            this.pageInfo.subCategoryId = categoryId;
            this.pageInfo.categoryId = ''; // Clear categoryId để tìm theo subCategoryId
            console.log('No products found, searching by subCategoryId:', this.pageInfo.subCategoryId);
            this.productService.searchProducts(this.pageInfo).subscribe(data => {
              this.products = data;
              console.log('Products found after re-search:', data);
            });
          }
        });

        // Đếm tổng số sản phẩm
        this.productService.countProducts(this.pageInfo).subscribe(data => {
          this.totalRecord = data;
          if (data == 0) {
            this.pageInfo.subCategoryId = categoryId;
            this.pageInfo.categoryId = '';
            this.productService.countProducts(this.pageInfo).subscribe(newData => this.totalRecord = newData);
          }
        });
      }
    });
  }



  // getCategoryName(id) {
  //   this.categoryService.getCategoryById(id).subscribe(data => {
  //     this.categoryName = data.name
  //     console.log(data.name, 'Name cat')
  //     this.breadcrumbService.set('@Category', data.name.toString());
  //   })
  // }
}
