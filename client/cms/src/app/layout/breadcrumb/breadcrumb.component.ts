import {Component} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {Title} from '@angular/platform-browser';
import {CategoryService} from "../../service/category.service";
import {forkJoin, map, of} from "rxjs";

interface Breadcrumb {
  label: {
    title: string;
  };
  url: string;
}

@Component({
  selector: 'app-breadcrumb',
  template: `
    <nav class="ezb_breadcrumb" style="background: #f2f4f8;font-weight: 500;">
      <div class="container">
        <ol class="breadcrumb mb-0" style="padding: 20px 0">
          <li *ngFor="let breadcrumb of breadcrumbs; let last=last; let first=first" class="breadcrumb-item"
              [ngClass]="{active: last}">
            <a *ngIf="!last || first" [routerLink]="breadcrumb.url"><i *ngIf="first"
                                                                       class="fa fa-home"></i> {{breadcrumb.label['title'] }}
            </a>
            <span *ngIf="last && !first"
                  [routerLink]="breadcrumb.url">{{breadcrumb.label['title'] }}</span>
          </li>
        </ol>
      </div>
    </nav>`
})
export class BreadcrumbComponent {
  breadcrumbs: Breadcrumb[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private title: Title,
    private categoryApiService: CategoryService,
  ) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.breadcrumbs = [];
      let currentRoute = this.route.root;
      let url = '';
      let label = '';

      let lastRoute = null;
      do {
        const childrenRoutes = currentRoute.children;
        currentRoute = null;
        childrenRoutes.forEach(childrenRoute => {
          const routeSnapshot = childrenRoute.snapshot;
          url += '/' + routeSnapshot.url.map(segment => segment.path).join('/');

          const data = childrenRoute.snapshot.data;
          const paramMap: any = childrenRoute.snapshot.paramMap;

          if (data['title'] && label !== data['title']) {
            Object.keys(paramMap.params).forEach(key => {
              data['title'] = data['title'].replace(new RegExp(':' + key, 'g'), paramMap.params[key]);
            });

            if (childrenRoute.snapshot.data['url']) {
              url = data['url'];
            }
            this.breadcrumbs.push({
              label: {title: data['title']},
              url: url
            });

            label = childrenRoute.snapshot.data['title'];
          }

          currentRoute = childrenRoute;
          lastRoute = childrenRoute;
        });
      } while (currentRoute);

      if (lastRoute != null && lastRoute.snapshot.params['id']) {
        this.categoryApiService.getCategoriesByCode(lastRoute.snapshot.params['id']).subscribe(cats => {
          const breadcrumbObservables = [];

          // Check if parent category exists
          if (cats.parent) {
            breadcrumbObservables.push(
              this.categoryApiService.getCategoriesByCode(cats.parent).pipe(
                map(cat => ({
                  label: {
                    title: cat?.name
                  },
                  url: `/category/${cat.code}`
                }))
              )
            );
          }

          // Always include the current category breadcrumb
          breadcrumbObservables.push(
            of({
              label: {
                title: cats.name
              },
              url: `/category/${cats.code}`
            })
          );

          // Use forkJoin to combine observables and ensure all are completed
          forkJoin(breadcrumbObservables).subscribe(breadcrumbs => {
            this.breadcrumbs.push(...breadcrumbs);
          });

        });
      }

      this.title.setTitle(label);
    });
  }
}
