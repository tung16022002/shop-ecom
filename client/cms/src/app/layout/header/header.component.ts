import {Component, inject, OnInit, TemplateRef} from '@angular/core';
import {StartupService} from "../../service/startup.service";
import {CartService} from "../../service/cart.service";
import {Router} from "@angular/router";
import {NgbOffcanvas, OffcanvasDismissReasons} from "@ng-bootstrap/ng-bootstrap";
import {AuthService} from "../../service/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLogin: boolean;
  quantityProduct: string;
  userID: string;
  textSearch: any;


  constructor(
    private startupService: StartupService,
    private cartService: CartService,
    private router: Router,
    private authService: AuthService,
  ) {
  }

  private offcanvasService = inject(NgbOffcanvas);
  closeResult = '';

  open(content: TemplateRef<any>) {
    this.offcanvasService.open(content, { ariaLabelledBy: 'offcanvas-basic-title' }).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  private getDismissReason(reason: any): string {
    switch (reason) {
      case OffcanvasDismissReasons.ESC:
        return 'by pressing ESC';
      case OffcanvasDismissReasons.BACKDROP_CLICK:
        return 'by clicking on the backdrop';
      default:
        return `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.isLogin = this.startupService.loggedIn();
    if (this.isLogin) {
      this.userID = this.startupService.getCurrentUser().id;
      this.cartService.productCount$.subscribe(count => this.quantityProduct = String(count));
      this.cartService.updateProductCount(this.userID);

    }
  }

  onSearch() {
    if (this.textSearch == undefined) {
      return;
    }
    this.router.navigate(['shopping/searchProduct'], { queryParams: { keyword: this.textSearch } });

  }

  protected readonly parseInt = parseInt;

  logOut() {
    this.authService.clearToken();
    this.router.navigate(['/']).then(() => {
      location.reload();
    });
  }
}
