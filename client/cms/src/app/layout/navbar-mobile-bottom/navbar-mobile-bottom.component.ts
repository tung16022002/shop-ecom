import {Component, OnInit} from '@angular/core';
import {StartupService} from "../../service/startup.service";
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-navbar-mobile-bottom',
  templateUrl: './navbar-mobile-bottom.component.html',
  styleUrls: ['./navbar-mobile-bottom.component.scss']
})
export class NavbarMobileBottomComponent implements OnInit{
  listMenu = [
    {
      active: false,
      name: 'Trang chủ',
      icon: 'fa-solid fa-house',
      routing: '/shopping',
      noLogin: '/shopping'
    },
    {
      active: false,
      name: 'Giỏ hàng',
      icon: 'fa-solid fa-cart-shopping',
      routing: '/cart',
      noLogin: '/login'
    },
    {
      active: false,
      name: 'Đơn hàng',
      icon: 'fa-solid fa-truck',
      routing: '/order',
      noLogin: '/login'
    },
    {
      active: false,
      name: 'Tài khoản',
      icon: 'fa-solid fa-user',
      routing: '/profile',
      noLogin: '/login'
    }
  ]

  isLogin: boolean;

  constructor(
    private startupService: StartupService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.isLogin = this.startupService.loggedIn();
    // Đặt menu active dựa vào URL hiện tại
    this.setActiveMenu(this.router.url);

    // Lắng nghe sự kiện điều hướng để cập nhật trạng thái menu
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.setActiveMenu(event.urlAfterRedirects);
      }
    });
  }

  setActiveMenu(url: string): void {
    this.listMenu.forEach(menu => {
      const route = this.isLogin ? menu.routing : menu.noLogin;
      menu.active = route === url; // Đặt menu active nếu URL trùng
    });
  }


  selectMenu(menu) {
    this.listMenu.forEach(item => item.active = false);
    menu.active = true;
  }

}
