import {Component, OnInit} from '@angular/core';
import {StartupService} from "../../service/startup.service";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  currentUser: string;
  isLogin: boolean;
  avt: string;

  constructor(
    private startupService: StartupService,
    private authService: AuthService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.isLogin = this.startupService.loggedIn();
    if (this.isLogin) {
      this.currentUser = this.startupService.getCurrentUser().fullName;
      this.avt = this.startupService.getCurrentUser().avt;
    }
  }

  logOut() {
    this.authService.clearToken();
    this.router.navigate(['/']).then(() => {
      location.reload();
    });
  }
}
