import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../service/user.service";
import {AuthService} from "../service/auth.service";
import {MessageService} from 'primeng/api'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {
  signInForm: FormGroup;
  errorMessage: string | null = null;
  hidePassword: boolean = true;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private authService: AuthService,
    private messageService: MessageService
  ) {
  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.signInForm = this.fb.group({
      phoneNumber: ['', Validators.compose([
        Validators.required,
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])]
    })
  }

  login() {
    if (this.signInForm.invalid) {
      this.signInForm.markAllAsTouched();
      return;
    }
    const data = this.signInForm.value;
    this.userService.login(data).subscribe(res => {
        // if(res.active == true) {
          this.authService.saveToken(res);
          this.router.navigate(['/shopping']);
        // }
        // else {
        //   this.messageService.add({
        //     key: 'tc',
        //     severity: 'error',
        //     summary: 'Đăng nhập thất bại',
        //     detail: 'Số điện thoại hoặc mật khẩu không đúng !'
        //   });
        // }
      },
      error => {
        // this.errorMessage = 'Số điện thoại hoặc mật khẩu không chính xác';
        this.messageService.add({
          key: 'tc',
          severity: 'error',
          summary: 'Đăng nhập thất bại',
          detail: 'Số điện thoại hoặc mật khẩu không đúng !'
        });
      }
    )
  }

  togglePassword() {
    this.hidePassword = !this.hidePassword;
  }
}
