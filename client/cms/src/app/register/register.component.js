"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var api_1 = require("primeng/api");
var rxjs_1 = require("rxjs");
var RegisterComponent = exports.RegisterComponent = function () {
    var _classDecorators = [(0, core_1.Component)({
            selector: 'app-register',
            templateUrl: './register.component.html',
            styleUrls: ['./register.component.scss'],
            providers: [api_1.MessageService]
        })];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var RegisterComponent = _classThis = /** @class */ (function () {
        function RegisterComponent_1(fb, userService, messageService, router, validationService) {
            this.fb = fb;
            this.userService = userService;
            this.messageService = messageService;
            this.router = router;
            this.validationService = validationService;
            this.hidePassword = true;
        }
        RegisterComponent_1.prototype.ngOnInit = function () {
            this.initData();
        };
        RegisterComponent_1.prototype.initData = function () {
            this.registerForm = this.fb.group({
                phoneNumber: ['', forms_1.Validators.compose([
                        forms_1.Validators.required,
                        forms_1.Validators.minLength(10),
                        forms_1.Validators.pattern(/^(0|\+84)(3|5|7|8|9)(\d{8})$/),
                    ]),
                    this.validateUserNameFromAPIDebounce.bind(this)
                ],
                fullName: ['', forms_1.Validators.compose([forms_1.Validators.required])],
                password: ['', forms_1.Validators.compose([
                        forms_1.Validators.required,
                        forms_1.Validators.minLength(6),
                        forms_1.Validators.maxLength(30),
                        forms_1.Validators.pattern(/^(?=.*[A-Z])(?=.*\d)(?=.*[@!#$%^&*.])[A-Za-z\d@!#$%^&*.]{6,30}$/),
                    ])],
                active: [true],
                retypePassword: ['', forms_1.Validators.compose([forms_1.Validators.required])],
                address: ['', forms_1.Validators.compose([forms_1.Validators.required])],
                email: ['', forms_1.Validators.compose([
                        forms_1.Validators.required,
                        forms_1.Validators.email
                    ])],
                roleName: "user"
            }, {
                validators: this.validateControlsValue('password', 'retypePassword')
            });
        };
        RegisterComponent_1.prototype.togglePasswordVisibility = function () {
            this.hidePassword = !this.hidePassword;
        };
        RegisterComponent_1.prototype.register = function () {
            var _this = this;
            if (this.registerForm.invalid) {
                this.registerForm.markAllAsTouched();
                return;
            }
            console.log(this.registerForm.value);
            var data = this.registerForm.value;
            this.userService.register(data).subscribe(function (data) {
                _this.messageService.add({ severity: 'success', summary: 'Thành công', detail: 'Đăng ký tài khoản thành công' });
                setTimeout(function () {
                    _this.router.navigate(['/login']);
                }, 1500);
            });
        };
        RegisterComponent_1.prototype.validateControlsValue = function (firstControlName, secondControlName) {
            return function (formGroup) {
                var firstControlValue = formGroup.get(firstControlName).value;
                var secondControlValue = formGroup.get(secondControlName).value;
                return firstControlValue === secondControlValue ? null : { valueNotMatch: true };
            };
        };
        RegisterComponent_1.prototype.validateUserNameFromAPIDebounce = function (control) {
            var _this = this;
            return (0, rxjs_1.timer)(300).pipe((0, rxjs_1.switchMap)(function () {
                return _this.userService.validatePhoneNumber(control.value).pipe((0, rxjs_1.map)(function (isValid) {
                    if (isValid) {
                        return null;
                    }
                    return {
                        phoneNumberDuplicated: true
                    };
                }));
            }));
        };
        return RegisterComponent_1;
    }());
    __setFunctionName(_classThis, "RegisterComponent");
    (function () {
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        RegisterComponent = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return RegisterComponent = _classThis;
}();
