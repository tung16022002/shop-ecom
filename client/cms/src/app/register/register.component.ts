import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {UserService} from "../service/user.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {map, Observable, switchMap, timer} from "rxjs";
import {ValidationService} from "../shared/service/validation.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [MessageService]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  hidePassword = true;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private messageService: MessageService,
              private router: Router,
              private validationService: ValidationService
  ) {
  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.registerForm = this.fb.group({
      phoneNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.pattern(/^(0|\+84)(3|5|7|8|9)(\d{8})$/),
      ]),
        this.validateUserNameFromAPIDebounce.bind(this)
      ],
      fullName: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(30),
        Validators.pattern(/^(?=.*[A-Z])(?=.*\d)(?=.*[@!#$%^&*.])[A-Za-z\d@!#$%^&*.]{6,30}$/),
      ])],
      active: [true],
      retypePassword: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      roleName: "user"
    }, {
      validators: this.validateControlsValue('password', 'retypePassword')
    });
  }

  togglePasswordVisibility() {
    this.hidePassword = !this.hidePassword;
  }

  register() {
    if (this.registerForm.invalid) {
      this.registerForm.markAllAsTouched();
      return;
    }
    console.log(this.registerForm.value);
    const data = this.registerForm.value;
    this.userService.register(data).subscribe(data => {
      this.messageService.add({severity: 'success', summary: 'Thành công', detail: 'Đăng ký tài khoản thành công'});
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 1500);
    });
  }

  validateControlsValue(firstControlName: string, secondControlName: string) {
    return function (formGroup: FormGroup) {
      const {value: firstControlValue} = formGroup.get(firstControlName);
      const {value: secondControlValue} = formGroup.get(secondControlName);
      return firstControlValue === secondControlValue ? null : {valueNotMatch: true};
    };
  }

  validateUserNameFromAPIDebounce(control: AbstractControl): Observable<ValidationErrors | null> {
    return timer(300).pipe(
      switchMap(() =>
        this.userService.validatePhoneNumber(control.value).pipe(
          map(isValid => {
            if (isValid) {
              return null;
            }
            return {
              phoneNumberDuplicated: true
            };
          })
        )
      )
    );
  }
}
