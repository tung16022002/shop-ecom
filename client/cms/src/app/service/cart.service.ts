import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from "../../enviroment/enviroment";
import {Cart} from "../dtos/cart/cart";
import {CartAddInfo} from "../dtos/cart/cart.info";
import {CartUpdateInfo} from "../dtos/cart/cartUpdate";
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private apiPrefix = `${environment.apiUrl}/cart`;

  // BehaviorSubject to keep track of product count
  private productCountSubject = new BehaviorSubject<number>(0);
  productCount$ = this.productCountSubject.asObservable();

  constructor(private http: HttpClient) {
  }

  // Lấy giỏ hàng của người dùng theo userId
  getCart(userId: string): Observable<Cart> {
    return this.http.get<Cart>(`${this.apiPrefix}/${userId}`);
  }

  //Update gio hang
  updateCart(userId: string, model: CartUpdateInfo): Observable<any> {
    return this.http.post<any>(`${this.apiPrefix}/update/${userId}`, model);
  }

  // Thêm sản phẩm vào giỏ hàng
  addProductToCart(userId: string, model: CartAddInfo): Observable<Cart> {
    return this.http.post<Cart>(`${this.apiPrefix}/add/${userId}`, model).pipe(
      tap(() => this.updateProductCount(userId))
    );
  }

  // Xóa sản phẩm khỏi giỏ hàng
  removeProductFromCart(userId: string, model: CartAddInfo): Observable<Cart> {
    return this.http.post<Cart>(`${this.apiPrefix}/remove/${userId}`, model).pipe(
      tap(() => this.updateProductCount(userId))
    );
  }

  // Lấy tổng giá của giỏ hàng
  getTotalPrice(userId: string): Observable<number> {
    return this.http.get<number>(`${this.apiPrefix}/total-price/${userId}`);
  }

  // Lấy số lượng sản phẩm khác nhau trong giỏ hàng
  getProductCount(userId: string): Observable<number> {
    return this.http.post<number>(`${this.apiPrefix}/count/${userId}`, {});
  }

  unselectProducts(userId: string): Observable<Cart> {
    return this.http.post<Cart>(`${this.apiPrefix}/unSelected/${userId}`, {});
  }

  // Update product count and notify all subscribers
  updateProductCount(userId: string): void {
    this.getProductCount(userId).subscribe(count => {
      this.productCountSubject.next(count);
    });
  }
}
