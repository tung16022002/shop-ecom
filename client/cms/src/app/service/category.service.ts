import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../enviroment/enviroment";
import {CategoriesTree} from "../dtos/categoriesTree";
import {CategoryDTO} from "../dtos/category";
import {Categories} from "../dtos/categories";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private apiUrl = `${environment.apiUrl}/categories`;// Replace with your Spring Boot backend API base URL

  constructor(private http: HttpClient) {
  }


  getSubcategoriesByCategoryId(categoryId: string): Observable<CategoryDTO[]> {
    return this.http.get<CategoryDTO[]>(`${this.apiUrl}/subCategory/${categoryId}`);
  }


  getCategoryTreeWithPath(categoryId: string): Observable<CategoriesTree> {
    return this.http.get<CategoriesTree>(`${this.apiUrl}/getCategoriesTreeWithPathByParent?parent=${categoryId}`);
  }

  getCategoriesByParent(parent: string): Observable<Categories[]> {
    const params = new HttpParams().set('parent', parent);
    return this.http.get<Categories[]>(`${this.apiUrl}/getCategoriesByParent`, {params});
  }

  // getCategoryByCode(code: string): Observable<Categories> {
  //   const params = new HttpParams().set('code', code);
  //   return this.http.get<Categories>(`${this.apiUrl}/getCategoriesByCode`, { params });
  // }

  //Lay danh sach Tree Category bang code
  getCategoriesTreeByParent(parent: string): Observable<CategoriesTree[]> {
    const params = new HttpParams().set('parent', parent);
    return this.http.get<CategoriesTree[]>(`${this.apiUrl}/getCategoriesTreeByParent`, {params});
  }

  getCategoriesByCode(code: string): Observable<Categories> {
    const params = new HttpParams().set('code', code);
    return this.http.get<Categories>(`${this.apiUrl}/getCategoriesByCode`, {params});
  }

  getAllCategoriesWithChildren(): Observable<CategoriesTree[]> {
    return this.http.get<CategoriesTree[]>(`${this.apiUrl}/getAll`);
  }


}
