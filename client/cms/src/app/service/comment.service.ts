import {Injectable} from "@angular/core";
import {environment} from "../../enviroment/enviroment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private apiUrl = `${environment.apiUrl}/comments`;// Replace with your Spring Boot backend API base URL

  constructor(private http: HttpClient) {
  }

  createComment(comment): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}`, comment);
  }

  // Lấy danh sách bình luận cho một sản phẩm theo productId
  getCommentsByProductId(productId: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${productId}`);
  }

  uploadProductImages( files: File[]): Observable<any> {
    const formData: FormData = new FormData();
    files.forEach(file => formData.append('files', file, file.name));
    return this.http.post<any>(`${this.apiUrl}/upload-list`, formData);
  }
}
