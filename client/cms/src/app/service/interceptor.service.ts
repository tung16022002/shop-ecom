import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {MessageService} from "primeng/api";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService,
              private router: Router,
              private messageService: MessageService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.getToken();
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Phiên đăng nhập hết hạn',
            detail: 'Vui lòng đăng nhập lại'
          });
          // Xử lý khi token hết hạn
          this.authService.clearToken(); // Gọi phương thức để xóa token và thông tin đăng nhập
          this.router.navigate(['/login']); // Chuyển hướng đến trang đăng nhập
        }
        return throwError(error);
      })
    );
  }
}
