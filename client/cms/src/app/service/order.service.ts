import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../enviroment/enviroment";
import {OrderDTO} from "../dtos/order/order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private apiUrl = `${environment.apiUrl}/orders`;

  constructor(private http: HttpClient) {
  }

  // Create a new order
  createOrder(order: OrderDTO): Observable<OrderDTO> {
    return this.http.post<OrderDTO>(this.apiUrl, order);
  }

  // Get all orders
  getAllOrders(): Observable<OrderDTO[]> {
    return this.http.get<OrderDTO[]>(this.apiUrl);
  }

  // Get orders by userId
  getOrdersByUserId(userId: string): Observable<OrderDTO[]> {
    return this.http.get<OrderDTO[]>(`${this.apiUrl}/user/${userId}`);
  }

  // Get order by id
  getOrderById(id: string): Observable<OrderDTO> {
    return this.http.get<OrderDTO>(`${this.apiUrl}/${id}`);
  }

  // Update an order
  updateOrder(order: OrderDTO): Observable<OrderDTO> {
    return this.http.put<OrderDTO>(this.apiUrl, order);
  }

  // Delete an order by id
  deleteOrder(id: string): Observable<OrderDTO> {
    return this.http.delete<OrderDTO>(`${this.apiUrl}/${id}`);
  }

  // private httpOptions() {
  //   return {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json'
  //     })
  //   };
  // }
}
