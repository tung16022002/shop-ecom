import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  private apiUrl = 'http://localhost:8088/api/v1/payment/vn-pay'; // Đường dẫn tới API Spring Boot của bạn

  constructor(private http: HttpClient) {}

  /**
   * Tạo yêu cầu thanh toán đến VNPay.
   * @param paymentRequest Đối tượng chứa thông tin thanh toán
   * @returns Observable chứa URL thanh toán của VNPay
   */
  createVnPayPayment(paymentRequest: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<any>(this.apiUrl, paymentRequest, { headers });
  }
}
