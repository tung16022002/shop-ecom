import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from "../../enviroment/enviroment";
import {ProductDTO} from "../dtos/product/productDTO";
import {ProductPageInfo} from "../dtos/productPageInfo";
import {ProductImageDTO} from "../dtos/product/productImageDTO";
import {ProductCartInfoDTO} from "../dtos/product/productCartInfoDTO";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private apiUrl = `${environment.apiUrl}/products`;

  constructor(private http: HttpClient) {
  }

  getAllProducts(): Observable<ProductDTO[]> {
    return this.http.get<ProductDTO[]>(this.apiUrl);
  }

  searchProducts(model: ProductPageInfo): Observable<ProductDTO[]> {
    return this.http.post<ProductDTO[]>(`${this.apiUrl}/search`, model);
  }

  getProductsByIds(productIds: string[]): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/getList`, productIds);
  }

  countProducts(model: ProductPageInfo): Observable<number> {
    return this.http.post<number>(`${this.apiUrl}/count`, model);
  }

  getProductById(id: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${id}`);
  }

  getListImages(id: string): Observable<ProductImageDTO> {
    return this.http.get<ProductImageDTO>(`${this.apiUrl}/getThumbnail/${id}`);
  }

  createProduct(product: ProductDTO): Observable<ProductDTO> {
    return this.http.post<ProductDTO>(this.apiUrl, product);
  }

  updateProduct(product: ProductDTO): Observable<ProductDTO> {
    return this.http.put<ProductDTO>(this.apiUrl, product);
  }

  deleteProduct(id: string): Observable<ProductDTO> {
    return this.http.delete<ProductDTO>(`${this.apiUrl}/${id}`);
  }

  uploadProductImages(id: string, files: File[]): Observable<any> {
    const formData: FormData = new FormData();
    files.forEach(file => formData.append('files', file, file.name));
    return this.http.post<any>(`${this.apiUrl}/upload/${id}`, formData);
  }
}
