import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() {
  }

  static getValidatorErrorMessage(validatorName: string, label: string, validatorValue?: any) {
    const config = {
      'required': `${label} là bắt buộc`,
      'minlength': `${label} phải lớn hơn ${validatorValue.requiredLength} ký tự`,
      'duplicated': ` ${label} đã tồn tại trong hệ thống. Vui lòng nhập ${label} khác!`,
      'pattern': `${label} không khớp`,
      'valueNotMatch': `Mật khẩu và nhập lại mật khẩu không khớp`,
    }
    return config[validatorName];
  }

}
