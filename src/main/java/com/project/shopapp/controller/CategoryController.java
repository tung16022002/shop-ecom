package com.project.shopapp.controller;

import com.project.shopapp.models.Categories;
import com.project.shopapp.models.CategoriesTree;
import com.project.shopapp.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;


@RestController
@RequiredArgsConstructor
@RequestMapping("${api.prefix}/categories")
public class CategoryController {
    private final CategoryService categoryService;

    //Tao Category
    @PostMapping("/create")
    public ResponseEntity<?> createCategory(@RequestBody Categories category) {
        try {
            // Gọi phương thức tạo danh mục từ service
            Categories createdCategory = categoryService.createCategory(category);
            return ResponseEntity.ok(createdCategory);
        } catch (Exception e) {
            // Hiển thị lỗi cụ thể dựa trên nội dung của Exception
            String errorMessage;
            HttpStatus status;

            if (e.getMessage().contains("Tên danh mục đã tồn tại")) {
                errorMessage = "Lỗi: Tên danh mục đã tồn tại.";
                status = HttpStatus.FORBIDDEN;  // Trả về lỗi 403
            } else if (e.getMessage().contains("Parent category không tồn tại")) {
                errorMessage = "Lỗi: Parent category không tồn tại.";
                status = HttpStatus.BAD_REQUEST;  // Trả về lỗi 400
            } else {
                errorMessage = "Lỗi không xác định: " + e.getMessage();
                status = HttpStatus.INTERNAL_SERVER_ERROR;  // Trả về lỗi 500
            }

            // Trả về lỗi với mã trạng thái phù hợp
            return ResponseEntity.status(status).body(errorMessage);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable("id") String id) {

        return ResponseEntity.ok(categoryService.deleteCategory(id));
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<CategoriesTree>> getAllCategoriesWithChildren() {
        List<CategoriesTree> categoriesTree = categoryService.getAllCategoriesWithChildren();
        return ResponseEntity.ok(categoriesTree);
    }

    @GetMapping("parent")
    public ResponseEntity<List<Categories>> getCategoriesWithParent() {
        List<Categories> categories = categoryService.getCategoryParents();
        return ResponseEntity.ok(categories);
    }

    @GetMapping("children/{parent}")
    public ResponseEntity<List<Categories>> getCategoryChild(@PathVariable("parent") String parent) {
        List<Categories> categories = categoryService.getCategoryByParent(parent);
        return ResponseEntity.ok(categories);
    }

    @PostMapping("/upload/{id}")
    public ResponseEntity<?> uploadThumbnail(@RequestParam("file") MultipartFile file, @PathVariable("id") String id) throws IOException {
       categoryService.uploadThumbnail(file, id);
        return ResponseEntity.ok("done");
    }


    //Lay danh sach categories bang parent
    @GetMapping("/getCategoriesByParent")
    public ResponseEntity<List<Categories>> getCategoriesByParent(@RequestParam String parent) {
        List<Categories> categories = categoryService.getCategoriesByParent(parent);
        return ResponseEntity.ok(categories);
    }

    //Lay danh sach TreeCategories bang parent
    @GetMapping("/getCategoriesTreeByParent")
    public ResponseEntity<List<CategoriesTree>> getCategoriesTreeByParent(@RequestParam String parent) {
        List<CategoriesTree> categoriesTree = categoryService.getCategoriesTreeByParent(parent);
        return ResponseEntity.ok(categoriesTree);
    }

    //Lay Category bang code
    @GetMapping("/getCategoriesByCode")
    public ResponseEntity<?> getCategoriesByCode(@RequestParam String code) {
        try {
            Categories category = categoryService.getCategoriesByCode(code);
            return ResponseEntity.ok(category);
        } catch (AccessDeniedException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
    //Lay Tree Path bang Parent
    @GetMapping("/getCategoriesTreeWithPathByParent")
    public ResponseEntity<CategoriesTree> getCategoriesTreeWithPathByParent(@RequestParam String parent) {
        CategoriesTree categoriesTree = categoryService.getCategoriesTreeWithPathByParent(parent);
        return ResponseEntity.ok(categoriesTree);
    }

    @PutMapping("/updateNameCategories")
    public ResponseEntity<Categories> updateNameCategories(@RequestBody Categories category) {
        try {
            Categories updatedCategory = categoryService.updateNameCategories(category);
            return ResponseEntity.ok(updatedCategory);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping("/getCategoriesByName")
    public ResponseEntity<Categories> getCategoriesByName(@RequestParam String name) {
        Categories category = categoryService.getCategoriesByName(name);
        return ResponseEntity.ok(category);
    }

    @GetMapping("/getMapCategoriesByParent")
    public ResponseEntity<Map<String, String>> getMapCategoriesByParent(@RequestParam String parent) {
        Map<String, String> categoriesMap = categoryService.getMapCategoriesByParent(parent);
        return ResponseEntity.ok(categoriesMap);
    }
}
