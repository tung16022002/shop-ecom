package com.project.shopapp.controller;

import com.project.shopapp.dtos.CommentDTO;
import com.project.shopapp.models.ProductImage;
import com.project.shopapp.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("${api.prefix}/comments")
@RequiredArgsConstructor
public class CommentController {
    private final CommentService commentService;

    @PostMapping()
    public ResponseEntity<CommentDTO> createComment(@RequestBody CommentDTO commentDTO){
        return ResponseEntity.ok(commentService.createComment(commentDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<CommentDTO>> findAllComments(
            @PathVariable("id") String productId
    ){
        return ResponseEntity.ok(commentService.getCommentsByProductId(productId));
    }

    @PostMapping("/upload-list")
    public ResponseEntity<?> uploadListThumbnail(@RequestParam("files") MultipartFile[] files) {
        List<ProductImage> images = commentService.uploadListThumbnail(files);
        return new ResponseEntity<>(images, HttpStatus.OK);
    }
}
