package com.project.shopapp.controller;

import com.project.shopapp.models.ProductAttributes;
import com.project.shopapp.service.ProductAttributeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("${api.prefix}/attributes")
public class ProductAttributesController {
    private final ProductAttributeService productAttributeService;

    @GetMapping("/{code}")
    public ResponseEntity<List<ProductAttributes>>  getByCode(@PathVariable("code") String code) throws Exception {
       List<ProductAttributes> model = this.productAttributeService.getByCode(code);
       return ResponseEntity.ok(model);
    }

    @PostMapping("/add")
    public ResponseEntity<ProductAttributes>  add(@RequestBody ProductAttributes productAttributes){
        ProductAttributes model = this.productAttributeService.create(productAttributes);
        return ResponseEntity.ok(model);
    }


}
