package com.project.shopapp.controller;

import com.cloudinary.api.exceptions.NotFound;
import com.project.shopapp.dtos.ProductDTO;
import com.project.shopapp.dtos.ProductImageDTO;
import com.project.shopapp.dtos.info.ProductCartInfo;
import com.project.shopapp.dtos.info.ProductPageInfo;
import com.project.shopapp.models.Product;
import com.project.shopapp.models.ProductImage;
import com.project.shopapp.repository.ProductRepository;
import com.project.shopapp.service.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Logger;

@RestController
@RequestMapping("${api.prefix}/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final ProductRepository productRepository;

    @GetMapping()
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(productService.getAllProduct());
    }

    //get products by page
    @PostMapping("/search")
    public ResponseEntity<?> search(
            @RequestBody ProductPageInfo model,
            BindingResult result
            ) {
        try {
            if(result.hasErrors()) {
                List<String> errMsg = result.getFieldErrors()
                        .stream()
                        .map(FieldError::getDefaultMessage)
                        .toList();
                return ResponseEntity.badRequest().body(errMsg);
            }
            return ResponseEntity.ok(productService.search(model));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

    @PostMapping("/count")
    public ResponseEntity<?> count(@RequestBody ProductPageInfo model) {
        return ResponseEntity.ok(productService.count(model));
    }


//    Get product by id
    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable("id") String productID) {
        return ResponseEntity.ok(productService.getProductById(productID));
    }

    @GetMapping("/getThumbnail/{id}")
    public ResponseEntity<List<ProductImage>> getListThumbnail(@PathVariable String id) {
        try {
            List<ProductImage> images = productService.getListThumbnail(id);
            return new ResponseEntity<>(images, HttpStatus.OK);
        } catch (ResponseStatusException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    //Get list product by categoryId
//    @GetMapping("/category/{id}")
//    public ResponseEntity<List<ProductDTO>> getProductByCategory(@PathVariable("id") String categoryID) {
//        return ResponseEntity.ok(productService.getAllByCategoryId(categoryID));
//    }
//
//    @GetMapping("/subCategory/{id}")
//    public ResponseEntity<List<ProductDTO>> getProductBySubCategory(@PathVariable("id") String subCategoryID) {
//        return ResponseEntity.ok(productService.getAllBySubCategoryId(subCategoryID));
//    }

    //Create product
    @PostMapping()
    public ResponseEntity<?> createProduct(
            @Valid @RequestBody ProductDTO model,
            BindingResult result
            ) {
        try {
            if (result.hasErrors()) {
                List<String> errMsg = result.getFieldErrors()
                        .stream()
                        .map(FieldError::getDefaultMessage)
                        .toList();
                return ResponseEntity.badRequest().body(errMsg);
            }
            ProductDTO productDTO = productService.createProduct(model);




            return ResponseEntity.ok(productDTO);
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/upload/{id}")
    public ResponseEntity<?> uploadProductImages(
            @PathVariable("id") String productId,
            @RequestParam("files") MultipartFile files) {
        try {
            ProductImage imageDTOs = productService.uploadThumbnail(files, productId);
            return ResponseEntity.ok(imageDTOs);
        } catch (ResponseStatusException e) {
            // Trả về lỗi cụ thể
            return ResponseEntity.status(e.getStatusCode()).body(e.getReason());
        } catch (IOException e) {
            // Trả về lỗi chung
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload images: " + e.getMessage());
        } catch (Exception e) {
            // Trả về lỗi không xác định
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An unexpected error occurred: " + e.getMessage());
        }
    }

//    private static final Logger logger = LoggerFactory.getLogger(Product.class);

    @PostMapping("/upload-list")
    public ResponseEntity<?> uploadListThumbnail(
            @RequestParam("files") MultipartFile[] files) {
        try {
            List<ProductImage> images = productService.uploadListThumbnail(files);
            return new ResponseEntity<>(images, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>("Failed to upload files: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>("Unexpected error: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update-text-search")
    public ResponseEntity<String> updateTextSearchForAll() {
        List<Product> products = productRepository.findAll();

        for (Product product : products) {
            product.makeTextSearch();
            productRepository.save(product);
        }

        return ResponseEntity.ok("Cập nhật textSearch cho tất cả sản phẩm thành công!");
    }



    //Update product
    @PutMapping()
    public ResponseEntity<ProductDTO> updateProduct(@RequestBody ProductDTO model) {
        return ResponseEntity.ok(productService.updateProduct(model));
    }

    //Delete product
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable String id) {
        try {
            // Thực hiện xóa sản phẩm
            ProductDTO deletedProduct = productService.deleteProductById(id);
            // Trả về thông báo thành công nếu xóa thành công
            return ResponseEntity.ok(deletedProduct);

        } catch (Exception e) {
            // Trả về lỗi 500 nếu có lỗi xảy ra trong quá trình xóa
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi khi xóa sản phẩm: " + e.getMessage());
        }
    }

    @PostMapping("/getList")
    public ResponseEntity<List<ProductCartInfo>> getList(@RequestBody List<String> ids) {
        return ResponseEntity.ok(productService.getProductByIds(ids));
    }

}
