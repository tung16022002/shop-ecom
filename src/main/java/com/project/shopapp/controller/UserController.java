package com.project.shopapp.controller;

import com.project.shopapp.dtos.UserDTO;
import com.project.shopapp.dtos.UserLoginDTO;
import com.project.shopapp.dtos.info.ChangePasswordDTO;
import com.project.shopapp.dtos.info.PhoneNumberDTO;
import com.project.shopapp.dtos.info.UserUpdateInfo;
import com.project.shopapp.exception.DataNotFoundException;
import com.project.shopapp.models.User;
import com.project.shopapp.service.CommentService;
import com.project.shopapp.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("${api.prefix}/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final CommentService commentService;

    @GetMapping("")
    public ResponseEntity<List<UserDTO>> getAllUser() {
        return ResponseEntity.ok(userService.findAll());
    }

    @PutMapping("/update")
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserUpdateInfo userDTO) throws DataNotFoundException {
        try {
            UserDTO updatedUser = userService.updateUser(userDTO);
            return ResponseEntity.ok(updatedUser);
        } catch (DataNotFoundException e) {
            throw new DataNotFoundException("User not found with the given phone number");
        }
    }


    @GetMapping("/current")
    public ResponseEntity<UserDTO> getCurrentUser() {
        UserDTO userDTO = new UserDTO();
        userDTO.setPhoneNumber(userService.getCurrentUser().getPhoneNumber());
        userDTO.setId(userService.getCurrentUser().getId());
        userDTO.setFullName(userService.getCurrentUser().getFullName());
        userDTO.setRoleName(userService.getCurrentUser().getRoleName());
       return ResponseEntity.ok(userDTO);
    }

    @PostMapping("/upload-avt/{id}")
    public ResponseEntity<UserDTO> uploadAvatar(@PathVariable("id") String id, @RequestParam("file") MultipartFile file) throws IOException {
        UserDTO userDTO = userService.uploadAvatar(file, id);
        commentService.updateAvt(userDTO.getId(), userDTO.getAvt());
        return ResponseEntity.ok(userDTO);
    }

    @PutMapping("/change-password/{phoneNumber}")
    public ResponseEntity<?> changePassword(@PathVariable("phoneNumber") String phoneNumber, @RequestBody ChangePasswordDTO changePasswordDTO) {
        try {
            User user = userService.changePassword(phoneNumber, changePasswordDTO);
            return ResponseEntity.ok(user);
        } catch (DataNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUserbyId(@PathVariable("id") String id) {
        return ResponseEntity.ok(userService.findByUserId(id));
    }
    @PostMapping("/register")
    public ResponseEntity<?> createUser(
            @Valid @RequestBody  UserDTO userDTO,
            BindingResult result
    ) {
        try {
            if(result.hasErrors()) {
                List<String> errMsg = result.getFieldErrors()
                        .stream()
                        .map(FieldError::getDefaultMessage)
                        .toList();
                return ResponseEntity.badRequest().body(errMsg);
            }
            //Kiem tra retypePassword == password khong
            if(!userDTO.getPassword().equals(userDTO.getRetypePassword()))
                return ResponseEntity.badRequest().body("Khong trung mat khau !");
           UserDTO user =  userService.createUser(userDTO);
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(
            @Valid @RequestBody UserLoginDTO userLoginDTO
    ) throws DataNotFoundException {
        String token = userService.login(userLoginDTO.getPhoneNumber(),userLoginDTO.getPassword());
        return ResponseEntity.ok(token);
    }

    @PostMapping("/validatePhoneNumber")
    public ResponseEntity<?> validatePhoneNumber(@RequestBody PhoneNumberDTO phoneNumberDTO ) {
        boolean exists = userService.checkPhoneNumberExists(phoneNumberDTO.getPhoneNumber());
        return ResponseEntity.ok(exists);
    }
}
