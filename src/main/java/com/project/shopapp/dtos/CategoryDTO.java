package com.project.shopapp.dtos;

import com.project.shopapp.models.SubCategory;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;


@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDTO {
    private String id;
    @NotEmpty(message = "Category name is not empty")
    private String name;
    private List<SubCategory> subcategories = new ArrayList<>();
}
