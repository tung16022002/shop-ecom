package com.project.shopapp.dtos;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class CommentDTO {
    private String id;
    private String productId;

    private String userId;

    private String userName;

    private String content;

    private String avtUser;

    private float rating;

    private List<String> listThumbnail = new ArrayList<>();

    private LocalDateTime createdAt;

    public CommentDTO() {
        this.createdAt = LocalDateTime.now();
    }

}
