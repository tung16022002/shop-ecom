package com.project.shopapp.dtos;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
public class ProductImageDTO {

    @Field("product_id")
    private String productId;

    @Field("image_url")
    private String imageUrl;
}
