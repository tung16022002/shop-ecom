package com.project.shopapp.dtos;

import lombok.Data;

@Data
public class SubCategoryDTO {
    String id;
    String name;
    String categoryId;
}
