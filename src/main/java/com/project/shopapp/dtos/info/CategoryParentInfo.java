package com.project.shopapp.dtos.info;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CategoryParentInfo {
    private String id;

    @NotNull
    private String name;

    private String code;

    private String path;

    private String parent;

    private int numberOfProduct;
}
