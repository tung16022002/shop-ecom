package com.project.shopapp.dtos.info;

import lombok.Data;

@Data
public class PhoneNumberDTO {
    private String phoneNumber;
}
