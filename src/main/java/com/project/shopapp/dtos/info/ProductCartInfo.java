package com.project.shopapp.dtos.info;

import lombok.Data;

import java.util.List;

@Data
public class ProductCartInfo {
    private String id;

    private String name;

    private Float price;
    private String thumbnail;
    private int quantity;
    private Float totalPrice;
    private int totalProduct;
}
