package com.project.shopapp.dtos.info;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductPageInfo extends BasePageableInfo {
//    private int currentPage = 1;
//    private int recordPerPage = 20;
    private String categoryId ;
    private String subCategoryId ;
    private String stockStatus;
}
