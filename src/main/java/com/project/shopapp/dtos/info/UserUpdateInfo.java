package com.project.shopapp.dtos.info;

import lombok.Data;

import java.util.Date;

@Data
public class UserUpdateInfo {
    private String phoneNumber;
    private String email;
    private String password;
    private String fullName;
    private String avt;
    private String address;
    private Date dateOfBirth;
    private boolean active;
    private String sex;
}
