package com.project.shopapp.models;


import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "categories")
public class Categories {
    @Id
    private String id;

    @NotNull
    private String name;

    private String code;

    private String path;

    private String parent;

    private Long numberOfProduct;

    private String thumbnail;

}
