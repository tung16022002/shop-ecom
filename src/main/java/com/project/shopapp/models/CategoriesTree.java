package com.project.shopapp.models;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class CategoriesTree {
    private List<CategoriesTree> children = new ArrayList<>();
    private String text;
    private String value;
}
