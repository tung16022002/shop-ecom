package com.project.shopapp.models;

import lombok.Data;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Document(collection = "comments")
public class Comment {
    @Id
    private String id;

    private String productId;

    private String userId;

    private String userName;

    private String avtUser;

    private List<String> listThumbnail = new ArrayList<>();

    private String content;

    private float rating;

    private LocalDateTime createdAt;

    public Comment() {
        this.createdAt = LocalDateTime.now();
    }
}
