package com.project.shopapp.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "social_accounts")
public class SocialAccount {
    @Id
    private String id;

    private String provider;

    @Field("provider_id")
    private String providerId;

    private String name;

    private String email;

    @Field("user_id")
    private String userId;
}
