package com.project.shopapp.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "subcategory")
public class SubCategory {
    @Id
    String id;
    String name;
    String categoryId;
}
