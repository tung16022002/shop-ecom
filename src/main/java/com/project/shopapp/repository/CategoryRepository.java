package com.project.shopapp.repository;

import com.project.shopapp.models.Categories;
import com.project.shopapp.models.CategoriesTree;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends MongoRepository<Categories, String> {
//    @Query(value = "{'code': ?0}")
    Categories getCategoriesByCode(String code);

    Optional<Categories> findByCode(String code);

//    @Query(value = "{'name': ?0} ")
    Categories getCategoriesByName(String name);

//    @Query(value = "{'parent': null} ")
//    @Query("{ 'parent' : ?0 }")
    List<Categories> getCategoriesByParent(String parent);

    List<Categories> findAllByParent(String parent);

    @Query(value = "{'parent': null}")
    List<Categories> findAllParentCategories();
}
