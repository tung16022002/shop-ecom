package com.project.shopapp.repository;

import com.project.shopapp.models.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends MongoRepository<Comment, String> {
    List<Comment> findByProductId(String id);
    List<Comment> findByUserId(String id);
}
