package com.project.shopapp.repository;

import com.project.shopapp.models.ProductAttributes;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductAttributesRepository extends MongoRepository<ProductAttributes, String> {
    List<ProductAttributes> findByCode(String name);
}
