package com.project.shopapp.repository;

import com.project.shopapp.dtos.ProductDTO;
import com.project.shopapp.models.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends MongoRepository<Product, String>,ProductRepositoryCustom {
    boolean existsByName(String name);

    List<Product> findAllByCategoryId(String id);
    List<Product> findAllBySubCategoryId(String id);

}
