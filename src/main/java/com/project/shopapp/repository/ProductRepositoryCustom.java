package com.project.shopapp.repository;

import com.project.shopapp.dtos.ProductDTO;
import com.project.shopapp.dtos.info.ProductPageInfo;
import com.project.shopapp.models.Product;

import java.util.List;

public interface ProductRepositoryCustom {
    List<Product> search(ProductPageInfo model);

    Long count(ProductPageInfo model);

}
