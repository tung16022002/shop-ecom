package com.project.shopapp.repository;

import com.project.shopapp.models.SubCategory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface SubCategoryRepository extends MongoRepository<SubCategory, Long> {
     Optional<SubCategory>  findById(String id);
     List<SubCategory> findAllByCategoryId(String id);
}
