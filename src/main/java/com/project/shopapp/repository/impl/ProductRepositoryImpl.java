package com.project.shopapp.repository.impl;

import com.project.shopapp.dtos.info.ProductPageInfo;
import com.project.shopapp.helper.QueryUtils;
import com.project.shopapp.models.Product;
import com.project.shopapp.repository.ProductRepositoryCustom;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;



@RequiredArgsConstructor
public class ProductRepositoryImpl implements ProductRepositoryCustom {
    private final MongoTemplate mongoTemplate;

    private Query buildSearchQuery(String categoryId, String subCategoryId,ProductPageInfo model, boolean count) {
        Query query = new Query();

        //Search text
        if (model.getKeyword() != null && !model.getKeyword().trim().isEmpty()) {
            String keyword = model.getKeyword().toLowerCase().trim();
            keyword = StringUtils.stripAccents(keyword); // Xử lý bỏ dấu

            // Tạo các điều kiện tìm kiếm cho cả trường name và textSearch
            Criteria nameCriteria = Criteria.where("name").regex(keyword, "i"); // Tìm kiếm không phân biệt hoa thường
            Criteria textSearchCriteria = Criteria.where("textSearch").regex(keyword, "i");

            // Kết hợp các điều kiện tìm kiếm
            query.addCriteria(new Criteria().orOperator(nameCriteria, textSearchCriteria));
        }

        //Search theo category
        if (StringUtils.isNotEmpty(subCategoryId) && StringUtils.isEmpty(categoryId)) {
            query.addCriteria(Criteria.where("subCategoryId").is(subCategoryId));
        }
        // Nếu không có subCategoryId, tìm kiếm theo categoryId
        else if (StringUtils.isNotEmpty(categoryId)) {
            query.addCriteria(Criteria.where("categoryId").is(categoryId));
        }

        //search theo status
        if (model.getStockStatus() != null) {
            switch (model.getStockStatus()) {
                case "instock": // còn hàng
                    query.addCriteria(Criteria.where("totalProduct").gt(10));
                    break;
                case "lowstock": // còn ít hàng
                    query.addCriteria(Criteria.where("totalProduct").gte(1).lt(10));
                    break;
                case "outstock": // hết hàng
                    query.addCriteria(Criteria.where("totalProduct").is(0));
                    break;
            }
        }

        if (StringUtils.isNotEmpty(model.getTypeSorted())) {
            if ("asc".equalsIgnoreCase(model.getTypeSorted())) {
                query.with(Sort.by(Sort.Order.asc("price")));
            } else if ("desc".equalsIgnoreCase(model.getTypeSorted())) {
                query.with(Sort.by(Sort.Order.desc("price")));
            }
        }

        if (!count) {
            QueryUtils.getQuerySortAndPageable(query, model);
        }

        return query;
    }

//    @Override
//    public List<Product> search(ProductPageInfo model) {
//        List<Product> products = mongoTemplate.find(buildSearchQuery(model.getCategoryId(), null, model,false), Product.class);
//
//        if (products.isEmpty()) {
//            // Nếu không tìm thấy sản phẩm, tìm kiếm bằng subCategoryId
//            products = mongoTemplate.find(buildSearchQuery(null, model.getCategoryId(),model ,false), Product.class);
//        }
//
//        return products;
////    }
//
//    @Override
//    public Long count(ProductPageInfo model) {
//        long count = mongoTemplate.count(buildSearchQuery(model.getCategoryId(), null, model,true), Product.class);
//
//        if (count == 0) {
//            // Nếu không tìm thấy sản phẩm, đếm bằng subCategoryId
//            count = mongoTemplate.count(buildSearchQuery(null, model.getCategoryId(), model,true), Product.class);
//        }
//
//        return count;
//    }
@Override
public List<Product> search(ProductPageInfo model) {
    // Tìm kiếm ưu tiên theo subCategoryId nếu có
    List<Product> products = mongoTemplate.find(buildSearchQuery(model.getCategoryId(), model.getSubCategoryId(), model, false), Product.class);


    if (products.isEmpty()) {
        return products;
    }

    return products;
}


    @Override
    public Long count(ProductPageInfo model) {
        // Đếm sản phẩm ưu tiên theo subCategoryId và categoryId nếu có

        // Trả về số lượng đã đếm, không có fallback đếm lại theo categoryId hoặc subCategoryId khác
        return mongoTemplate.count(buildSearchQuery(model.getCategoryId(), model.getSubCategoryId(), model, true), Product.class);
    }


}
