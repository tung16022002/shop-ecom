package com.project.shopapp.service;

import com.project.shopapp.dtos.CategoryDTO;
import com.project.shopapp.dtos.info.CategoryParentInfo;
import com.project.shopapp.models.Categories;
import com.project.shopapp.models.CategoriesTree;
import com.project.shopapp.models.SubCategory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CategoryService {
    Categories createCategory(Categories category) throws Exception;

    List<Categories> getCategoriesByParent(String parent);

    List<CategoriesTree> getCategoriesTreeByParent(String parent);

    List<Categories> getCategoryParents();

    List<Categories> getCategoryByParent(String parent);

    void uploadThumbnail(MultipartFile file, String id) throws IOException;

    List<CategoriesTree> getAllCategoriesWithChildren();

    Categories getCategoriesByCode(String code);

    CategoriesTree getCategoriesTreeWithPathByParent(String parent);

    Categories updateNameCategories(Categories category) throws Exception;

    Categories getCategoriesByName(String name);

    Map<String, String> getMapCategoriesByParent(String parent);

    Categories deleteCategory(String id);
}
