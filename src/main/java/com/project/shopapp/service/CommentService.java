package com.project.shopapp.service;

import com.project.shopapp.dtos.CommentDTO;
import com.project.shopapp.models.ProductImage;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CommentService {
    CommentDTO createComment(CommentDTO model);
    List<CommentDTO> getCommentsByProductId(String productId);
    void deleteComment(String commentId);
    void updateAvt(String userId, String avt);
    List<ProductImage> uploadListThumbnail(MultipartFile[] files);
    List<ProductImage> getListThumbnail(String id);
}
