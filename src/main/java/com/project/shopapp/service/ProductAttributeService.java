package com.project.shopapp.service;

import com.project.shopapp.models.ProductAttributes;

import java.util.List;

public interface ProductAttributeService {
    ProductAttributes create(ProductAttributes model);
    List<ProductAttributes> getByCode(String code) throws Exception;
}
