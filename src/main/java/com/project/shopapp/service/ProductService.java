package com.project.shopapp.service;


import com.project.shopapp.dtos.ProductDTO;
import com.project.shopapp.dtos.ProductImageDTO;
import com.project.shopapp.dtos.info.ProductCartInfo;
import com.project.shopapp.dtos.info.ProductPageInfo;
import com.project.shopapp.exception.DataNotFoundException;
import com.project.shopapp.exception.InValidParamException;
import com.project.shopapp.models.Product;
import com.project.shopapp.models.ProductImage;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ProductService {
    ProductDTO createProduct(ProductDTO model) throws DataNotFoundException;

    ProductDTO getProductById(String id);

    List<ProductDTO> getAllProduct();

    List<ProductDTO> search(ProductPageInfo model);

    Long count(ProductPageInfo model);

    ProductDTO updateProduct(ProductDTO model);

    ProductDTO deleteProductById(String id);

    boolean existsByName(String name);

    List<ProductDTO> getAllByCategoryId(String id);

    List<ProductDTO> getAllBySubCategoryId(String id);

    ProductImage uploadThumbnail(MultipartFile file, String id) throws IOException;

    List<ProductImage> uploadListThumbnail(MultipartFile[] files) throws IOException;

    List<ProductImage> getListThumbnail(String id);

    List<ProductCartInfo> getProductByIds(List<String> ids);

}
