package com.project.shopapp.service;

import com.project.shopapp.dtos.SubCategoryDTO;
import com.project.shopapp.models.SubCategory;

public interface SubCategoryService {
    SubCategoryDTO add(SubCategoryDTO model);
}
