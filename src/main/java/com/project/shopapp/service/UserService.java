package com.project.shopapp.service;

import com.project.shopapp.dtos.UserDTO;
import com.project.shopapp.dtos.info.ChangePasswordDTO;
import com.project.shopapp.dtos.info.UserUpdateInfo;
import com.project.shopapp.exception.DataNotFoundException;
import com.project.shopapp.exception.PermissionDenied;
import com.project.shopapp.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UserService {
    UserDTO createUser(UserDTO model) throws PermissionDenied, DataNotFoundException;
    String login(String phoneNumber, String password) throws DataNotFoundException;
    UserDTO findByUserId(String id);
    List<UserDTO> findAll();
    boolean checkPhoneNumberExists(String phoneNumber);
    User getCurrentUser();

    UserDTO uploadAvatar(MultipartFile file, String userId) throws IOException;

    UserDTO updateUser(UserUpdateInfo model) throws DataNotFoundException;

    User changePassword(String phoneNumber, ChangePasswordDTO model) throws DataNotFoundException;

}
