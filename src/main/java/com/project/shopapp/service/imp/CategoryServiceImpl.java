package com.project.shopapp.service.imp;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.project.shopapp.dtos.UserDTO;
import com.project.shopapp.dtos.info.CategoryParentInfo;
import com.project.shopapp.dtos.info.ProductPageInfo;
import com.project.shopapp.models.Categories;
import com.project.shopapp.models.CategoriesTree;
import com.project.shopapp.models.User;
import com.project.shopapp.repository.CategoryRepository;
import com.project.shopapp.repository.ProductRepository;
import com.project.shopapp.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.text.Normalizer;
import java.util.*;


@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final Cloudinary cloudinaryClient;


    @Override
    public List<CategoriesTree> getAllCategoriesWithChildren() {
        List<Categories> parentCategories = categoryRepository.findAllParentCategories();
        return buildCategoryTree(parentCategories);
    }


    private List<CategoriesTree> buildCategoryTree(List<Categories> categories) {
        List<CategoriesTree> categoriesTree = new ArrayList<>();
        for (Categories category : categories) {
            CategoriesTree tree = new CategoriesTree();
            tree.setText(category.getName());
            tree.setValue(category.getCode());
            tree.setChildren(setValueCode(category.getCode()));
            categoriesTree.add(tree);
        }
        return categoriesTree;
    }

    @Override
    public Categories createCategory(Categories category) throws Exception {
        if (categoryRepository.getCategoriesByName(category.getName()) != null) {
            throw new Exception("Ten danh muc bi trong");
        }

        if(category.getCode() == null ) {
            category.setCode(generateCodeFromName(category.getName()));
//            category.setPath(",");
        }
        else {
            Categories parent = categoryRepository.getCategoriesByCode(category.getParent());
            category.setPath(parent.getPath() + category.getName() + ",");
        }
        return categoryRepository.save(category);
    }



    private String generateCodeFromName(String name) {
        // Chuẩn hóa tên và loại bỏ dấu tiếng Việt
        String normalized = Normalizer.normalize(name, Normalizer.Form.NFD);

        // Loại bỏ các dấu tiếng Việt
        String withoutAccent = normalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
                .replaceAll("Đ", "D")
                .replaceAll("đ", "d");

        // Xóa khoảng trắng và ký tự đặc biệt, chuyển về chữ thường
        return withoutAccent.toLowerCase()
                .replaceAll("\\s+", "")  // Xóa khoảng trắng
                .replaceAll("[^a-zA-Z0-9]", "");  // Xóa các ký tự đặc biệt
    }


    @Override
    public List<Categories> getCategoriesByParent(String parent) {
        return categoryRepository.getCategoriesByParent(parent);
    }

    @Override
    public List<CategoriesTree> getCategoriesTreeByParent(String parent) {
        return setValueCode(parent);
    }

    @Override
    public List<Categories> getCategoryParents() {
        List<Categories> categories = categoryRepository.findAllParentCategories();
        categories.forEach(categories1 -> {
            ProductPageInfo productPageInfo = new ProductPageInfo();
            productPageInfo.setCategoryId(categories1.getCode());

            Long count = productRepository.count(productPageInfo);
            categories1.setNumberOfProduct(count);
        });
        return categories;
    }

    @Override
    public List<Categories> getCategoryByParent(String parent) {
        List<Categories> categories = categoryRepository.findAllByParent(parent);
        categories.forEach(categories1 -> {
            ProductPageInfo productPageInfo = new ProductPageInfo();
            productPageInfo.setSubCategoryId(categories1.getCode());

            Long count = productRepository.count(productPageInfo);
            categories1.setNumberOfProduct(count);
        });
        return categories;
    }

    @Override
    public void uploadThumbnail(MultipartFile file, String id) throws IOException {
            if (!Objects.requireNonNull(file.getContentType()).startsWith("image/")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only image files are allowed");
            }

            if (file.getSize() > 10 * 1024 * 1024) { // 10MB
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File size must be less than 10MB");
            }

            Categories categories = categoryRepository.findById(id).orElse(null);
            if(categories == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Category not found");
            }

            Map imageUpload = cloudinaryClient.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
            String avtUrl = (String) imageUpload.get("url");

            categories.setThumbnail(avtUrl);
            categoryRepository.save(categories);
    }



    private List<CategoriesTree> setValueCode(String parent) {
        List<Categories> categoriesList = categoryRepository.getCategoriesByParent(parent);

        List<CategoriesTree> categoriesTrees = new ArrayList<>();

        if (categoriesList == null || categoriesList.isEmpty()) {
            return new ArrayList<>();
        }

        categoriesList.forEach(category -> {
            CategoriesTree tree = new CategoriesTree();
            tree.setText(category.getName());
            tree.setValue(category.getCode());
            tree.setChildren(setValueCode(category.getCode()));
            categoriesTrees.add(tree);
        });

        return categoriesTrees;
    }


    @Override
    public Categories getCategoriesByCode(String code) {

        return categoryRepository.getCategoriesByCode(code);
    }

    @Override
    public CategoriesTree getCategoriesTreeWithPathByParent(String parent) {
        CategoriesTree categoriesTree = new CategoriesTree();

        Categories rootCategories = categoryRepository.getCategoriesByCode(parent);

        categoriesTree.setText(rootCategories.getName());
        categoriesTree.setValue(rootCategories.getPath() + rootCategories.getCode() + ",");
        categoriesTree.setChildren(setValuePath(parent));
        return categoriesTree;
    }

    private List<CategoriesTree> setValuePath(String parent) {

        List<Categories> categoriesList = categoryRepository.getCategoriesByParent(parent);

        List<CategoriesTree> categoriesTrees = new ArrayList<>();

        if (categoriesList == null || categoriesList.isEmpty()) {
            return new ArrayList<>();
        }

        categoriesList.forEach(category -> {
            CategoriesTree tree = new CategoriesTree();
            tree.setText(category.getName());
            tree.setValue(category.getPath() + category.getCode() + ",");
            tree.setChildren(setValuePath(category.getCode()));
            categoriesTrees.add(tree);
        });
        return categoriesTrees;
    }



    @Override
    public Categories updateNameCategories(Categories category) throws Exception {
        Categories categories = categoryRepository.findById(category.getId()).orElse(null);
        if(categories == null) {
            throw new Exception("Category khong ton tai");
        }

        List<Categories> categoriesList = categoryRepository.getCategoriesByParent(categories.getCode());


        categories.setName(category.getName());
        categories.setCode(generateCodeFromName(category.getName()));
        categoriesList.forEach(data -> {
            data.setParent(categories.getCode());
        });
        categoryRepository.saveAll(categoriesList);
        return categoryRepository.save(categories);
    }

    @Override
    public Categories getCategoriesByName(String name) {
        return categoryRepository.getCategoriesByName(name);
    }

    @Override
    public Map<String, String> getMapCategoriesByParent(String parent) {
        List<Categories> list = categoryRepository.getCategoriesByParent(parent);
        Map<String, String> result = new HashMap<>();
        list.forEach(category -> result.put(category.getCode(), category.getName()));
        return result;
    }

    @Override
    public Categories deleteCategory(String id) {
        Categories category = categoryRepository.findById(id).orElse(null);
        if(category == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Category not found");
        }

        categoryRepository.delete(category);
        return category;
    }
}
