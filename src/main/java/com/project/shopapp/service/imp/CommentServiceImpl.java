package com.project.shopapp.service.imp;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.project.shopapp.dtos.CommentDTO;
import com.project.shopapp.dtos.UserDTO;
import com.project.shopapp.models.Comment;
import com.project.shopapp.models.ProductImage;
import com.project.shopapp.repository.CommentRepository;
import com.project.shopapp.repository.ProductImageRepository;
import com.project.shopapp.service.CommentService;
import com.project.shopapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl  implements CommentService {
    private final CommentRepository commentRepository;
    private final UserService userService;
    private final Cloudinary cloudinaryClient;
    private final ProductImageRepository productImageRepository;



    public static ModelMapper mapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT)
                .setSkipNullEnabled(true);

        return modelMapper;
    }

    public static CommentDTO mapToView(Comment comment) {
        if (comment == null) {
            return null;
        }

        return mapper().map(comment, CommentDTO.class);
    }

    public static List<CommentDTO> mapToView(List<Comment> comments) {
        return comments.stream().map(d -> mapper().map(d, CommentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public CommentDTO createComment(CommentDTO model) {
        UserDTO currentUser = userService.findByUserId(model.getUserId());
        model.setUserName(currentUser.getFullName());
        model.setAvtUser(currentUser.getAvt());

        Comment comment = mapper().map(model, Comment.class);

        return mapToView(commentRepository.save(comment));
    }

    @Override
    public List<CommentDTO> getCommentsByProductId(String productId) {
        List<Comment> comments = commentRepository.findByProductId(productId);

        return mapToView(comments);
    }

    @Override
    public void deleteComment(String commentId) {
        Comment comment = commentRepository.findById(commentId).orElse(null);
        if (comment == null) {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        commentRepository.delete(comment);
    }

    @Override
    public void updateAvt(String userId, String avt) {
        List<Comment> comments = commentRepository.findByUserId(userId);
        comments.forEach(comment -> {comment.setAvtUser(avt);});
        commentRepository.saveAll(comments);
    }

    @Override
    public List<ProductImage> uploadListThumbnail(MultipartFile[] files) {
        if (files.length > 5) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot upload more than 5 files at a time");
        }

        for (MultipartFile file : files) {
            if (!Objects.requireNonNull(file.getContentType()).startsWith("image/")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only image files are allowed");
            }

            if (file.getSize() > 10 * 1024 * 1024) { // 10MB
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File size must be less than 10MB");
            }
        }



        List<ProductImage> images = new ArrayList<>();

        for (MultipartFile file : files) {
            try {
                // Upload từng file lên Cloudinary
                Map imageUpload = cloudinaryClient.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
                String imageUrl = (String) imageUpload.get("url");


                // Tạo đối tượng ProductImage và lưu lại thông tin ảnh
                ProductImage image = new ProductImage();
                image.setImageUrl(imageUrl);

                images.add(productImageRepository.save(image));
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to upload image: " + e.getMessage(), e);
            }
        }

        return images;
    }

    @Override
    public List<ProductImage> getListThumbnail(String id) {
        return List.of();
    }


}
