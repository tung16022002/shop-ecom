package com.project.shopapp.service.imp;

import com.project.shopapp.dtos.OrderDTO;
import com.project.shopapp.dtos.info.OrderPageInfo;
import com.project.shopapp.exception.DataNotFoundException;
import com.project.shopapp.models.*;
import com.project.shopapp.repository.OrderRepository;
import com.project.shopapp.repository.ProductRepository;
import com.project.shopapp.repository.UserRepository;
import com.project.shopapp.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class   OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;

    public static ModelMapper mapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        return modelMapper;
    }

    public static OrderDTO mapToView(Order order) {
        if (order == null) {
            return null;
        }

        return mapper().map(order, OrderDTO.class);
    }

    public static List<OrderDTO> mapToView(List<Order> orders) {
        return orders.stream().map(d -> mapper().map(d, OrderDTO.class))
                .collect(Collectors.toList());
    }
    @Override
    public List<OrderDTO> getAllOrder() {
        return mapToView(orderRepository.findAll());
    }

    @Override
    public OrderDTO getOrderById(String id) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Order not found");
        }
        return mapToView(order);
    }

    @Override
    public List<OrderDTO> getOrdersByUserId(String userId) {

        return orderRepository.findAllByUserId(userId);
    }

    @Override
    public OrderDTO createOrder(OrderDTO model) throws DataNotFoundException {
        User user = userRepository.findById(model.getUserId()).orElse(null);
        if(user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User_id not found " + model.getUserId());
        }

        Order order = mapper().map(model, Order.class);
        order.setOrderDate(new Date());
        order.setStatus(OrderStatus.PENDING);
        order.setActive(true);

        OrderStatusHistory initialStatus = new OrderStatusHistory();
        initialStatus.setStatus("pending");  // Hoặc trạng thái mặc định bạn muốn
        initialStatus.setTime(new Date());
        order.getStatusHistory().add(initialStatus);

        if (order.getTotalMoney() == null || order.getTotalMoney() <= 0) {
            float totalMoney = order.getOrderDetails().stream()
                    .map(OrderDetail::getTotalPrice)
                    .reduce(0.0f, Float::sum);
            order.setTotalMoney(totalMoney);
        }


        return mapToView(orderRepository.save(order));
    }

    @Override
    public OrderDTO updateOrder(OrderDTO model) {
        Order order = orderRepository.findById(model.getId()).orElse(null);
        if (order == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Order not found");
        }

        order.getStatusHistory().add(new OrderStatusHistory(model.getStatus(), new Date()));

        // Cập nhật đơn hàng
        if (model.getStatus().equals("prepare")) {
            List<OrderDetail> orderDetails = order.getOrderDetails();
            updateTotalProduct(orderDetails);
        }
        // Nếu trạng thái đã thay đổi, lưu vào lịch sử trạng thái
//        if (!oldStatus.equals(order.getStatus())) {
//            order.getStatusHistory().add(new OrderStatusHistory(oldStatus, new Date())); // Thêm trạng thái cũ vào lịch sử
//        }

        mapper().map(model, order);
        return mapToView(orderRepository.save(order));
    }

    private void updateTotalProduct(List<OrderDetail> orderDetail) {
        orderDetail.forEach(data -> {
            Product product = productRepository.findById(data.getProductId()).orElse(null);
            if (product == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found " + data.getProductId());
            }
            product.setTotalProduct(product.getTotalProduct() - data.getQuantity());
            productRepository.save(product);
        });
    }

    @Override
    public OrderDTO deleteOrderById(String id) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order != null) {
            order.setActive(false);
            orderRepository.save(order);
        }
        return mapToView(order);
    }

    @Override
    public List<OrderDTO> search(OrderPageInfo model) {
        return mapToView(orderRepository.search(model)) ;
    }

    @Override
    public Long count(OrderPageInfo model) {
        return orderRepository.count(model);
    }
}
