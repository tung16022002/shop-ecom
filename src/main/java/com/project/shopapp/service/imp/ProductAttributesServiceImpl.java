package com.project.shopapp.service.imp;

import com.project.shopapp.models.ProductAttributes;
import com.project.shopapp.repository.ProductAttributesRepository;
import com.project.shopapp.service.ProductAttributeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductAttributesServiceImpl implements ProductAttributeService {
    private final ProductAttributesRepository productAttributesRepository;

    @Override
    public ProductAttributes create(ProductAttributes model) {
        return this.productAttributesRepository.save(model);
    }

    @Override
    public List<ProductAttributes> getByCode(String code) throws Exception {
        List<ProductAttributes> productAttributes = this.productAttributesRepository.findByCode(code);

        if (productAttributes.isEmpty()) {
            return new ArrayList<>();
        }

        return productAttributes;
    }
}
