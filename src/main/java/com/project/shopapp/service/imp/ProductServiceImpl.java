package com.project.shopapp.service.imp;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.project.shopapp.dtos.ProductDTO;
import com.project.shopapp.dtos.ProductImageDTO;
import com.project.shopapp.dtos.info.ProductCartInfo;
import com.project.shopapp.dtos.info.ProductPageInfo;
import com.project.shopapp.exception.DataNotFoundException;
import com.project.shopapp.models.Categories;
import com.project.shopapp.models.Product;
import com.project.shopapp.models.ProductImage;
import com.project.shopapp.models.SubCategory;
import com.project.shopapp.repository.CategoryRepository;
import com.project.shopapp.repository.ProductImageRepository;
import com.project.shopapp.repository.ProductRepository;
import com.project.shopapp.repository.SubCategoryRepository;
import com.project.shopapp.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductImageRepository productImageRepository;
    private final Cloudinary cloudinaryClient;
    private final SubCategoryRepository subCategoryRepository;


    public static ModelMapper mapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        return modelMapper;
    }

    public static ProductDTO mapToView(Product product) {
        if (product == null) {
            return null;
        }

        return mapper().map(product, ProductDTO.class);
    }

    public static ProductImage mapToViewProductImage(ProductImage model) {
        if (model == null) {
            return null;
        }

        return mapper().map(model, ProductImage.class);
    }

    public static List<ProductImageDTO> mapToViewProductImage(List<ProductImage> images) {
        return images.stream().map(d -> mapper().map(d, ProductImageDTO.class))
                .collect(Collectors.toList());
    }



    public static List<ProductDTO> mapToView(List<Product> products) {
        return products.stream().map(d -> mapper().map(d, ProductDTO.class))
                .collect(Collectors.toList());
    }

    public static List<ProductCartInfo> mapToViewCartInfo(List<Product> products) {
        return products.stream().map(d -> mapper().map(d, ProductCartInfo.class))
                .collect(Collectors.toList());
    }



    @Override
    public ProductDTO createProduct(ProductDTO model) throws DataNotFoundException {
       categoryRepository.findByCode(model.getCategoryId()).orElseThrow(
                () -> new DataNotFoundException("Cannot find code " + model.getCategoryId()));

        if (model.getListThumbnail() != null && !model.getListThumbnail().isEmpty()) {
            model.setThumbnail(model.getListThumbnail().get(0));  // Gán phần tử đầu tiên làm thumbnail
        }
        model.makeTextSearch();

       Product product = mapper().map(model, Product.class);
       return mapToView(productRepository.save(product));
    }

    @Override
    public ProductDTO getProductById(String id) {
        Product product = productRepository.findById(id).orElse(null);
        if(product == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
        }
        return mapToView(product);
    }

    @Override
    public List<ProductDTO> getAllProduct() {
        return mapToView(productRepository.findAll());
    }

    @Override
    public List<ProductDTO> search(ProductPageInfo model) {

        return mapToView(productRepository.search(model));
    }

    @Override
    public Long count(ProductPageInfo model) {
        return productRepository.count(model);
    }

    @Override
    public ProductDTO updateProduct(ProductDTO model) {
        Product product = productRepository.findById(model.getId()).orElse(null);
        if(product == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
        }
        if (model.getListThumbnail() != null && !model.getListThumbnail().isEmpty()) {
            model.setThumbnail(model.getListThumbnail().get(0));  // Gán phần tử đầu tiên làm thumbnail
        }

        mapper().map(model, product);
        return mapToView(productRepository.save(product));
    }

    @Override
    public ProductDTO deleteProductById(String id) {
        Product product = productRepository.findById(id).orElse(null);
        if(product == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
        }
        productRepository.delete(product);
        return mapToView(product);
    }

    @Override
    public boolean existsByName(String name) {
        return productRepository.existsByName(name);
    }

    @Override
    public List<ProductDTO> getAllByCategoryId(String id) {
        Categories categories = categoryRepository.findById(id).orElse(null);
        if(categories == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Category not found");
        }

        return mapToView(productRepository.findAllByCategoryId(id));
    }

    @Override
    public List<ProductDTO> getAllBySubCategoryId(String id) {
        SubCategory subCategory = subCategoryRepository.findById(id).orElse(null);
        if(subCategory == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "SubCategory not found");
        }

        return mapToView(productRepository.findAllBySubCategoryId(id));
    }


    @Override
    public ProductImage uploadThumbnail(MultipartFile file, String id) throws IOException {
        if (!Objects.requireNonNull(file.getContentType()).startsWith("image/")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only image files are allowed");
        }

        if (file.getSize() > 10 * 1024 * 1024) { // 10MB
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File size must be less than 10MB");
        }

        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
        }

        try {
            Map imageUpload = cloudinaryClient.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
            String imageUrl = (String) imageUpload.get("url");

            product.setThumbnail(imageUrl);
            productRepository.save(product);

            ProductImage image = new ProductImage();
            image.setProductId(id);
            image.setImageUrl(imageUrl);
            return productImageRepository.save(image);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to upload image: " + e.getMessage(), e);
        }
    }

    @Override
    public List<ProductImage> uploadListThumbnail(MultipartFile[] files) throws IOException {
//        if (files.length > 5) {
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot upload more than 5 files at a time");
//        }

        for (MultipartFile file : files) {
            if (!Objects.requireNonNull(file.getContentType()).startsWith("image/")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only image files are allowed");
            }

            if (file.getSize() > 10 * 1024 * 1024) { // 10MB
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File size must be less than 10MB");
            }
        }



        List<ProductImage> images = new ArrayList<>();

        for (MultipartFile file : files) {
            try {
                // Upload từng file lên Cloudinary
                Map imageUpload = cloudinaryClient.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
                String imageUrl = (String) imageUpload.get("url");


                // Tạo đối tượng ProductImage và lưu lại thông tin ảnh
                ProductImage image = new ProductImage();
                image.setImageUrl(imageUrl);

                images.add(productImageRepository.save(image));
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to upload image: " + e.getMessage(), e);
            }
        }

        return images;
    }

    @Override
    public List<ProductImage> getListThumbnail(String id) {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
        }

        return productImageRepository.findByProductId(id);
    }

    @Override
    public List<ProductCartInfo> getProductByIds(List<String> ids) {
        List<Product> products = productRepository.findAllById(ids);
        return mapToViewCartInfo(products);
    }

//    @Override
//    public ProductImage createProductImage(String productId,ProductImageDTO model) throws InValidParamException {
//        Product product = productRepository.findById(productId).orElse(null);
//        if(product == null) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
//        }
//
//        ProductImage productImage = mapper().map(model, ProductImage.class);
//        int size = productImageRepository.findByProductId(productId).size();
//        if(size >= ProductImage.MAX_IMAGES)
//            throw new InValidParamException("Numbers of images must be <= " + ProductImage.MAX_IMAGES);
//
//        return mapToViewProductImage(productImageRepository.save(productImage));
//    }
}
