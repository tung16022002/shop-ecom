package com.project.shopapp.service.imp;

import com.project.shopapp.dtos.SubCategoryDTO;
import com.project.shopapp.models.Categories;
import com.project.shopapp.models.SubCategory;
import com.project.shopapp.repository.CategoryRepository;
import com.project.shopapp.repository.SubCategoryRepository;
import com.project.shopapp.service.SubCategoryService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SubCategoryServiceDTO implements SubCategoryService {
    private final SubCategoryRepository subCategoryRepository;
    private final CategoryRepository categoryRepository;

    public static ModelMapper mapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        return modelMapper;
    }

    public static SubCategoryDTO mapToView(SubCategory subCategory) {
        if (subCategory == null) {
            return null;
        }

        return mapper().map(subCategory, SubCategoryDTO.class);
    }

    public static List<SubCategoryDTO> mapToView(List<SubCategory> subCategories) {
        return subCategories.stream().map(d -> mapper().map(d, SubCategoryDTO.class))
                .collect(Collectors.toList());
    }


    @Override
    public SubCategoryDTO add(SubCategoryDTO model) {
        // Check if the category ID exists
        Optional<Categories> categoryOpt = categoryRepository.findById(model.getCategoryId());
        if (categoryOpt.isEmpty()) {
            throw new IllegalArgumentException("Category ID does not exist");
        }
        // Save the subcategory
        SubCategory subCategory = mapper().map(model, SubCategory.class);
        subCategoryRepository.save(subCategory);


        // Add the subcategory to the corresponding category
        Categories categories = categoryOpt.get();
        try {
//            categories.getSubcategories().add(subCategory);
        } catch (Exception e) {
            throw new RuntimeException("Failed to add SubCategory to Category subcategories list: " + e.getMessage());
        }

        // Save the updated category
        categoryRepository.save(categories);

        return mapToView(subCategory);
    }
}
