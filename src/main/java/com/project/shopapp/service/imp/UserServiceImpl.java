package com.project.shopapp.service.imp;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.project.shopapp.dtos.UserDTO;
import com.project.shopapp.dtos.info.ChangePasswordDTO;
import com.project.shopapp.dtos.info.UserUpdateInfo;
import com.project.shopapp.exception.DataNotFoundException;
import com.project.shopapp.exception.PermissionDenied;
import com.project.shopapp.helper.JwtTokenUtil;
import com.project.shopapp.models.Role;
import com.project.shopapp.models.User;
import com.project.shopapp.repository.RoleRepository;
import com.project.shopapp.repository.UserRepository;
import com.project.shopapp.service.CommentService;
import com.project.shopapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenUtil jwtTokenUtil;
    private final AuthenticationManager authenticationManager;
    private final Cloudinary cloudinaryClient;

    public static ModelMapper mapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT)
                .setSkipNullEnabled(true);

        return modelMapper;
    }

    public static UserDTO mapToView(User user) {
        if (user == null) {
            return null;
        }

        return mapper().map(user, UserDTO.class);
    }

    public static List<UserDTO> mapToView(List<User> users) {
        return users.stream().map(d -> mapper().map(d, UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO createUser(UserDTO model) throws PermissionDenied, DataNotFoundException {
        String phoneNumber = model.getPhoneNumber();
        //Kiem tra xem co ton tai hay chua
        if(userRepository.existsByPhoneNumber(phoneNumber)) {
            throw new DataIntegrityViolationException("Phone is already exists");
        }

        Role role = roleRepository.findByName(model.getRoleName()).orElse(null);
        if(role == null) {
            throw new DataNotFoundException("Role not found");
        }
        if(role.getName().equalsIgnoreCase("ADMIN")) {
            throw new PermissionDenied("Khong duoc tao role admin");
        }


        User user = mapper().map(model, User.class);

        //Kiem tra neu co accountId thi khong yeu cau mat khau
        if(user.getFacebookAccountId() == 0 && user.getGoogleAccountId() == 0) {
            String password = user.getPassword();
            String encoderPassword = passwordEncoder.encode(password);
            user.setPassword(encoderPassword);
        }
        return mapToView(userRepository.save(user));
    }

    @Override
    public String login(String phoneNumber, String password) throws DataNotFoundException {
        User user = userRepository.findByPhoneNumber(phoneNumber).orElse(null);
        if(user == null) {
            throw new DataNotFoundException("Invalid Phone Number or Password !");
        }
        //Muon tra ve jwt token
        //check password
        if(user.getFacebookAccountId() == 0 && user.getGoogleAccountId() == 0) {
            if(!passwordEncoder.matches(password, user.getPassword())) {
                throw new BadCredentialsException("Invalid Password or PhoneNumber !");
            }
        }
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(phoneNumber, password, user.getAuthorities());
        //Authenticate voi Spring security
        authenticationManager.authenticate(authenticationToken);
        return jwtTokenUtil.generateToken(user);
    }

    @Override
    public  User getCurrentUser() {
        return  (User) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
    }

    @Override
    public UserDTO uploadAvatar(MultipartFile file, String userId) throws IOException {
        if (!Objects.requireNonNull(file.getContentType()).startsWith("image/")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only image files are allowed");
        }

        if (file.getSize() > 10 * 1024 * 1024) { // 10MB
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File size must be less than 10MB");
        }

        User user = userRepository.findById(userId).orElse(null);
        if(user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }

        Map imageUpload = cloudinaryClient.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
        String avtUrl = (String) imageUpload.get("url");

        user.setAvt(avtUrl);
        userRepository.save(user);
        return mapToView(user);
    }

    @Override
    public UserDTO updateUser(UserUpdateInfo model) throws DataNotFoundException {
        User user = userRepository.findByPhoneNumber(model.getPhoneNumber()).orElse(null);
        if(user == null) {
            throw new DataNotFoundException("User not found");
        }
        mapper().map(model, user);
        return mapToView(userRepository.save(user));
    }

    @Override
    public User changePassword(String phoneNumber, ChangePasswordDTO model) throws DataNotFoundException {
        User user = userRepository.findByPhoneNumber(phoneNumber).orElse(null);
        if(user == null) {
            throw new DataNotFoundException("User not found");
        }

        if (!passwordEncoder.matches(model.getCurrentPassword(), user.getPassword())) {
            throw new BadCredentialsException("Old password is incorrect");
        }

        String newHashedPassword = passwordEncoder.encode(model.getNewPassword());

        user.setPassword(newHashedPassword);

        return userRepository.save(user);

    }

    @Override
    public UserDTO findByUserId(String id) {
        User user = userRepository.findById(id).orElse(null);
        return mapToView(user);
    }


    @Override
    public List<UserDTO> findAll() {
        return mapToView(userRepository.findAll());
    }

    @Override
    public boolean checkPhoneNumberExists(String phoneNumber) {
        return userRepository.existsByPhoneNumber(phoneNumber);
    }
}
